<?php
/*
Plugin Name: WiredForSound
Version: 2.7.a
Description: Ajoute du son à la galerie / Wire for sound your gallery
Plugin URI: http://piwigo.org/ext/extension_view.php?eid=152
Author: P@t
Author URI: http://www.gauchon.com
*/

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

global $prefixeTable;

define('WFS_DIR' , basename(dirname(__FILE__)));
define('WFS_PATH' , PHPWG_PLUGINS_PATH . WFS_DIR . '/');
define('WFS_IMG_CAT_TABLE' , $prefixeTable . 'wfs_img_cat_sound');
define('WFS_SOUNDS_TABLE' , $prefixeTable . 'wfs_sounds');

function wfs_admin_menu($menu)
{
    array_push($menu, array(
      'NAME' => 'Wired For Sound',
      'URL' => get_admin_plugin_menu_link(WFS_PATH . 'admin/admin.php')));
    return $menu;
}

function wired_for_sound_on_index()
{
    global $page, $template, $conf, $prefixeTable, $lang; 
    if (isset($page['category']) || isset($page['section']) and !isset($page['chronology_field']) and !isset($page['flat']))
        include(WFS_PATH . 'wfs_on_index.php');
}

function wired_for_sound_on_picture()
{
    global $page, $template, $conf, $prefixeTable, $lang;
	include(WFS_PATH . 'wfs_on_picture.php');
}

add_event_handler('get_admin_plugin_menu_links', 'wfs_admin_menu');
add_event_handler('loc_end_index' , 'wired_for_sound_on_index');
add_event_handler('loc_end_picture' , 'wired_for_sound_on_picture');

?>