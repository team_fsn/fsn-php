<?php

if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');
load_language('plugin.lang', WFS_PATH);

global $conf, $template, $lang;

$wfs_conf = explode ("," , $conf['wired_for_sound']);
// Enregistrement de la configuration
if (isset($_POST['submit']))
{
  if (strrpos($_POST['default_path'] , '/') != (strlen($_POST['default_path']) - 1))
  {
    $_POST['default_path'] .= '/';
  }
  if (!is_dir('./' . $_POST['default_path']))
  {
    $wfs_conf[0] = $_POST['default_path'];
    array_push($page['errors'], l10n('wfs_no_path'));
  }
  else
  {
    $wfs_conf = array($_POST['default_path'],
      $_POST['default_volume'],
      $_POST['width_player'],
      $_POST['autostart'],
      $_POST['autoreplay']);
    $new_wfs_conf = implode ("," , $wfs_conf);
    $query = '
UPDATE ' . CONFIG_TABLE . '
    SET value="' . $new_wfs_conf . '"
    WHERE param="wired_for_sound"
    LIMIT 1';
      pwg_query($query);
      array_push($page['infos'], l10n('wfs_conf_saved'));
  }
}
// Parametrage du template
$template->assign(array(
  'DEFAULT_PATH' => $wfs_conf[0],
  'DEFAULT_VOLUME' => $wfs_conf[1]));
if ($wfs_conf[2] == '44') $template->assign(array('SMALL_PLAYER' => 'checked="checked"'));
if ($wfs_conf[2] == '64') $template->assign(array('MEDIUM_PLAYER' => 'checked="checked"'));
if ($wfs_conf[2] == '202') $template->assign(array('BIG_PLAYER' => 'checked="checked"'));
if ($wfs_conf[3] == '1')
{
  $template->assign(array('AUTOSTART_ON' => 'checked="checked"'));
}
else
{
  $template->assign(array('AUTOSTART_OFF' => 'checked="checked"'));
}
if ($wfs_conf[4] == '1')
{
  $template->assign(array('AUTOREPLAY_ON' => 'checked="checked"'));
}
else
{
  $template->assign(array('AUTOREPLAY_OFF' => 'checked="checked"'));
}

$template->set_filenames(array('plugin_admin_content' => realpath(WFS_PATH . 'template/admin.tpl')));
$template->assign_var_from_handle('ADMIN_CONTENT', 'plugin_admin_content');

?>