<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_loop'] = '自动循环';
$lang['wfs_medium'] = '中';
$lang['wfs_no_mp3'] = '此目录里没有可用声音';
$lang['wfs_no_path'] = '无此目录';
$lang['wfs_only_this_cat'] = '只有当前类别';
$lang['wfs_optional'] = '选项';
$lang['wfs_path'] = '目录路径';
$lang['wfs_player_width'] = '播放于';
$lang['wfs_show_default_path'] = '显示默认目录';
$lang['wfs_volume'] = '音量';
$lang['wfs_actual_file'] = '当前链接的文件';
$lang['wfs_all_cat'] = '所有类别';
$lang['wfs_apply_only_this_cat'] = '只连接到当前类别里';
$lang['wfs_associate_category'] = '此类别的线材声音';
$lang['wfs_associate_image'] = '此相片的线材声音';
$lang['wfs_associate_sound'] = '添加－线材声音';
$lang['wfs_associate_to_cat'] = '连接到当前类别';
$lang['wfs_associate_to_pic'] = '连接到相片';
$lang['wfs_autostart'] = '自动开始';
$lang['wfs_back_cat'] = '返回到类别';
$lang['wfs_back_pic'] = '返回到相片';
$lang['wfs_change_path'] = '改变目录';
$lang['wfs_conf_saved'] = '配置已被保存。';
$lang['wfs_configuration'] = '配置';
$lang['wfs_default_directory'] = '默认目录';
$lang['wfs_default_volume'] = '默认音量';
$lang['wfs_delete'] = '取消连接';
$lang['wfs_file'] = '文件';
$lang['wfs_large'] = '大';
$lang['wfs_litle'] = '小';
?>