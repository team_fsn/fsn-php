<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_default_volume'] = 'Standardlydstyrke';
$lang['wfs_associate_category'] = 'Benyt lyd sammen med denne kategori';
$lang['wfs_associate_image'] = 'Benyt lyd sammen med dette billede';
$lang['wfs_loop'] = 'Autoløkke';
$lang['wfs_medium'] = 'Mellem';
$lang['wfs_no_mp3'] = 'Ingen lyde tilgængelige i denne mappe.';
$lang['wfs_no_path'] = 'Mappe ikke fundet.';
$lang['wfs_only_this_cat'] = 'Kun aktuel kategori';
$lang['wfs_optional'] = 'valgfri';
$lang['wfs_path'] = 'Sti til mappe';
$lang['wfs_player_width'] = 'Afspillers bredde';
$lang['wfs_show_default_path'] = 'Vis standardmappe';
$lang['wfs_volume'] = 'Lydstyrke';
$lang['wfs_actual_file'] = 'Aktuel fil der linkes til';
$lang['wfs_all_cat'] = 'Alle kategorier';
$lang['wfs_apply_only_this_cat'] = 'Link kun inden for den aktuelle kategori';
$lang['wfs_associate_sound'] = 'Wired For Sound - tilføj lyd';
$lang['wfs_associate_to_cat'] = 'Link til den aktuelle kategori';
$lang['wfs_associate_to_pic'] = 'Link til billedet';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_back_cat'] = 'Tilbage til kategori';
$lang['wfs_back_pic'] = 'Tilbage til billede';
$lang['wfs_change_path'] = 'Skift mappe';
$lang['wfs_conf_saved'] = 'Opæsningen er gemt.';
$lang['wfs_configuration'] = 'Opsætning';
$lang['wfs_default_directory'] = 'Standardmappe';
$lang['wfs_delete'] = 'Fjern link';
$lang['wfs_file'] = 'Fil';
$lang['wfs_large'] = 'Stor';
$lang['wfs_litle'] = 'Lille';
?>