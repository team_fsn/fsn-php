<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_actual_file'] = 'Aktualnie powiązany plik';
$lang['wfs_apply_only_this_cat'] = 'Powiąż tylko wewnątrz aktualnej kategorii';
$lang['wfs_medium'] = 'Średni';
$lang['wfs_optional'] = 'opcjonalny';
$lang['wfs_loop'] = 'Powtarzanie';
$lang['wfs_no_mp3'] = 'Brak dźwięków w tym katalogu';
$lang['wfs_only_this_cat'] = 'Wyłącznie obecna kategoria';
$lang['wfs_show_default_path'] = 'Pokaż domyślny katalog';
$lang['wfs_all_cat'] = 'Wszystkie kategorie';
$lang['wfs_associate_to_cat'] = 'Odnośnik do obecnej kategorii';
$lang['wfs_associate_to_pic'] = 'Odnośnik do obrazu';
$lang['wfs_autostart'] = 'Autostart';
$lang['wfs_back_cat'] = 'Wróć do kategorii';
$lang['wfs_back_pic'] = 'Wróć do obrazu';
$lang['wfs_change_path'] = 'Zmień katalog';
$lang['wfs_conf_saved'] = 'Konfiguracja została zapisana.';
$lang['wfs_configuration'] = 'Konfiguracja';
$lang['wfs_default_directory'] = 'Domyślny katalog';
$lang['wfs_default_volume'] = 'Domyślna głośność';
$lang['wfs_file'] = 'Plik';
$lang['wfs_large'] = 'Duży';
$lang['wfs_litle'] = 'Mały';
$lang['wfs_no_path'] = 'Nie znaleziono katalogu.';
$lang['wfs_path'] = 'Ścieżka katalogu';
$lang['wfs_player_width'] = 'Szerokość odtwarzacza';
$lang['wfs_volume'] = 'Głośność';
$lang['wfs_delete'] = 'Usuń powiązanie';
$lang['wfs_associate_category'] = 'Przypisz dźwięk do tej kategorii';
$lang['wfs_associate_image'] = 'Przypisz dźwięk do tego obrazu';
$lang['wfs_associate_sound'] = 'Wired For Sound - Dodaj dźwięk';
?>