<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['wfs_loop'] = 'Automatické opakovanie';
$lang['wfs_no_mp3'] = 'Zvuk v tomto adresári nie je k dispozícii.';
$lang['wfs_path'] = 'Cesta adresára';
$lang['wfs_player_width'] = 'Šírka prehrávača';
$lang['wfs_show_default_path'] = 'Zobraziť predvolený adresár';
$lang['wfs_volume'] = 'Zložka';
$lang['wfs_medium'] = 'Médium';
$lang['wfs_no_path'] = 'Adresár nenájdený.';
$lang['wfs_only_this_cat'] = 'Iba aktuálna kategória';
$lang['wfs_optional'] = 'volitelný';
$lang['wfs_actual_file'] = 'Aktuálny nalinkovaný súbor';
$lang['wfs_apply_only_this_cat'] = 'Link iba v aktuálnej kategŕii';
$lang['wfs_associate_category'] = 'Táto kategória prepojená so zvukom';
$lang['wfs_associate_image'] = 'Tento obrázok prepojený so zvukom';
$lang['wfs_associate_to_cat'] = 'Linka k aktuálnej kategórii';
$lang['wfs_default_volume'] = 'Predvolený zložka';
$lang['wfs_default_directory'] = 'Predvolený adresár';
$lang['wfs_all_cat'] = 'Všetky kategórie';
$lang['wfs_associate_sound'] = 'Prepojené so zvukom - Pridať zvuk';
$lang['wfs_associate_to_pic'] = 'Linka k obrázku';
$lang['wfs_autostart'] = 'Autoštart';
$lang['wfs_back_cat'] = 'Späť na kategóriu';
$lang['wfs_back_pic'] = 'Späť na obrázok';
$lang['wfs_change_path'] = 'Zmeniť ';
$lang['wfs_conf_saved'] = 'Konfigurácia bola uložená.';
$lang['wfs_configuration'] = 'Konfigurácia';
$lang['wfs_delete'] = 'Odlinkovať';
$lang['wfs_file'] = 'Súbor';
$lang['wfs_large'] = 'Veľký';
$lang['wfs_litle'] = 'Malý';
?>