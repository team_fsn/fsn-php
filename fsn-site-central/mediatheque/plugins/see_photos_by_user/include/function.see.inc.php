<?php 
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
function see_username($id_user){
  $query = 'SELECT id, username FROM ' . USERS_TABLE . ' WHERE id = \'' . $id_user . '\';';
  $result = pwg_query($query);
  $row = pwg_db_fetch_assoc($result);
  $username = str_replace(' ','_',$row['username']);
  $username = str_replace('.','_',$username);
  return $username;
}

function see_userlist_nb_photo(){
  global $conf;
  $query = '
	SELECT UT.id, UT.username, COUNT(DISTINCT(IT.id)) AS PBU
	FROM ' . USERS_TABLE . ' as UT
	INNER JOIN ' . IMAGES_TABLE . ' AS IT ON IT.added_by = UT.id
	INNER JOIN ' . IMAGE_CATEGORY_TABLE . ' AS ic ON IT.id = ic.image_id
	' . get_sql_condition_FandF(
      array(
		'forbidden_categories' => 'category_id',
		'visible_categories' => 'category_id',
		'visible_images' => 'id'
	  ), 'WHERE'
    ) . '
	GROUP BY IT.added_by
	HAVING PBU >' . $conf['see_photos_by_user_nbphotos'] . '
	ORDER BY ' . $conf['see_photos_by_user_order'] . '
	LIMIT ' . $conf['see_photos_by_user_limit'] . ';';

  $result = pwg_query($query);
  return $result;
}

function see_nb_photo(){
  $query = 'SELECT COUNT(DISTINCT(id)) AS PBU FROM ' . IMAGES_TABLE . ';';
  $result = pwg_query($query);
  $row = pwg_db_fetch_assoc($result);
  return $row['PBU'];
}

?>