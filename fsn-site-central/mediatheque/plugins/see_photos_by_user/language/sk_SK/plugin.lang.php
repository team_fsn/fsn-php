<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['users in alphabetical order'] = 'používatelia v abecednom poradí';
$lang['users by reverse alphabetical order'] = 'používatelia v obrátenom abecednom poradí';
$lang['submit'] = 'odoslať';
$lang['select users box'] = 'označiť používateľský box';
$lang['select user'] = 'označiť používateľa';
$lang['select other user'] = 'označiť iného používateľa';
$lang['link Specials menu'] = 'link špeciálnych menu';
$lang['by increasing number of photos'] = 'so zvýšením počtu fotografií';
$lang['by decreasing number of photos'] = 'so znížením počtu fotografií';
$lang['bloc menu users'] = 'menu používateľov';
$lang['Users order'] = 'zoradenie používateľov';
$lang['See photos by user'] = 'Pozrieť fotky používateľom';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimálny počet fotiek pre zobrazenie používateľom nie je správny!';
$lang['Minimal number photos for show users'] = 'Minimálny počet fotiek pre zobrazenie používateľom';
$lang['Maximal number users is incorrect !'] = 'Maximálny počet používateľov nie je správny!';
$lang['Maximal number users'] = 'Maximálny počet používateľov';
$lang['Photos by user'] = 'Fotky používateľa';
$lang['cumulus users cloud'] = 'používatelia cumulus-u';
$lang['users cloud'] = 'používatelia cloudu';
$lang['You must use colorpicker'] = 'Musíte použiť naberač farby';
$lang['Show menu'] = 'Zobraz menu';
$lang['Show home page users'] = 'Zobraz úvodnú stránku používateľa';
$lang['sphere'] = 'guľa';
$lang['vertical ring'] = 'vertikálny kruh';
$lang['vertical cylinder'] = 'vertikálny valec';
$lang['horizontal ring'] = 'horizontálny kruh';
$lang['horizontal cylinder'] = 'horizontálny valec';
$lang['Choose a presentation'] = 'Zvoľte prezentáciu';
$lang['Choose a color'] = 'Zvoľte farbu';