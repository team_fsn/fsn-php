<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['by decreasing number of photos'] = '按照片数量降序';
$lang['by increasing number of photos'] = '按照片数量升序';
$lang['link Specials menu'] = '在"特色浏览"的链接';
$lang['select other user'] = '选择另一个用户';
$lang['select user'] = '选择一个用户';
$lang['select users box'] = '用户选择';
$lang['Users order'] = '用户排序';
$lang['bloc menu users'] = '用户菜单区块';
$lang['submit'] = '提交';
$lang['users by reverse alphabetical order'] = '以字母逆序排列用户';
$lang['users in alphabetical order'] = '以字母顺序排列用户';
$lang['Photos by user'] = '由用户上传的照片';
$lang['See photos by user'] = '根据用户显示照片';
$lang['Minimal number photos for show users'] = '向用户显示的最少图片数量';
$lang['Minimal number photos for show users is incorrect !'] = '向用户显示的最少图片数量不正确！';
$lang['Maximal number users'] = '最大用户数';
$lang['Maximal number users is incorrect !'] = '最大用户数不正确！';
$lang['Choose a presentation'] = '选择一个表现形式';
$lang['horizontal cylinder'] = '水平圆柱';
$lang['horizontal ring'] = '水平环';
$lang['vertical cylinder'] = '垂直圆柱';
$lang['vertical ring'] = '垂直环';
$lang['sphere'] = '球形';
$lang['Show home page users'] = '在用户主页上显示';
$lang['You must use colorpicker'] = '你必须使用拾色器';
$lang['Show menu'] = '在菜单上显示';
$lang['Choose a color'] = '选择一个颜色';
$lang['cumulus users cloud'] = '动态用户云';
$lang['users cloud'] = '用户云';