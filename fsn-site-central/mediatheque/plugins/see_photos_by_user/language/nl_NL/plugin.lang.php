<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Maximaal aantal gebruikers';
$lang['Maximal number users is incorrect !'] = 'Maximaal aantal gebruikers is foutief!';
$lang['Minimal number photos for show users'] = 'Minimaal aantal foto\'s om gebruikers te zien ';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimaal aantal foto\'s om gebruikers te zien is foutief!';
$lang['Photos by user'] = 'Foto\'s per gebruiker';
$lang['See photos by user'] = 'Zie aantal foto\'s per gebruiker';
$lang['Users order'] = 'Sorteer gebruikers';
$lang['bloc menu users'] = 'Groep gebruikers menu';
$lang['by decreasing number of photos'] = 'op aantal foto\'s van laag naar hoog';
$lang['by increasing number of photos'] = 'op aantal foto\'s van hoog naar laag';
$lang['link Specials menu'] = 'Link naar Menu Speciaal';
$lang['select other user'] = 'Selecteer andere gebruiker';
$lang['select user'] = 'Selecteer gebruiker';
$lang['select users box'] = 'Selecteer gebruikers box';
$lang['submit'] = 'Verzenden';
$lang['users by reverse alphabetical order'] = 'gebruikers in omgekeerde alfabetische volgorde';
$lang['users in alphabetical order'] = 'gebruikers in alfabetische volgorde';
$lang['cumulus users cloud'] = 'stapel gebruikerswolken';
$lang['users cloud'] = 'gebruikerswolk';
$lang['You must use colorpicker'] = 'Gebruik de kleurenkiezer';
$lang['Show menu'] = 'Laat menu zien';
$lang['Show home page users'] = 'Laat homepage van gebruiker zien';
$lang['Choose a color'] = 'Kies een kleur';
$lang['Choose a presentation'] = 'Kies een presentatie';
$lang['horizontal cylinder'] = 'horizontale cilinder';
$lang['horizontal ring'] = 'horizontale ring';
$lang['sphere'] = 'bol';
$lang['vertical cylinder'] = 'verticale cilinder';
$lang['vertical ring'] = 'verticale ring';