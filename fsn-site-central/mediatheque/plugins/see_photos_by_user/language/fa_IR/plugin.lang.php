<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'بیشترین تعدادِ كاربران';
$lang['Maximal number users is incorrect !'] = 'بیشترین تعدادِ كاربران نادرست است!';
$lang['Minimal number photos for show users'] = 'كمترین تعدادِ عکس برای نمایشِ كاربران';
$lang['Minimal number photos for show users is incorrect !'] = 'كمترین تعدادِ عکس‌ها برای نمایش كاربران نادرست است!';
$lang['See photos by user'] = 'نمایش عکس‌های كاربر';
$lang['Users order'] = 'به ترتیب كاربران';
$lang['bloc menu users'] = 'محدوديت منوي كاربر';
$lang['by decreasing number of photos'] = 'با ترتیب نزولیِ تعداد عکس‌ها';
$lang['by increasing number of photos'] = 'با ترتیب صعودیِ تعداد عکس‌ها';
$lang['link Specials menu'] = 'پیوند در منوی ویژه';
$lang['select other user'] = 'كاربر ديگری را انتخاب كنید';
$lang['select user'] = 'كاربر را انتخاب كنید';
$lang['select users box'] = 'جعبه‌ی كاربران را انتخاب كنید';
$lang['submit'] = 'ثبت';
$lang['users by reverse alphabetical order'] = 'با ترتيبِ معكوس الفبایی نام کاربری';
$lang['users in alphabetical order'] = 'با ترتيبِ الفبایی نام کاربری';
$lang['Photos by user'] = 'عکس‌های کاربر';
$lang['Choose a color'] = 'یه رنگ انتخاب کنید';
$lang['Choose a presentation'] = 'یک سبک نمایش انتخاب کنید';
$lang['Show menu'] = 'نمایش منو';
$lang['You must use colorpicker'] = 'برای اینکار باید از انتخاب کننده‌ی رنگ استفاده کنید';
$lang['horizontal ring'] = 'حلقه‌ی افقی';
$lang['sphere'] = 'کره';
$lang['vertical ring'] = 'حلقه‌ی عمودی';