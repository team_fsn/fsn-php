<?php
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['See photos by user'] = 'See photos by user';
$lang['select user'] = 'select user';
$lang['select other user'] = 'select other user';
$lang['submit'] = 'submit';
$lang['users in alphabetical order'] = 'users in alphabetical order';
$lang['users by reverse alphabetical order'] = 'users by reverse alphabetical order';
$lang['by increasing number of photos'] = 'by increasing number of photos';
$lang['by decreasing number of photos'] = 'by decreasing number of photos';
$lang['Minimal number photos for show users'] = 'Minimal number photos for show users';
$lang['Maximal number users'] = 'Maximal number users';
$lang['Users order'] = 'users order';
$lang['Maximal number users is incorrect !'] = 'Maximal number users is incorrect !';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimal number photos for show users is incorrect !';
$lang['link Specials menu'] = 'link Specials menu';
$lang['select users box'] = 'select users box';
$lang['bloc menu users'] = 'bloc menu users';
$lang['Photos by user'] = 'Photos by user';
$lang['You must use colorpicker'] = 'You must use colorpicker';
$lang['users cloud'] = 'users cloud';
$lang['cumulus users cloud'] = 'cumulus users cloud';
$lang['Show menu'] = 'Show menu';
$lang['Show home page users'] = 'Show home page user';
$lang['Choose a color'] = 'Choose a color';
$lang['Choose a presentation'] = 'Choose a presentation';
$lang['sphere'] = 'sphere';
$lang['vertical cylinder'] = 'vertical cylinder';
$lang['horizontal cylinder'] = 'horizontal cylinder';
$lang['vertical ring'] = 'vertical ring';
$lang['horizontal ring'] = 'horizontal ring';

?>