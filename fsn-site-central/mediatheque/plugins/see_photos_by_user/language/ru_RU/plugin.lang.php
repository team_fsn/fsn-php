<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Maximal number users'] = 'Максимальное количество пользователей';
$lang['Maximal number users is incorrect !'] = 'Максимум пользователей указан неверно!';
$lang['Minimal number photos for show users'] = 'Минимальное количество изображений для показа пользователям';
$lang['Minimal number photos for show users is incorrect !'] = 'Минимальное количество изображений указано неверно!';
$lang['See photos by user'] = 'See photos by user';
$lang['Users order'] = 'порядок пользователей';
$lang['bloc menu users'] = 'блок меню пользователей';
$lang['by decreasing number of photos'] = 'по уменьшению числа изображений';
$lang['by increasing number of photos'] = 'по увеличению числа изображений';
$lang['link Specials menu'] = 'ссылка на Специальное меню';
$lang['select other user'] = 'выберите другого пользователя';
$lang['select user'] = 'выберите пользователя';
$lang['select users box'] = 'установите флажок пользователя';
$lang['submit'] = 'подтверждение';
$lang['users by reverse alphabetical order'] = 'пользователи в обратном алфавитном порядке';
$lang['users in alphabetical order'] = 'пользователи в алфавитном порядке';
$lang['Photos by user'] = 'Изображения по загрузившим их пользователям (плагин Photos by user)';
$lang['Show home page users'] = 'Показать домашнюю страницу пользователя';
$lang['Show menu'] = 'Показать меню';
$lang['You must use colorpicker'] = 'Вы должны использовать ColorPicker';
$lang['cumulus users cloud'] = '"кучевое облако" пользователей';
$lang['users cloud'] = 'облако пользователей';
$lang['Choose a color'] = 'Выбор цвета';
$lang['Choose a presentation'] = 'Выбор представления';
$lang['horizontal cylinder'] = 'горизонтальный цилиндр';
$lang['horizontal ring'] = 'горизонтальное кольцо';
$lang['sphere'] = 'сфера';
$lang['vertical cylinder'] = 'вертикальный цилиндр';
$lang['vertical ring'] = 'вертикальное кольцо';