<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2014 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['by decreasing number of photos'] = 'Od największej do najmniejszej ilości zdjęć';
$lang['by increasing number of photos'] = 'Od najmniejszej do największej ilości zdjęć';
$lang['cumulus users cloud'] = 'Użytkownicy chmury cumulus';
$lang['link Specials menu'] = 'Podlinkuj menu Specjalne';
$lang['select users box'] = 'Formularz wybierania użytkownika';
$lang['users by reverse alphabetical order'] = 'Użytkownicy w odwróconej kolejności alfabetycznej';
$lang['select user'] = 'Wybierz użytkownika';
$lang['sphere'] = 'Kula';
$lang['submit'] = 'Wyślij';
$lang['users cloud'] = 'Chmura użytkownika';
$lang['users in alphabetical order'] = 'Użytkownicy w kolejności alfabetycznej';
$lang['vertical cylinder'] = 'Pionowy cylinder';
$lang['vertical ring'] = 'Pionowy pierścień';
$lang['horizontal cylinder'] = 'Poziomy cylinder';
$lang['horizontal ring'] = 'Poziomy pierscien';
$lang['select other user'] = 'Wybierz innego użytkownika';
$lang['Maximal number users'] = 'Maksymalna ilość użytkowników';
$lang['Maximal number users is incorrect !'] = 'Maksymalna ilość użytkowników jest nieprawidłowa';
$lang['Minimal number photos for show users'] = 'Minimalna ilość zdjęć dla użytkowników pokazu';
$lang['Minimal number photos for show users is incorrect !'] = 'Minimalna ilość zdjęć dla użytkowników pokazu jest nieprawidłowa';
$lang['Photos by user'] = 'Zdjecia użytkownika';
$lang['See photos by user'] = 'Zobacz zdjecia użytkownika';
$lang['Show home page users'] = 'Pokaż strone domową użytkownika';
$lang['Show menu'] = 'Pokaż menu';
$lang['Users order'] = 'Kolejność użytkowników';
$lang['You must use colorpicker'] = 'Musisz użyć wybieracza kolorów';
$lang['bloc menu users'] = 'Zablokuj użytkowników menu';
$lang['Choose a color'] = 'Wybierz kolor';
$lang['Choose a presentation'] = 'Wybierz prezentację';