<?php
// +-----------------------------------------------------------------------+
// | See photos by user plugin for piwigo                                  |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2014 - 2016 ddtddt             http://temmii.com/piwigo/ |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+

global $page, $conf, $user, $template;

 /*test URL*/
$testa = explode('user-', $_SERVER['REQUEST_URI']);
if(!empty($testa[1])){
  $testd = explode('-', $testa[1]);
  if(is_numeric($testd[0])){
    $username= see_username($testd[0]);
    $me = 'user-' . $testd[0].'-'.$username;
    $page['section'] = $me;
    $page['section_title'] = '<a href="' . get_absolute_root_url() . '">' . l10n('Home') . '</a>' . $conf['level_separator'] . '<a href="' . get_absolute_root_url() . 'index.php?/user-">' . l10n('Users').'</a>'. $conf['level_separator'] . '<a href="' . get_absolute_root_url() . 'index.php?/' . $me . '">'.$username. '</a>';
    $template->assign(
      'gestionB', array(
      'USERUSBU' => $username,
    ));
    $query = '
      SELECT DISTINCT(id)
      FROM ' . IMAGES_TABLE . '
      INNER JOIN ' . IMAGE_CATEGORY_TABLE . ' AS ic ON id = ic.image_id
      ' . get_sql_condition_FandF
        (
          array
        (
          'forbidden_categories' => 'category_id',
          'visible_categories' => 'category_id',
          'visible_images' => 'id'
        ), 'WHERE'
        ) . '
        AND added_by = \'' . $testd[0] . '\'
        ' . $conf['order_by'] . '
        ;';
    $page = array_merge
      (
        $page, array
           (
            'title' => '<a href="' . duplicate_index_url(array('start' => 0)) . '">' . $me . '</a>',
            'items' => array_from_query($query, 'id'),
            )
      );
  }else{
    show_users_home();
  }
}else{
    show_users_home();
}

/*fin gestion URL*/

function show_users_home(){
 /*users cloud*/
 // template vars
 global $page, $conf, $user, $template;
 $me = 'user-';
 $page['section'] = $me;
 $page['section_title'] = '<a href="' . get_absolute_root_url() . '">' . l10n('Home') . '</a>' . $conf['level_separator'] . '<a href="' . get_absolute_root_url() . 'index.php?/' . $me . '">' . l10n('Users') . '</a>';        
 $userslistecloud1 = see_userlist_nb_photo();
 $level=see_nb_photo();
 if (pwg_db_num_rows($userslistecloud1)) {
    while ($userslistecloud = pwg_db_fetch_assoc($userslistecloud1)) {
      if($userslistecloud['PBU']>$level*4/5){$wheight=5;}
      else if($userslistecloud['PBU']>$level*3/5){$wheight=4;}
      else if($userslistecloud['PBU']>$level*2/5){$wheight=3;}
      else if($userslistecloud['PBU']>$level*1/5){$wheight=2;}
      else{$wheight=1;}
		$items = array(
			'USERSSPBYLID' => $userslistecloud['id'],
			'USERSSPBYL' => $userslistecloud['username'],
			'USERSSPBYLC' => $userslistecloud['PBU'],
			'USERSSPBYWEIGHT' => $wheight,
		);
      $template->append('userslistecloud1', $items);
      }
    }
    
  $linkusersliste = get_root_url() . 'index.php?/user-';
  $template->assign(array(
	'USERSSPBY'=> $linkusersliste,
	'USERSSPBYCOLOR' => $conf['see_photos_by_user_color'],
	'USERSSPBYSHAPE' => $conf['see_photos_by_user_shape'],
  ));
   
  if($conf['see_photos_by_user_show_user_home']==1){
	$template->assign(
      'gestionD', array(
      'SPBA_PATH'=> get_root_url() . SPBA_PATH,
	));
  }else if($conf['see_photos_by_user_show_user_home']==2){
    $template->assign(
	  'gestionC', array(
      'SPBA_PATH'=> get_root_url() . SPBA_PATH,
	));           
  }else if($conf['see_photos_by_user_show_user_home']==3){
    $groups = array();
    $result = see_userlist_nb_photo();
    while ($row = pwg_db_fetch_assoc($result)) {
      $groups[$row['id']] = $row['username'] . ' (' . $row['PBU'] . ')';
    }
    $selected = 0;
	  if (empty($testd[0])) {
		$options['b'] = l10n('select user');
	  } else {
		$options['b'] = l10n('select other user');
	  }
    $options['a'] = '----------------------';

    foreach ($groups as $metalist => $metalist2) {
      $options[$metalist] = $metalist2;
    }
    $template->assign(
      'gestionA', array(
      'OPTIONS' => $options,
      'SELECTED' => $selected
    ));
	if (isset($_POST['submitchoixauteur'])) {
	  $redirect_url = get_root_url() . 'index.php?/user-';
	  if(is_numeric($_POST['metalist'])){
		$query = 'SELECT id, username FROM ' . USERS_TABLE . ' WHERE id = \'' . $_POST['metalist'] . '\';';
		$result = pwg_query($query);
		$row = pwg_db_fetch_assoc($result);
		$username = $row['username'];
		$redirect_url .=($_POST['metalist']).'-'.$username;
	  }
	  redirect($redirect_url);
    } 
  }else{}
}

?>