<?php
defined('PREVNEXT_PATH') or die('Hacking attempt!');

global $txtnext;

function prevnext_add_button()
{
	global $template;
	global $page;
	global $txtprev, $txtnext, $mynext, $myprev;
	
	
	$txtpn = '';

	if (isset($page['is_homepage'])) {
		//echo  "Homepage";
	}else {
		 
  		if (isset($page['category']['id_uppercat'])) {
  			//echo "set";
  			$myupperid = $page['category']['id_uppercat'];
  			$myuppercats = $page['category']['uppercats'];
  				
  		} else {
  			$myupperid = $page['start'];
  			$myuppercats = $page['startcat'];
  				
  			//return;
  			//echo " not set"; 
  		}

  		$params['format'] = 'php';
  		$params['tree_output'] = false;
  		$params['cat_id']= $myupperid;
  		$params['recursive'] = false;
  		$params['public'] = false;
  		$params['fullname'] = false;
		$params['thumbnail_size'] = 'thumb';

	  	$txt = ws_categories_getList($params,$service);
	
  		if (isset($myupperid)) {
  			$currentcats = $txt['categories']->_content;
  			$p = 0;
  			$catid = -1;

  			foreach ($currentcats as $name) {
				$catid += 1;
  				if ((int)$name['id'] == (int)$myupperid)  { 
  					$p = 1; 
  					 
  				} else {
  				
  				}
  			
  				
  			if ($p == 1) {
  				$txtcurr = '';
  				$txtprev = '';
  				$txtnext = '';
  				
  				if ($page['category']['id'] == $name['id']) { // Found current Category

  					if($catid > 0 and $currentcats[$catid-1]['id'] <> $myupperid) {  // If there is a Previous cat, add it
  						$txtprev = 'Previous: <a href=' . $currentcats[$catid-1]['url'] . '>' . 
  						$currentcats[$catid-1]['name'] . "</a>" . '<span > </span>';
  				
  					
  					} else { 
  						$txtnext = 'No Prev Cat <br/>'; 
  					}
  					
  					// Add the current Category
  					$txtcurr .= '<b>Current: <a href=' . $currentcats[$catid-0]['url'] . '>' . 
  					$currentcats[$catid-0]['name'] . "</a></b><br/>";
  					
  					if (array_key_exists($catid+1, $currentcats)) {
  					  	$txtnext = 'Next: <a href=' . $currentcats[$catid+1]['url'] . '>' .
  					  	 $currentcats[$catid+1]['name'] . "</a>" . '<span> </span>';
  					} else { 

  					}
  					$p = 0;
  				}

  			}
  			
		}
		
		//echo $txtprev, $txtcurr, $txtnext;
  		$mynext = $txtnext;
  		$myprev = $txtprev;
  		
  	} else {
  		//// echo "One down from Top <br>";
  		
  		$currentcats = $txt['categories']->_content;
  		$p = 0;
  		//var_dump($currentcats);	
  		foreach ($currentcats as $name) {
  			$txtcurr = "";
  				
  			$txtcurr .= '<a href=' . $name['url'] . '>' . $name['name'] . "</a> <br>";
  			echo $txtcurr;
  		}
  	} 
  }

  //if (script_basename()=='index')
  //{
  	/*
  		$template->add_index_button($prevbutton, BUTTONS_RANK_NEUTRAL);
  	$template->add_index_button($nextbutton, BUTTONS_RANK_NEUTRAL);
  	*/
  	$template->add_index_button($myprev,BUTTONS_RANK_NEUTRAL);
  	$template->add_index_button($mynext,BUTTONS_RANK_NEUTRAL);
  	 
  	//$template->add_index_button($button, BUTTONS_RANK_NEUTRAL);
  //}
  //else
  //{
  	//$template->add_picture_button($button, BUTTONS_RANK_NEUTRAL);
  //}
  
}

?>
