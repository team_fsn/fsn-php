<?php

$lang['Add a 3D photo'] = 'Ajouter une image 3D';
$lang['Add a 3D video'] = 'Ajouter une vidéo 3D';
$lang['Batch mode'] = 'Mode multiple';
$lang['3D photo Uploader'] = 'Transfert de photo 3D';
$lang['Choose a file'] = 'Choisissez un fichier';
$lang['3D File to upload'] = 'Fichier 3D à transférer';
$lang['3D file max filesize'] = 'Taille max du fichier 3D';

$lang['3D video Uploader'] = 'Transfert de vidéo 3D';
$lang['Choose a thumbnail'] = 'Choisissez une miniature';
$lang['Thumbnail to upload'] = 'Miniature à tranférer';
$lang['Let the plugin do the job or use a personnal image'] = 'Laissez le plugin faire le travail ou choisissez une image personnelle';

$lang['3D Batch Mode Uploader'] = 'Transfert 3D en mode multiple';
$lang['Choose files'] = 'Choisissez les fichiers';
$lang['3D Files to upload'] = 'Fichiers 3D à transférer';
$lang['You cannot transfer more than '] = 'Vous ne pouvez pas transférer plus de ';
$lang['files due to php.ini parameters.'] = 'fichiers à cause des limitations de php.ini';

$lang['Common ThreeD configuration'] = 'Configuration de base de ThreeD';
$lang['Cast this picture'] = 'Diffuser cette image';
?>