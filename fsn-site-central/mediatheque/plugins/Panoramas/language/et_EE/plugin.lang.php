<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Pildi nime alamosa mida näidatakse režhiimis 180 [ != eelmine alamosa]';
$lang['Picture name substring to display in Mode 360'] = 'Pildi nime alamosa mida näidatakse režhiimis 360';
$lang['Picture name substrings must be different.'] = 'Pildi nime alamosa peab olema erinev';
$lang['Remove substring from picture title'] = 'Eemalda pildi nimest alamosa ';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Sinu seadistusi ei salvestatud ülal kirjeldatud põhjustel.';
$lang['Your configuration is saved.'] = 'Seadistused on salvestatud.';
$lang['authors of'] = 'autorid';
$lang['Viewport width ']['% between 50-100'] = 'Vaateakna laius [% 50-10 vahel]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Vaateakna suhe on piirkonnast väljas (Õige vahemik 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Sul ei ole õigusi antud seadistusi muuta (ainult administraatoril).';
$lang['Viewport minimal width ']['pixels > 320'] = 'Vaateakna minimaalne laius [Pixel>320]';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Vaateakna maximaalnelaius [Pixel>=miniaalne laius]';
$lang['Right'] = 'Parem';
$lang['Rotation direction'] = 'Keeramise suund';
$lang['Special thanks to:'] = 'Erilised tänud:';
$lang['Start position ']['% between 0-99'] = 'Algus positsioon [% 0-99 vahel]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Algus positsiooni suhe on piirkonnast väljas (Õige vahemik: 0-99)';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Maksimaalne vaateakna laius on mõõdust väljas (Lubatud vahemik: Minimaalne/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Minimaalne vaateakna laius on mõõdust väljas (Lubatus vahemik: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Ržhiim 180 (Alternatiivne vasak-parem restart)';
$lang['Relative speed factor ']['10-90'] = 'Suhteline kiiruse faktor [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Suhteline kiiruse faktor on lubatust väljas[Õige piirkond: 10-90]';
$lang['Auto'] = 'Automaatne';
$lang['Automatic start'] = 'Automaatne start';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Panoraami raami suurus on liiga suur (Õige valik: 0-10)';
$lang['Border color'] = 'Panoraami raami värv';
$lang['Border width'] = 'Panoraami raami suurus [0-10]';
$lang['Caption Color'] = 'Kirjelduse teksti värv';
$lang['Control display'] = 'Kontroll paneel';
$lang['Footer Color'] = 'Jaluse tausta värv';
$lang['Footer Control Color'] = 'Jaluse kontrolli värv';
$lang['Footer display'] = 'Jaluse paneel';
$lang['Left'] = 'Vasak';
$lang['Mode 360'] = 'Režhiim 360';
$lang['Off'] = 'Väljas';
$lang['On'] = 'Sees';
$lang['Panoramas configuration'] = 'Panoraami seadistused';
$lang['Permanent'] = 'Püsiv';
$lang['Picture name substring could not be empty.'] = 'Pildi nimi ei tohi sisaldata tühikut';
?>