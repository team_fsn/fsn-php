<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Auto'] = 'Automàtic';
$lang['Automatic start'] = 'Inici automàtic';
$lang['Control display'] = 'Control de pantalla';
$lang['Left'] = 'Esquerra';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Amplada màxima de Viewport fora de rang (rang correcte: Mínim/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Amplada mínima de Viewport fora de rang (rang correcte: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Mode 180 (Inici alternatiu dret-esquerre)';
$lang['Mode 360'] = 'Mode 360';
$lang['Off'] = 'Encendre';
$lang['On'] = 'Apagar';
$lang['Panoramas configuration'] = 'Configuració de Panoramas';
$lang['Permanent'] = 'Permanent';
$lang['Picture name substring could not be empty.'] = 'La subcadena del nom de la imatge no està buida.';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Subcadena del nom de la imatge a visualitzar en Mode 180 [!=subcadena anterior]';
$lang['Picture name substring to display in Mode 360'] = 'Subcadena nom d\'arxiu a visualitzar en Mode 360';
$lang['Picture name substrings must be different.'] = 'La subcadena nom d\'arxiu cal que sigui diferent';
$lang['Relative speed factor ']['10-90'] = 'Factor de velocitat relativa [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'El factor de velocitat relativa està fora de l\'interval (Rang correcte: 10-90)';
$lang['Remove substring from picture title'] = 'Eliminar subcadena de títol de la imatge';
$lang['Right'] = 'Dreta';
$lang['Rotation direction'] = 'Direcció de rotació';
$lang['Special thanks to:'] = 'Agraïments especials a:';
$lang['Start position ']['% between 0-99'] = 'Posició inicial [% entre 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'El rati de la posició inicial està fora de rang (Rang correcte: 0-99).';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Amplada màxima de Viewport [píxels >= amplada mínima]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Amplada mínima de Viewport [píxels >= 320]';
$lang['Viewport width ']['% between 50-100'] = 'Amplada Viewport [% entre 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'El rati d\'amplada està fora de rang (Rang correcte: 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'No està autoritzat per canviar aquesta configuració (només per a Webmaster).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'La seva configuració no ha sigut desada per les raons esmentades';
$lang['Your configuration is saved.'] = 'La seva configuració s\'ha desat correctament';
$lang['authors of'] = 'Autors de';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'La mida de la cantonada panoràmica està fora de rang (Rang correcte: 0-10)';
$lang['Border color'] = 'Color de la cantonada panoràmica';
$lang['Border width'] = 'Mida de la cantonada panoràmic';
$lang['Caption Color'] = 'Color del text descriptiu';
$lang['Footer Color'] = 'Color de fons del peu de pàgina';
$lang['Footer Control Color'] = 'Control del color del peu de pàgina';
$lang['Footer display'] = 'Visualització del peu de pàgina';
?>