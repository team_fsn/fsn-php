<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Du är inte behörig att ändra den här konfigurationen (endast Webmaster).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Din konfiguration har INTE sparats p.g.a. ovanstående fel.';
$lang['Footer Color'] = 'Sidfotens bakgrundsfärg';
$lang['Footer Control Color'] = 'Sidfotskontrollens färg';
$lang['Mode 180 (Alternative left-right restart)'] = '180-läge (motsatt rotationsriktning)';
$lang['Mode 360'] = '360-läge';
$lang['Relative speed factor ']['10-90'] = 'Relativ hastighet [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Relativ hastighet måste vara i spannet 10-90.';
$lang['Start position ']['% between 0-99'] = 'Startposition [% mellan 0-99]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Startpositionen måste vara i spannet 0-99.';
$lang['Your configuration is saved.'] = 'Din konfiguration sparades.';
$lang['Right'] = 'Höger';
$lang['Rotation direction'] = 'Rotationsriktning';
$lang['Special thanks to:'] = 'Extra tack till:';
$lang['Auto'] = 'Auto';
$lang['Automatic start'] = 'Automatisk start';
$lang['Caption Color'] = 'Beskrivningstextens färg';
$lang['Control display'] = 'Visa kontroller';
$lang['Footer display'] = 'Visa sidfot';
$lang['Left'] = 'Vänster';
$lang['Off'] = 'På';
$lang['On'] = 'Av';
$lang['Permanent'] = 'Permanent';
$lang['authors of'] = 'skapare av';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Panaramakantstorlek är utanför intervallet (Korrekt intervall: 0-10).';
$lang['Border color'] = 'Panoramakantfärg';
$lang['Border width'] = 'Panoramakantstorlek [0-10]';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Maximal bredd för visningsområde är utanför intervallet (Korrekt intervall: Minimal/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Minimal bredd för visningsområde är utanför intervallet (Korrekt intervall: 320/+).';
$lang['Panoramas configuration'] = 'Panoramakonfiguration';
$lang['Picture name substring could not be empty.'] = 'Bildnamnssubsträng kan inte vara tom';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Bildnamnssubsträng att visa i läge 180 [!=föregående substräng]';
$lang['Picture name substring to display in Mode 360'] = 'Bildnamnssubsträng att visa i läge 360';
$lang['Picture name substrings must be different.'] = 'Bildnamnssubsträngar måste vara olika';
$lang['Remove substring from picture title'] = 'Ta bort substräng från bildtitel';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Visningsområde maximal bredd [pixels >= minimal bredd]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Visningsområde minimal bredd [pixels > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Visningsområde bredd [% mellan 50-100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Visningsområde breddförhållande är utanför intervallet (Korrekt intervall: 50-100).';
?>