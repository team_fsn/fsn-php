<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Automatic start'] = 'Automatische start';
$lang['Auto'] = 'Auto';
$lang['Control display'] = 'Beheer weergave';
$lang['Left'] = 'Links';
$lang['Mode 180 (Alternative left-right restart)'] = '180 mode (alternatieve links-rechts herstart)';
$lang['Mode 360'] = '360 mode';
$lang['Off'] = 'Uit';
$lang['On'] = 'Aan';
$lang['Panoramas configuration'] = 'Panoramas configuratie';
$lang['Permanent'] = 'Permanent';
$lang['Right'] = 'Rechts';
$lang['Rotation direction'] = 'Draairichting';
$lang['Special thanks to:'] = 'Speciale dank aan:';
$lang['Start position [% between 0-99]'] = 'Start positie';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Je configuratie is niet opgeslagen vanwege de volgende redenen';
$lang['Your configuration is saved.'] = 'Je configuratie is opgeslagen';
$lang['authors of'] = 'auteurs van';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'De maximale viewportbreedte is onjuist (moet groter dan de minimale waarde zijn).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'De minimale viewportbreedte is onjuist (moet groter dan 320 zijn).';
$lang['Picture name substring could not be empty.'] = 'Het deel van de afbeeldingsnaam mag niet leeg zijn.';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Het deel van de afbeeldingsnaam in mode 180 [moet ongelijk aan het vorige deel zijn]';
$lang['Picture name substring to display in Mode 360'] = 'Het deel van de afbeeldingsnaam in mode 360';
$lang['Picture name substrings must be different.'] = 'Het deel van de afbeeldingsnaam mag niet hetzelfde zijn.';
$lang['Relative speed factor ']['10-90'] = 'Relatieve snelheidsfactor [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Relatieve snelheidsfactor is onjuist (correcte waarde 10-90).';
$lang['Remove substring from picture title'] = 'Verwijder het deel van de naam van afbeelding';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Startpositieverhouding is onjuist (correcte waarde 0-99).';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Maximale viewportbreedte [pixels >= minimale breedte]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Minimale viewportbreedte [pixels > 320]';
$lang['Viewport width ']['% between 50-100'] = 'Viewportbreedte [% tussen 50 en 100]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Viewportbreedtepercentage onjuist (correcte verhouding 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Je bent niet geautoriseerd om deze configuratie te wijzigen (alleen de Webmaster mag dit).';
$lang['Caption Color'] = 'Tekstkleur omschrijving';
$lang['Footer Color'] = 'Voettekst achtergrond kleur';
$lang['Footer Control Color'] = 'Voettekst controle kleur';
$lang['Footer display'] = 'Voettekst weergave';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Ingevoerde Panorama randdikte ligt buiten bereik (Correct bereik: 0-10).';
$lang['Border color'] = 'Panorama randkleur';
$lang['Border width'] = 'Panorama randdikte (0-10)';
?>