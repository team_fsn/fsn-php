<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2012 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Automatic start'] = 'Otomatik başla';
$lang['Auto'] = 'Otomatik';
$lang['Control display'] = 'Kontrol ekranı';
$lang['Left'] = 'Sol';
$lang['Maximal Viewport width is out of range (Correct range: Minimal/+).'] = 'Maks. viewport genişliği aralık dışıdır (Doğru aralık: Min/+).';
$lang['Minimal Viewport width is out of range (Correct range: 320/+).'] = 'Min. viewport genişliği aralık dışıdır (Doğru aralık: 320/+).';
$lang['Mode 180 (Alternative left-right restart)'] = 'Kip 180 (Alternatif sol-sağ tekrar başlat)';
$lang['Mode 360'] = '360 Kipi';
$lang['Off'] = 'Kapalı';
$lang['On'] = 'Açık';
$lang['Panoramas configuration'] = 'Panorama yapılandırması';
$lang['Permanent'] = 'Kalıcı';
$lang['Picture name substring could not be empty.'] = 'Resim ismi alt yazısı boş olamaz.';
$lang['Picture name substring to display in Mode 180 '][' != previous substring '] = 'Kip 180 \'de gösterilecek resim ismi alt yazısı [!=önceki alt yazı]';
$lang['Picture name substring to display in Mode 360'] = 'Kip 360 \'da gösterilecek resim ismi alt yazısı ';
$lang['Picture name substrings must be different.'] = 'Resim ismi alt yazısı değişik olmalı.';
$lang['Relative speed factor ']['10-90'] = 'Bağıl hız faktörü [10-90]';
$lang['Relative speed factor is out of range (Correct range: 10-90).'] = 'Bağıl hız faktörü aralık dışı [10-90]';
$lang['Remove substring from picture title'] = 'Resim isminden alt yazıyı çıkart';
$lang['Right'] = 'Sağ';
$lang['Rotation direction'] = 'Döndürme yönü';
$lang['Special thanks to:'] = 'Bu kişi özel teşekkürü hak ediyor:';
$lang['Start position ']['% between 0-99'] = 'Başlangıç konumu [%0-99 arası]';
$lang['Start position ratio is out of range (Correct range: 0-99).'] = 'Başlangıç konumu aralık dışında (Doğrusu 0-99 olmalı)';
$lang['Viewport maximal width ']['pixels >= minimal width'] = 'Viewport maks. genişliği [pikseller>=min.genişlik]';
$lang['Viewport minimal width ']['pixels > 320'] = 'Viewport min. genişliği [pikseller>320]';
$lang['Viewport width ']['% between 50-100'] = 'Viewport genişliği [% 50-100 arasında]';
$lang['Viewport width ratio is out of range (Correct range: 50-100).'] = 'Viewport genişlik oranı aralık dışıdır (Correct range: 50-100).';
$lang['You are not authorized to change this configuration (Webmaster only).'] = 'Yapılandırmayı değiştirme yetkiniz bulunmuyor (Sadece yönetici).';
$lang['Your configuration is NOT saved due to above reasons.'] = 'Yukarıdaki sebeplerden dolayı yapılandırmanız kaydedilmedi.';
$lang['Your configuration is saved.'] = 'Yapılandırma ayarlarınız kaydedildi.';
$lang['authors of'] = 'yazarlar';
$lang['Footer display'] = 'Altbilgi görüntüle';
$lang['Footer Control Color'] = 'Altbilgi kontrol rengi';
$lang['Footer Color'] = 'Altbilgi arkaplan rengi';
$lang['Caption Color'] = 'Açıklama yazısı rengi';
$lang['Border width'] = 'Panaromik çerçeve büyüklüğü [0-10]';
$lang['Border color'] = 'Panaromik çerçeve rengi';
$lang['Border Width ratio is out of range (Correct range: 0-10).'] = 'Panaromik çerçeve büyüklüğü aralık dışı (Doğru aralık: 0-10)';
?>