<p class="file_uploader_help_title">
	{'Help'|@translate}
</p>
<p class="file_uploader_help_content">
	{'The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'|@translate}<br />
	{'The Configuration tab allows you to customize the plugin in two ways:'|@translate}
</p>
	<ul>
		<li>{'Open files in a new tab'|@translate}{': Check to open files in a new tab or download them depending on the extension.'|@translate}</li>
		<li>{'Overwrite files without notice'|@translate}{': Check to overwrite files during upload if a file with the same name already exists.'|@translate}</li>
	</ul>
<p class="file_uploader_help_content">
	{'You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'|@translate}
</p>
<p class="file_uploader_help_title">
	{'Donate'|@translate}
</p>
<p class="file_uploader_help_content">
	{'Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'|@translate}
</p>
<p class="file_uploader_center">
	<a href="http://fr.piwigo.org/donate" target="_blank" alt="http://fr.piwigo.org/donate">http://www.piwigo.org/donate</a>
</p>