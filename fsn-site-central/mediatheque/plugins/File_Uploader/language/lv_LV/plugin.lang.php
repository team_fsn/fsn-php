<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['Choose a file'] = 'Izvēlaties failu';
$lang['Album:'] = 'Albūms:';
$lang['... or '] = '... vai';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo ir bezmaksas aplikācija (atvērtā koda programmatūra). Varat izmantot to par brīvu un tā tas būs arī turpmāk. Ziedojumus vāc Piwigo Fonds (bezpeļņas organizācija). Tie tiek izlietoti projekta izmaksu segšanai.';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'Augšupielādes cilne dod iespēju augšupielādēt jūsu "neattēla" failus jūsu galerijā. Aizpildiet formu, lai augšupielādētu un sinhronizētu failu. ';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'Varat lietot savu personīgo attēlu vai konvertēt savu failu attēlā izmantojot esošu programmu vai web servisu, piemēram  ';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'Jūs varat lietot šo spraudni, lai pievienotu ikonu jūsu "neattēla" failiem, kopā ar spraudni <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a>.';
$lang['file_uploader_error_already_exist'] = 'Fails ar šādu nosaukumu jau eksistē. Jums jāpārsauc šis fails. Ja vēlaties pārrakstīt failu, modificējiet konfigurāciju konfigurācijas cilnē.';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Ievadiet paplašinājumus, pēc kuriem spraudnis atvērs failus jaunā cilnē vai lejupielādēs. Paplašinājumi nav reģisrjutīgi.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ':Veiciet atzīmi, lai pārrakstītu failus augšupielādes laikā, ja jau eksistē faili ar tādu pašu nosaukumu.';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ':Veiciet atzīmi, lai atvērtu failus jaunā cilnē vai lejupielādējiet tos atkarībā no paplašinājuma.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'Konfigurācijas cilne dod iespēju pielāgot spraudni divos veidos:';
$lang['file_uploader_pdf2tab_hint_text'] = 'Ievadiet paplašinājumu';
$lang['file_uploader_upload_max_filesize'] = 'Jūsu serveris ir noteicis maksimālo faila lielumu kā';
$lang['file_uploader_pdf2tab_tooltip'] = 'Atvērt failu jaunā cilnē';
$lang['create a new album'] = 'izveidot jaunu albūmu';
$lang['Type of file not supported'] = 'Faila tisps nav atbalsīts';
$lang['Unable to create folder '] = 'Nav iespējams izveidot mapi';
$lang['Upload error'] = 'Augšupielādes kļūda';
$lang['Upload'] = 'Augšupielāde';
$lang['The thumbnail must be a picture'] = 'Sīkbildei jābūt attēlam';
$lang['There have been errors. See below'] = 'Ir iespējamas kļūdas. Skatīt zemāk';
$lang['Thumbnail to upload:'] = 'Augšupielādējamā sīkbilde:';
$lang['Title:'] = 'Nosaukums:';
$lang['Properties'] = 'Rekvizīti';
$lang['Can\'t upload file to galleries directory'] = 'Nevar augšupielādēt failu galerijas direktorijā';
$lang['Specify a thumbnail for your file'] = 'Izvēlaties sīkbildi savam failam';
$lang['Specify a file to upload'] = 'Izvēlaties failu augšupielādei';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'Fails pārsniedz upload_max_filesize direktīvu php.ini ';
$lang['File upload stopped by extension'] = 'Faila augšupielāde apturēta dēļ paplašinājuma';
$lang['File uploaded and synchronized'] = 'Fails ir augšupielādēts un sinhronizēts';
$lang['File was only partially uploaded'] = 'Fails ir tikai daļēji augšupielādēts';
$lang['Overwrite files without notice'] = 'Pārrakstīt failus bez ziņojuma';
$lang['Open files in a new tab'] = 'Atvērt failus jaunā cilnē';
$lang['No file to upload'] = 'Nav augšupielādējamā faila';
$lang['Missing a temporary folder'] = 'Iztrūkst pagaidu mape';
$lang['Invalid file name'] = 'Nekorekts faila nosaukums';
$lang['Help'] = 'Palīdzība';
$lang['Failed to write file to disk'] = 'Faila ierakstīšana diskā neizdevās';
$lang['Choose a thumbnail'] = 'Izvēlaties sīkbildi';
$lang['File Uploader Plugin'] = 'Failu augšupielādētāja Spraudnis';
$lang['File to upload:'] = 'Augšupielādējamais fails:';
$lang['Donate'] = 'Ziedot';
$lang['Configuration'] = 'Konfigurācija';
?>