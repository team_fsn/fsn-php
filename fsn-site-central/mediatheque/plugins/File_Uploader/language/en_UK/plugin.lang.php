<?php
$lang['File Uploader Plugin'] = 'File Uploader Plugin';

//admin.php
$lang['Upload'] = 'Upload';
$lang['Configuration'] = 'Configuration';
$lang['Help'] = 'Help';

//admin_upload.php
$lang['Specify a file to upload'] = 'Specify a file to upload';
$lang['Specify a thumbnail for your file'] = 'Specify a thumbnail for your file';
$lang['Unable to create folder '] = 'Unable to create folder ';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = 'File exceeds the upload_max_filesize directive in php.ini';
$lang['File was only partially uploaded'] = 'File was only partially uploaded';
$lang['No file to upload'] = 'No file to upload';
$lang['Missing a temporary folder'] = 'Missing a temporary folder';
$lang['Failed to write file to disk'] = 'Failed to write file to disk';
$lang['File upload stopped by extension'] = 'File upload stopped by extension';
$lang['Upload error'] = 'Upload error';
$lang['Type of file not supported'] = 'Type of file not supported';
$lang['file_uploader_error_already_exist'] = 'File with this name already exists. You should rename this file. If you want to overwrite the file, modify the configuration in the configuration tab.';
$lang['Invalid file name'] = 'Invalid file name';
$lang['Can\'t upload file to galleries directory'] = 'Can\'t upload file to galleries directory';
$lang['The thumbnail must be a picture'] = 'The thumbnail must be a picture';
$lang['File uploaded and synchronized'] = 'File uploaded and synchronized';
$lang['There have been errors. See below'] = 'There have been errors. See below';

//admin_upload.tpl
$lang['Choose a file'] = 'Choose a file';
$lang['File to upload:'] = 'File to upload:';
$lang['file_uploader_upload_max_filesize'] = 'Your server impose a maximum file size of';
$lang['Choose a thumbnail'] = 'Choose a thumbnail';
$lang['Thumbnail to upload:'] = 'Thumbnail to upload:';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'You can use a personnal image or convert your file to an image using a desktop program or a web service such as';
$lang['Properties'] = 'Properties';
$lang['Album:'] = 'Album:';
$lang['Title:'] = 'Title:';
$lang['... or '] = '... or ';
$lang['create a new album'] = 'create a new album';

//admin_configuration.tpl
$lang['Open files in a new tab'] = 'Open files in a new tab';
$lang['Overwrite files without notice'] = 'Overwrite files without notice';
$lang['file_uploader_pdf2tab_supported_extensions_description'] = 'Type in the extensions the plugin will open in new tabs or download. The extensions are case insensitive.';
$lang['file_uploader_pdf2tab_hint_text'] = 'Type in an extension';

//admin_help.tpl
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = 'The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = 'The Configuration tab allows you to customize the plugin in two ways:';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = ': Check to open files in a new tab or download them depending on the extension.';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = ': Check to overwrite files during upload if a file with the same name already exists.';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = 'You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.';
$lang['Donate'] = 'Donate';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.';


//pdf2tab
//picture.php
$lang['file_uploader_pdf2tab_tooltip'] = 'Open the file in a new tab';
?>