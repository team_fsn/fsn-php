<?php
// +-----------------------------------------------------------------------+
// | Piwigo - a PHP based photo gallery                                    |
// +-----------------------------------------------------------------------+
// | Copyright(C) 2008-2013 Piwigo Team                  http://piwigo.org |
// | Copyright(C) 2003-2008 PhpWebGallery Team    http://phpwebgallery.net |
// | Copyright(C) 2002-2003 Pierrick LE GALL   http://le-gall.net/pierrick |
// +-----------------------------------------------------------------------+
// | This program is free software; you can redistribute it and/or modify  |
// | it under the terms of the GNU General Public License as published by  |
// | the Free Software Foundation                                          |
// |                                                                       |
// | This program is distributed in the hope that it will be useful, but   |
// | WITHOUT ANY WARRANTY; without even the implied warranty of            |
// | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU      |
// | General Public License for more details.                              |
// |                                                                       |
// | You should have received a copy of the GNU General Public License     |
// | along with this program; if not, write to the Free Software           |
// | Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, |
// | USA.                                                                  |
// +-----------------------------------------------------------------------+
$lang['file_uploader_pdf2tab_supported_extensions_description'] = '输入在新标签页中打开还是下载的文件扩展名。文件扩展名不区分大小写。';
$lang[': Check to open files in a new tab or download them depending on the extension.'] = '：勾选以根据文件扩展名决定在新标签页中打开文件还是下载。';
$lang['file_uploader_pdf2tab_hint_text'] = '输入文件扩展名';
$lang['file_uploader_pdf2tab_tooltip'] = '在新标签页中打开文件';
$lang['file_uploader_upload_max_filesize'] = '您的服务器限制文件最大值为';
$lang[': Check to overwrite files during upload if a file with the same name already exists.'] = '：勾选以在已有同名文件存在时覆盖之';
$lang['Can\'t upload file to galleries directory'] = 'Can\'t upload file to galleries directory';
$lang['File upload stopped by extension'] = 'File upload stopped by extension';
$lang['File was only partially uploaded'] = '文件仅被部分上传';
$lang['Missing a temporary folder'] = 'Missing a temporary folder';
$lang['Properties'] = '属性';
$lang['Specify a file to upload'] = '指定一个上传文件';
$lang['Specify a thumbnail for your file'] = '为您的文件指定一个缩略图';
$lang['The Configuration tab allows you to customize the plugin in two ways:'] = '设置页允许您以两种方式定制此插件：';
$lang['The Upload tab allows you to upload "non picture" files to your gallerie. Fill in the form to upload and synchronize your file.'] = '上传页允许您上传非图片文件。填写表格以上传并同步您的文件。';
$lang['The thumbnail must be a picture'] = '缩略图必须是图片文件';
$lang['There have been errors. See below'] = '出错。如下：';
$lang['Thumbnail to upload:'] = '待上传的缩略图：';
$lang['Title:'] = '标题：';
$lang['Type of file not supported'] = '不支持此文件类型';
$lang['Unable to create folder '] = '无法创建文件夹';
$lang['Upload'] = '上传';
$lang['Upload error'] = '上传出错';
$lang['You can use a personnal image or convert your file to an image using a desktop program or a web service such as'] = 'You can use a personnal image or convert your file to an image using a desktop program or a web service such as';
$lang['You can use this plugin with the plugin <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> to add an icon to your "non picture" files.'] = '您可以使用 <a href="http://fr.piwigo.org/ext/extension_view.php?eid=654">Media Icon</a> 插件为您的非图片文件添加图标。';
$lang['create a new album'] = '创建一个新相册';
$lang['file_uploader_error_already_exist'] = '已存在同名文件。您需要重命名此文件。如果您想覆盖文件，请在设置页进行修改。';
$lang['Piwigo is a free software (opensource software), you can use it for free and this is not going to change. Donations are collected by the Piwigo Foundation (non profit organization) to cover project fees.'] = 'Piwigo 是一个免费程序（开源程序），您可免费使用，这不会有变。捐赠由 Piwigo 基金（非营利组织）接收，用作项目开销。';
$lang['Overwrite files without notice'] = '不提示，直接覆盖文件';
$lang['Open files in a new tab'] = '在新标签页中打开文件';
$lang['No file to upload'] = '没有待上传的文件';
$lang['Album:'] = '相册：';
$lang['Choose a file'] = '选择一个文件';
$lang['Choose a thumbnail'] = '选择一个缩略图';
$lang['Configuration'] = '设置';
$lang['Donate'] = '捐赠';
$lang['Failed to write file to disk'] = '文件写入失败';
$lang['File Uploader Plugin'] = 'File Uploader 插件';
$lang['File exceeds the upload_max_filesize directive in php.ini'] = '文件超过了php.ini中upload_max_filesize指令的限制';
$lang['File to upload:'] = '要上传的文件：';
$lang['File uploaded and synchronized'] = '文件已上传并同步';
$lang['Help'] = '帮助';
$lang['Invalid file name'] = '无效的文件名';
$lang['... or '] = '... 或';
?>