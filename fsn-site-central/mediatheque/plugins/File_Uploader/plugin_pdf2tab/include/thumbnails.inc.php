<?php
add_event_handler('loc_end_index_thumbnails', 'file_uploader_pdf2tab_thumbnails');

function file_uploader_pdf2tab_thumbnails($tpl_thumbnails_var) {
	global $template, $conf;
	
	$conf_file_uploader = unserialize($conf['file_uploader']);
	
	$template->set_prefilter('index_thumbnails', 'file_uploader_pdf2tab_add_tag_thumbnails');
	
	$template->assign('file_uploader_pdf2tab_supported_extensions', $conf_file_uploader['pdf2tab_extensions']);
	$template->set_filename('file_uploader_pdf2tab_template_thumbnails', FILE_UPLOADER_PDF2TAB_ABSOLUTE_PATH.'template/thumbnails.tpl');
	$template->concat('PLUGIN_INDEX_CONTENT_END', $template->parse('file_uploader_pdf2tab_template_thumbnails', true));
	
	return $tpl_thumbnails_var;
}

function file_uploader_pdf2tab_add_tag_thumbnails($content, &$smarty) {
	global $template;

	$search = 'class="thumbnail"';
	
	$replacement = 'class="thumbnail" file_uploader_pdf2tab="{$thumbnail.path}"';
	$content= str_replace($search, $replacement, $content);

	return $content;
}
?>