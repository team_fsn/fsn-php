<?php
if (!defined('PHPWG_ROOT_PATH')) die('Hacking attempt!');

function plugin_install() {
	require_once('install/functions.inc.php');
	require('install/config_default.inc.php');

	file_uploader_install($file_uploader_default_config);
	file_uploader_folder($file_uploader_galleries_folder, $file_uploader_galleries_dir, $file_uploader_galleries_folder_name);
}

function plugin_activate() {
	require_once('install/functions.inc.php');
	require('install/config_default.inc.php');
	
	file_uploader_update_db($file_uploader_default_config);
	file_uploader_folder($file_uploader_galleries_folder, $file_uploader_galleries_dir, $file_uploader_galleries_folder_name);
}

function plugin_uninstall() {
	require_once('install/functions.inc.php');

	file_uploader_delete_conf("file_uploader");
}

?>