<?php

class Form_Conteneur extends Yab_Form {

	public function __construct(Model_Conteneur $conteneur) {

		$this->set('method', 'post')->set('name', 'form_conteneur')->set('action', '')->set('role', 'form');

		$this->setElement('nom', array(
			'type' => 'text',
			'id' => 'nom',
			'label' => 'Nom',
			'value' => $conteneur->has('nom') ? $conteneur->get('nom') : null,
			'validators' => array('NotEmpty'),
			'errors' => array('NotEmpty'),
		));

		$this->setElement('description', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'description',
			'label' => 'description',
			'value' => $conteneur->has('description') ? $conteneur->get('description') : null,
			'validators' => array(),
			'errors' => array(),
		));
		/*
		$this->setElement('codebarre', array(
			'type' => 'text',
			'id' => 'codebarre',
			'label' => 'Code-barres',
			'value' => $conteneur->has('codebarre') ? $conteneur->get('codebarre') : null,
			'validators' => array(),
			'errors' => array(),
		));
		*/

		$this->setElement('entiteadmin_id', array(
				'type' => 'hidden',
				'id' => 'entiteadmin_id',
				'label' => 'Entité administrative',
				'value' => $conteneur->has("entiteadmin_id")?$conteneur->get("entiteadmin_id"):null,
				'options' => $conteneur->getTable('Model_Fsn_Entiteadmin')->getEntiteAdministrativeDetail()->setKey('id')->setValue('organisme_nom'),
				'validators' => array(),
				'errors' => array(),
		));
		
		$this->setElement('emplacement_id', array(
				'type' => 'hidden',
				'id' => 'emplacement_id',
				'label' => 'Emplacement',
				'value' => $conteneur->has('emplacement_id') ? $conteneur->get('emplacement_id') : Yab_Loader::getInstance()->getSession()->get('session')['entiteadmin_id'],
				'validators' => array('NotEmpty'),
				'errors' => array('NotEmpty'),
		));
		
		/*$this->setElement('emplacement_id', array(
			'type' => 'select',
			'id' => 'emplacement_id',
			'label' => 'emplacement_id',
			'value' => $conteneur->has('emplacement_id') ? $conteneur->get('emplacement_id') : null,
			'class'=>'white select_style',
			'fake_options' => array(),
			'options' => $conteneur->getTable('Model_Emplacement')->fetchAll()->setKey('id')->setValue('nom'),
			'errors' => array(),
		));*/
		
		$this->setElement('type_id', array(
				'type' => 'select',
				'id' => 'type_id',
				'label' => 'Type de conteneur',
				'value' => $conteneur->has('type_id') ? $conteneur->get('type_id') : null,
				'fake_options' => array(),
				'options' => $conteneur->getTable('Model_Fsn_Categorie')->fetchAll()->where("'conteneur_type'=categorie_type") ->setKey('id')->setValue('categorie_value'),
				'errors' => array(),
		));

	}

}