<?php

class Form_Elementpoterie extends Yab_Form {

	public function __construct(Model_Elementpoterie $elementpoterie, $mode='add') {

		$loader = Yab_Loader::getInstance();
		
		$this->set('method', 'post')->set('name', 'form_elementpoterie')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$registry = $loader->getRegistry();
		$i18n = $registry->get('i18n');
		
		$this->setElement('caracterepate_id', array(
			'type' => 'select',
			'id' => 'caracterepate_id',
			'label' => $i18n->say('caracterepate_id'),
			'value' => $elementpoterie->has('caracterepate_id') ? $elementpoterie->get('caracterepate_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementpoterie->getTable('Model_Caracterepate')->fetchAll()->setKey('id')->setValue('caracterepate'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('forme_id', array(
			'type' => 'select',
			'id' => 'forme_id',
			'label' => $i18n->say('forme_id'),
			'value' => $elementpoterie->has('forme_id') ? $elementpoterie->get('forme_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementpoterie->getTable('Model_Forme')->fetchAll()->setKey('id')->setValue('description'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('classification_id', array(
			'type' => 'select',
			'id' => 'classification_id',
			'label' => $i18n->say('classification_id'),
			'value' => $elementpoterie->has('classification_id') ? $elementpoterie->get('classification_id') : null,
				'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementpoterie->getTable('Model_Classification')->fetchAll()->setKey('id')->setValue('classification'),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('production_id', array(
			'type' => 'select',
			'id' => 'production_id',
			'label' => $i18n->say('production_id'),
			'value' => $elementpoterie->has('production_id') ? $elementpoterie->get('production_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			'options' => $elementpoterie->getTable('Model_Production')->fetchAll()->setKey('id')->setValue('production'),
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('decor', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'decor',
			'label' => $i18n->say('decor'),
			'value' => $elementpoterie->has('decor') ? $elementpoterie->get('decor') : null,
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('traceusage', array(
			'type' => 'textarea',
      		'rows' => '3',
			'id' => 'traceusage',
			'label' => $i18n->say('traceusage'),
			'value' => $elementpoterie->has('traceusage') ? $elementpoterie->get('traceusage') : null,
			'validators' => array(),
			'errors' => array(),
			'needed' => false,
		));

		$this->setElement('proportionconserve', array(
			'type' => 'text',
			'id' => 'proportionconserve',
			'label' => $i18n->say('proportionconserve'),
			'value' => $elementpoterie->has('proportionconserve') ? $elementpoterie->get('proportionconserve') : null,
			'validators' => array('Float'),
			//'errors' => array(),
			'needed' => false,
		));

		$production = $elementpoterie->has('production_id') ? $elementpoterie->get('production_id') : null;
		
		$relations  = new Model_Fsn_Categorie_Relation();
		
		$this->setElement('precisionproduction_id', array(
			'type' => 'select',
			'id' => 'precisionproduction_id',
			'label' => $i18n->say('precisionproduction_id'),
			'value' => $elementpoterie->has('precisionproduction_id') ? $elementpoterie->get('precisionproduction_id') : null,
			'fake_options' => array(null => 'Non déterminé'),
			 //'options' => array(),
			// 'options' => $elementpoterie->has('production_id') ? $elementpoterie->getTable('Model_Precisionproduction')->fetchAll()->where($where)->setKey('id')->setValue('precisonproduction') : array(),
			'options' => $elementpoterie->has('production_id') ? $relations->getListRelations()->where('fcb.id = "'.$production.'"')->setKey('element_a_id')->setValue('element_a_value') : array(),
			'errors' => array(),
			'needed' => false,
		));
		
		if($mode == 'show'){
			foreach($this->getElements() as $element) {
		
				if ($element->get('type') == 'select' or $element->get('type') == 'checkbox')
					$element->set('disabled',true);
				else
					$element->set('readonly','readonly');
			}
		
		}

	}

}