<?php

class Form_Couchelog extends Yab_Form {

	public function __construct(Model_Couchelog $couchelog, $mode = 'add') {
		$this->set('method', 'post')->set('name', 'form_couchelog')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$loader = Yab_Loader::getInstance() ;
		$registry = $loader -> getRegistry();
		$session = $loader ->getSession();

		// appel fichier internationalisation
		$i18n = $registry -> get('i18n');
		$log = new Model_Log($couchelog->has('log_id') ? $couchelog->get('log_id') : $session->get('log_id'));
		$filter_no_html = new Yab_Filter_NoHtml();

		$this->setElement('log_id', array(
				'type' => 'hidden',
				'id' => 'log_id',
				'label' => $filter_no_html->filter( $i18n -> say('log_title_show') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('log_title_show') ),
				'value' => $log->get('id'),
				'intitule' => $log -> get('identification'),
				'readonly' => true,
				'errors' => array(),
		));

		$this->setElement('id', array(
				'type' => 'hidden',
				'id' => 'id_couchelog',
				'value' => $couchelog->has('id') ? $couchelog->get('id') : null,
				'readonly' => true,
				'validators' => array('NotEmpty'),
				'errors' => array()
		));

		$this->setElement('num_sequence', array(
				'type' => 'text',
				'id' => 'num_sequence',
				'label' => $filter_no_html->filter( $i18n -> say('couchelog_num_sequence') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('couchelog_num_sequence') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('couchelog_num_sequence_tooltip') ),
				'value' => $couchelog->has('num_sequence') ? $couchelog->get('num_sequence') : $couchelog->getNextNumSequence($session->get('log_id')),
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('epaisseur', array(
				'type' => 'text',
				'id' => 'epaisseur',
				'label' => $filter_no_html->filter( $i18n -> say('epaisseur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('epaisseur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('epaisseur') ),
				'value' => $couchelog->has('epaisseur') ? $couchelog->get('epaisseur') : null,
				'validators' => array('NotEmpty'),
				'errors' => array(),
		));
		
		$this->setElement('couleur', array(
				'type' => 'text',
				'id' => 'couleur',
				'label' => $filter_no_html->filter( $i18n -> say('couleur') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('couleur') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('couleur') ),
				'value' => $couchelog->has('couleur') ? $couchelog->get('couleur') : null,
				'needed' => false,
				'validators' => array(),
				'errors' => array(),
		));

		$this->setElement('description', array(
				'type' => 'textarea',
				'rows' => '3',
				'id' => 'description',
				'label' => $filter_no_html->filter( $i18n -> say('description') ),
				'placeholder' => $filter_no_html->filter( $i18n -> say('description') ),
				'value' => $couchelog->has('description') ? $couchelog->get('description') : null,
				'validators' => array(),
				'errors' => array(),
		));
		
		$this->setElement('us_id', array(
				'type' => 'select',
				'id' => 'us_id',
				'label' => $filter_no_html->filter( $i18n -> say('us') ),
				'tooltip' => $filter_no_html->filter( $i18n -> say('us') ),
				'value' => $couchelog->has('us_id') ? $couchelog->get('us_id') : null,
				'options' => $couchelog->getTable('us')->fetchAll()->where('sitefouille_id="'.$session->get('sitefouille_id').'"')->setKey('id')->setValue('identification'),
				'fake_options' => array(null => 'Aucune US'),
				'validators' => array(),
				'errors' => array(),
				'needed' => false
		));

	}

}