<?php

class Form_Typetraitement extends Yab_Form {

	public function __construct(Model_Typetraitement $typetraitement) {

		$this->set('method', 'post')->set('name', 'form_typetraitement')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('type', array(
			'type' => 'text',
			'id' => 'type',
			'label' => 'type',
			'value' => $typetraitement->has('type') ? $typetraitement->get('type') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $typetraitement->has('visible') ? $typetraitement->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}