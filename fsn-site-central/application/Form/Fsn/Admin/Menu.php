<?php

class Form_Fsn_Admin_Menu extends Yab_Form {

	public function __construct(Model_Fsn_Admin_Menu $fsn_admin_menu) {

		$this->set('method', 'post')->set('name', 'form_fsn_admin_menu')->set('action', '')->set('role', 'form');

		$this->setElement('menu_name', array(
			'type' => 'text',
			'id' => 'menu_name',
			'label' => 'menu_name',
			'value' => $fsn_admin_menu->has('menu_name') ? $fsn_admin_menu->get('menu_name') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('menu_label', array(
			'type' => 'text',
			'id' => 'menu_label',
			'label' => 'menu_label',
			'value' => $fsn_admin_menu->has('menu_label') ? $fsn_admin_menu->get('menu_label') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('is_active', array(
			'type' => 'text',
			'id' => 'is_active',
			'label' => 'is_active',
			'value' => $fsn_admin_menu->has('is_active') ? $fsn_admin_menu->get('is_active') : '1',
			'validators' => array('Int'),
			'errors' => array(),
		));

		$this->setElement('html_href', array(
			'type' => 'text',
			'id' => 'html_href',
			'label' => 'html_href',
			'value' => $fsn_admin_menu->has('html_href') ? $fsn_admin_menu->get('html_href') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('html_title', array(
			'type' => 'text',
			'id' => 'html_title',
			'label' => 'html_title',
			'value' => $fsn_admin_menu->has('html_title') ? $fsn_admin_menu->get('html_title') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('html_target', array(
			'type' => 'text',
			'id' => 'html_target',
			'label' => 'html_target',
			'value' => $fsn_admin_menu->has('html_target') ? $fsn_admin_menu->get('html_target') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('html_action', array(
			'type' => 'text',
			'id' => 'html_action',
			'label' => 'html_action',
			'value' => $fsn_admin_menu->has('html_action') ? $fsn_admin_menu->get('html_action') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('html_class', array(
			'type' => 'text',
			'id' => 'html_class',
			'label' => 'html_class',
			'value' => $fsn_admin_menu->has('html_class') ? $fsn_admin_menu->get('html_class') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('php_controller', array(
			'type' => 'text',
			'id' => 'php_controller',
			'label' => 'php_controller',
			'value' => $fsn_admin_menu->has('php_controller') ? $fsn_admin_menu->get('php_controller') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('php_action', array(
			'type' => 'text',
			'id' => 'php_action',
			'label' => 'php_action',
			'value' => $fsn_admin_menu->has('php_action') ? $fsn_admin_menu->get('php_action') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('php_param', array(
			'type' => 'text',
			'id' => 'php_param',
			'label' => 'php_param',
			'value' => $fsn_admin_menu->has('php_param') ? $fsn_admin_menu->get('php_param') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('comment', array(
			'type' => 'text',
			'id' => 'comment',
			'label' => 'comment',
			'value' => $fsn_admin_menu->has('comment') ? $fsn_admin_menu->get('comment') : null,
			'validators' => array(),
			'errors' => array(),
		));

		$this->setElement('ordre', array(
			'type' => 'text',
			'id' => 'ordre',
			'label' => 'ordre',
			'value' => $fsn_admin_menu->has('ordre') ? $fsn_admin_menu->get('ordre') : null,
			'validators' => array('Int'),
			'errors' => array(),
		));

	}

}