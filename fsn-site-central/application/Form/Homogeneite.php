<?php

class Form_Homogeneite extends Yab_Form {

	public function __construct(Model_Homogeneite $homogeneite) {

		$this->set('method', 'post')->set('name', 'form_homogeneite')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('homogeneite', array(
			'type' => 'text',
			'id' => 'homogeneite',
			'label' => 'homogeneite',
			'value' => $homogeneite->has('homogeneite') ? $homogeneite->get('homogeneite') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $homogeneite->has('visible') ? $homogeneite->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}