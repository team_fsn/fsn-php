<?php

class Form_Contour_Sitefouille extends Yab_Form {

	public function __construct(Model_Contour_Sitefouille $contour_sitefouille) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form');

		$this->setElement('sitefouille_id', array(
			'type' => 'select',
			'id' => 'sitefouille_id',
			'label' => 'sitefouille_id',
			'value' => $contour_sitefouille->has('sitefouille_id') ? $contour_sitefouille->get('sitefouille_id') : null,
			'fake_options' => array(),
			'options' => $contour_sitefouille->getTable('Model_Sitefouille')->fetchAll()->setKey('id')->setValue('nom'),
			'errors' => array(),
		));

		$this->setElement('x', array(
			'type' => 'text',
			'id' => 'x',
			'label' => 'x',
			'value' => $contour_sitefouille->has('x') ? $contour_sitefouille->get('x') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('y', array(
			'type' => 'text',
			'id' => 'y',
			'label' => 'y',
			'value' => $contour_sitefouille->has('y') ? $contour_sitefouille->get('y') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('z', array(
			'type' => 'text',
			'id' => 'z',
			'label' => 'z',
			'value' => $contour_sitefouille->has('z') ? $contour_sitefouille->get('z') : null,
			'validators' => array('Float', 'NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('sequence', array(
			'type' => 'text',
			'id' => 'sequence',
			'label' => 'sequence',
			'value' => $contour_sitefouille->has('sequence') ? $contour_sitefouille->get('sequence') : null,
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}