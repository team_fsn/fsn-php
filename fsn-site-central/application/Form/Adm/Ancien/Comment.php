<?php

class Form_Adm_Ancien_Comment extends Yab_Form {

	public function __construct(Model_Adm_Ancien_Comment $adm_ancien_comment) {

		$this->set('method', 'post')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('table', array(
			'type' => 'text',
			'id' => 'table',
			'label' => 'table',
			'value' => $adm_ancien_comment->has('table') ? $adm_ancien_comment->get('table') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('ancien_commentaire', array(
			'type' => 'text',
			'id' => 'ancien_commentaire',
			'label' => 'ancien_commentaire',
			'value' => $adm_ancien_comment->has('ancien_commentaire') ? $adm_ancien_comment->get('ancien_commentaire') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('nouveau_commentaire', array(
			'type' => 'textarea',
      'rows' => '3',
			'id' => 'nouveau_commentaire',
			'label' => 'nouveau_commentaire',
			'value' => $adm_ancien_comment->has('nouveau_commentaire') ? $adm_ancien_comment->get('nouveau_commentaire') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('datechmnt', array(
			'type' => 'text',
			'id' => 'datechmnt',
			'label' => 'datechmnt',
			'value' => $adm_ancien_comment->has('datechmnt') ? $adm_ancien_comment->get('datechmnt') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('user_id', array(
			'type' => 'select',
			'id' => 'user_id',
			'label' => 'user_id',
			'value' => $adm_ancien_comment->has('user_id') ? $adm_ancien_comment->get('user_id') : null,
			'fake_options' => array(),
			'options' => $adm_ancien_comment->getTable('Model_User')->fetchAll()->setKey('id')->setValue('login'),
			'errors' => array(),
		));

		$this->setElement('user', array(
			'type' => 'text',
			'id' => 'user',
			'label' => 'user',
			'value' => $adm_ancien_comment->has('user') ? $adm_ancien_comment->get('user') : null,
			'validators' => array(),
			'errors' => array(),
		));

	}

}