<?php

class Form_Association_Er extends Yab_Form {

	public function __construct(Model_Association_Er $association_er) {

		$this->set('method', 'post')->set('name', 'form_association_er')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('typeasso_id', array(
			'type' => 'select',
			'id' => 'typeasso_id',
			'label' => 'typeasso_id',
			'value' => $association_er->has('typeasso_id') ? $association_er->get('typeasso_id') : null,
			'fake_options' => array(),
			'options' => $association_er->getTable('Model_Typeasso')->fetchAll()->setKey('id')->setValue('type'),
			'errors' => array(),
		));

	}

}