<?php

class Form_Oa extends Yab_Form {

    public function __construct(Model_Oa $oa) {

        // appel fichier internationalisation
        $registry = Yab_Loader::getInstance()->getRegistry();
        $i18n = $registry -> get('i18n');
        $filter_no_html = new Yab_Filter_NoHtml();

        $this->set('method', 'post')->set('name','form_oa')->set('action', '')->set('role', 'form') ->set('class', 'form-horizontal');
		
		$filter_html = new Yab_Filter_Html();
        $filter_html->set('format','%d/%m/%Y');
				
        $session = Yab_Loader::getInstance()->getSession();
		$sf = new Model_Sitefouille($oa->has('sitefouille_id') ? $oa->get('sitefouille_id') : $session->get('sitefouille_id'));
		
		$this->setElement('sitefouille_id', array(
			'type' => 'hidden',
			'id' => 'sitefouille_id',
			'label' => $filter_no_html->filter( $i18n -> say('sitefouille_id') ),
			'value' => $sf->get('id'),
			'intitule' => $sf->get('nomabrege')." - ".$sf->get('nom'),
			'validators' => array('NotEmpty'),
			'readonly' => true,
			'errors' => array(),
		));
        $this->setElement('identification', array(
            'type' => 'text',
            'id' => 'identification',
            'label' => $filter_no_html->filter( $i18n -> say('identification') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('identification') ),
            'value' => $oa->has('identification') ? $oa->get('identification') : null,
            'validators' => array('NotEmpty'),
            'errors' => array(),
        ));
        $this->setElement('arretedesignation', array(
            'type' => 'text',
            'id' => 'arretedesignation',
            'label' => $filter_no_html->filter( $i18n -> say('arretedesignation') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('arretedesignation') ),
            'value' => $oa->has('arretedesignation') ? $oa->get('arretedesignation') : null,
            'validators' => array(),
            'errors' => array(),
        ));
        $this->setElement('arreteprescription', array(
            'type' => 'text',
            'id' => 'arreteprescription',
            'label' => $filter_no_html->filter( $i18n -> say('arreteprescription') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('arreteprescription') ),
            'value' => $oa->has('arreteprescription') ? $oa->get('arreteprescription') : null,
            'validators' => array(),
            'errors' => array(),
        ));
        $this->setElement('debut', array(
            'type' => 'text',
            'id' => 'debut',
            'label' => $filter_no_html->filter($i18n -> say('debut')),
            'placeholder' => $i18n->say('date_placeholder_format'),
            'class' => 'form-control datepicker',
            'value' => $oa->has('debut') && $oa->get('debut') ? date('d/m/Y', strtotime($oa->get('debut'))) : null,
            'validators' => array('Date'=>array('format' => 'dd/mm/yyyy'), 'NotEmpty'),
            'errors' => array(),
        ));
        $this->setElement('fin', array(
            'type' => 'text',
            'id' => 'fin',
            'label' => $filter_no_html->filter( $i18n -> say('fin') ),
            'placeholder' => $i18n->say('date_placeholder_format'),
            'class' => 'form-control datepicker',
            'value' => $oa->has('fin') && $oa->get('fin') ? date('d/m/Y', strtotime($oa->get('fin'))) : null,
            'validators' => array('Date'=>array('format' => 'dd/mm/yyyy'), 'NotEmpty'),
            'errors' => array(),
        ));
        $this->setElement('oastatut_id', array(
            'type' => 'select',
            'id' => 'oastatut_id',
            'label' => $filter_no_html->filter( $i18n -> say('oastatut_id') ),
            'value' => $oa->has('oastatut_id') ? $oa->get('oastatut_id') : null,
            'fake_options' => array(null => 'Non déterminé'),
            'options' => $oa->getTable('Model_Oastatut')->fetchAll()->setKey('id')->setValue('oastatut'),
            'validators' => array('NotEmpty'),
            'errors' => array(),
			'needed' => false,
        ));
        $this->setElement('nature_id', array(
            'type' => 'select',
            'id' => 'nature_id',
            'label' => $filter_no_html->filter( $i18n -> say('nature_id') ),
            'value' => $oa->has('nature_id') ? $oa->get('nature_id') : null,
            'fake_options' => array(null => "Non déterminé"),
            'options' => $oa->getTable('Model_Nature')->fetchAll()->setKey('id')->setValue('nature'),
            'validators' => array('NotEmpty'),
            'errors' => array(),
			'needed' => false,
        ));
        $this->setElement('conditionfouille', array(
            'type' => 'textarea',
            'id' => 'conditionfouille',
            'label' => $filter_no_html->filter( $i18n -> say('conditionfouille') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('conditionfouille') ),
            'value' => $oa->has('conditionfouille') ? $oa->get('conditionfouille') : null,
            'validators' => array(),
            'errors' => array(),
        ));
        $this->setElement('organismeratachement', array(
            'type' => 'text',
            'id' => 'organismeratachement',
            'label' => $filter_no_html->filter( $i18n -> say('organismeratachement') ),
            'value' => $oa->has('organismeratachement') ? $oa->get('organismeratachement') : 'Unité d\'archéologie de la ville de Saint-Denis',
            'validators' => array(),
            'errors' => array(),
        ));
        $this->setElement('maitriseouvrage', array(
            'type' => 'text',
            'id' => 'maitriseouvrage',
            'label' => $filter_no_html->filter( $i18n -> say('maitriseouvrage') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('maitriseouvrage') ),
            'value' => $oa->has('maitriseouvrage') ? $oa->get('maitriseouvrage') : null,
            'validators' => array(),
            'errors' => array(),
        ));
        $this->setElement('surfacefouille', array(
            'type' => 'text',
            'id' => 'surfacefouille',
            'label' => $filter_no_html->filter( $i18n -> say('surfacefouille') ),
            'placeholder' => 200,
            'value' => $oa->has('surfacefouille') ? $oa->get('surfacefouille') : null,
            'validators' => array('Int'),
            'errors' => array(),
        ));
        $this->setElement('commentaire', array(
            'type' => 'textarea',
            'id' => 'commentaire',
            'label' => $filter_no_html->filter( $i18n -> say('commentaire') ),
            'placeholder' => $filter_no_html->filter( $i18n -> say('commentaire') ),
            'value' => $oa->has('commentaire') ? $oa->get('commentaire') : null,
            'validators' => array(),
            'errors' => array(),
        ));
    }
}