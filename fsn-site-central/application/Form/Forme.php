<?php

class Form_Forme extends Yab_Form {

	public function __construct(Model_Forme $forme) {

		$this->set('method', 'post')->set('name', 'form_forme')->set('action', '')->set('role', 'form')->set('class', 'form-horizontal');

		$this->setElement('description', array(
			'type' => 'text',
			'id' => 'description',
			'label' => 'description',
			'value' => $forme->has('description') ? $forme->get('description') : null,
			'validators' => array('NotEmpty'),
			'errors' => array(),
		));

		$this->setElement('visible', array(
			'type' => 'text',
			'id' => 'visible',
			'label' => 'visible',
			'value' => $forme->has('visible') ? $forme->get('visible') : '1',
			'validators' => array('Int', 'NotEmpty'),
			'errors' => array(),
		));

	}

}