<?php

class Filter_HumanArea extends Yab_Filter_Abstract {

  public function _filter($value) {

	  $units = array('mm', 'm', 'km') ;

    $start = array_search($this->get('unit'), $units);

    for($i = $start; $value > 1000; $i++)
      $value /= 1000;

		return round($value, 2).' '.$units[$i];

  }

}