<?php

class Controller_Consistance extends Yab_Controller_Action {

	public function actionIndex() {

		$consistance = new Model_Consistance();

		$consistances = $consistance->fetchAll();

		$this->_view->set('consistances', $consistances);
	}

	public function actionAdd() {

		$consistance = new Model_Consistance();

		$form = new Form_Consistance($consistance);

		if($form->isSubmitted() && $form->isValid()) {

			$consistance->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'consistance as been added');

			$this->forward('Consistance', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$consistance = new Model_Consistance($this->_request->getParams());

		$form = new Form_Consistance($consistance);

		if($form->isSubmitted() && $form->isValid()) {

			$consistance->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'consistance as been edited');

			$this->forward('Consistance', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$consistance = new Model_Consistance($this->_request->getParams());

		$consistance->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'consistance as been deleted');

		$this->forward('Consistance', 'index');

	}

}