<?php

class Controller_Troncon extends Yab_Controller_Action {

	public function actionIndex() {

        $session = $this->getSession();
		$troncon = new Model_Troncon();
        $unitesondage_id = null;
        $sf_id = $session->has("sitefouille_id")?$session->get("sitefouille_id"):null;
        $unitesondages = (new Model_Unitesondage())->getListunitesondage($sf_id);

        if (isset ( $_POST ["unitesondage_id"] )) {
            $unitesondage_id = $_POST ["unitesondage_id"];
            $session ["unitesondage_id"] = $unitesondage_id;
        } else if (isset ( $session ["unitesondage_id"] ))
            $unitesondage_id = $_SESSION ["unitesondage_id"];

	    $troncons = $troncon->getListTroncon($unitesondage_id);
        $this->_view->set('unitesondage_id', $unitesondage_id);
		$this->_view->set('troncons', $troncons);
		$this->_view->set('units',$unitesondages);
	}

	public function actionAdd() {
		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();
		$errors_messages = '';
		$mode = self::ACTION_MODE_CREATE;
		$troncon = new Model_Troncon();
		$form_add = new Form_Troncon($troncon, $mode);
		$form_add->setElement('numero_ident', array(
				'type' => 'text',
				'id' => 'numero_ident',
				'label' => $form_add->getElement('identification')->get('label'),
				'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_add->getElement('identification')->get('numero'),
				'validators' => array('NotEmpty','Int'),
				'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide'),
						'Int' => array('Ce champs doit être un entier')),
		));
		
		if($form_add->isValid() && isset($_POST['identification'])) {
			
			try {

				$formvalues = $form_add->getValues();
				unset($formvalues['numero_ident']);
				
				// Ajout du GUUID
				$generateGuuid = new Plugin_Guuid();
				$guuid = $generateGuuid -> GetUUID();
				//$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
				$formvalues['id'] = $guuid;
				$formvalues['identification'] = $form_add->getElement('identification')->get('value');
				
				$formvalues['date_debut'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_debut'])));
				$formvalues['date_fin'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_fin'])));

				//Controle date debut < fin		
				$date_debut = $formvalues['date_debut'];
				$date_fin = $formvalues['date_fin'];

				if($date_debut > $date_fin)
					$errors_messages['date_fin'] = $filter_no_html->filter( $i18n -> say('troncon_superieurDatation') );
					
				if (!empty($errors_messages)) throw new Exception();	

				$formvalues = $form_add -> setNullValues($formvalues);
				$troncon->populate($formvalues)->save();

                $registry = Yab_Loader::getInstance() -> getRegistry();
                $i18n = $registry -> get('i18n');
                $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');
                $this->getSession()->set('flash_message',str_replace("__ELEM__",$form_add->getRessourceType(),$i18n->say('added_message')));

			} catch(Exception $e){
				// $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning');
				// $this->getSession()->set('flash_message', '');
				// $this->getSession()->setFlashErrors($errors_messages);
			}
		}		

		$this->_view->set('form_add', $form_add);
		$this->_view->set('errors_messages', $errors_messages);

	}

	public function actionShow($id = null) {

		$mode = self::ACTION_MODE_SHOW;

		$session = $this->getSession();

		$params = $this->_request->getParams();
		$troncon_id = $params[0];

		$modeltroncon = new Model_Troncon( $this->_request->getParams () );
		$form = new Form_Troncon($modeltroncon, $mode);
		//$formvalues = $form->getValues();
		$id_troncon = $modeltroncon->get ('id');
		
		// la liste des US dans ce tronçon
		$liste_US = $modeltroncon->getListLinkedUS($id_troncon);

		$tranchee = null;
		$i18n = Yab_Loader::getInstance()->getRegistry()->get('i18n');

		$session = $this->getSession();
		$session['troncon_id'] = $troncon_id;

		$this->_view->set('form_show', $form);
		$this->_view->set('liste_us', $liste_US);

	}

	public function actionEdit() {

		// appel fichier internationalisation
		$registry = Yab_Loader::getInstance() -> getRegistry();
		$i18n = $registry -> get('i18n');
		$filter_no_html = new Yab_Filter_NoHtml();

		$errors_messages = '';

		$mode = self::ACTION_MODE_UPDATE;

		$params = $this->_request->getParams();
		$troncon_id = $params[0];

		$troncon = new Model_Troncon($params);
		$form = new Form_Troncon($troncon, $mode);
		
		$session = $this->getSession();
		$mUS = new Model_Us();
		$liste_us_liees = $troncon->getListLinkedUS($troncon_id);
		$liste_us = $troncon->getListNonLinkedUS($troncon_id, $session->get('sitefouille_id'));
		
		// echo "isSubmitted = ".$form->isSubmitted()."  isValid = ".$form->isValid()." empty(POST) = ".empty($_POST);

		if(!empty($_POST) && $form->isValid()) {
			try{
				$formvalues = $form->getValues();
				
				$formvalues['date_debut'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_debut'])));
				$formvalues['date_fin'] = date("Y-m-d", strtotime(str_replace('/', '-', $formvalues['date_fin'])));

				//Controle date debut < fin		
				$date_debut = $formvalues['date_debut'];
				$date_fin = $formvalues['date_fin'];

				if($date_debut > $date_fin)
					$errors_messages['date_fin'] = $filter_no_html->filter( $i18n -> say('troncon_superieurDatation') );

				if (!empty($errors_messages)) throw new Exception();

				$formvalues = $form->setNullValues($formvalues);
				$troncon->populate($formvalues)->save();

				if(isset($_POST["liste_us"])){
					$troncon->updateTronconUS($_POST["liste_us"], $troncon_id);
				}
				
				$this->getSession()->set('flash_title', '');
                $registry = Yab_Loader::getInstance() -> getRegistry();
                $i18n = $registry -> get('i18n');
                $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');
                $this->getSession()->set('flash_message',str_replace("__ELEM__",$form->getRessourceType(),$i18n->say('updated_message')));
				if($this->getSession()->has('unitesondage_id')) {
					$this->forward('Unitesondage', 'edit', array($this->getSession()->get('unitesondage_id')));
				} else {
					$this->forward('Unitesondage', 'index');
				}
			} catch(Exception $e){
				$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'warning'); 
				$this->getSession()->set('flash_message', '');
				$this->getSession()->setFlashErrors($errors_messages);
			}
		}

		$session = $this->getSession();
		$session['troncon_id'] = $troncon_id;

		$this->_view->set('form_edit', $form);
		$this->_view->set('liste_us', $liste_us);
		$this->_view->set('liste_us_liees', $liste_us_liees);

	}

	public function actionDelete() {

		$troncon = new Model_Troncon($this->_request->getParams());
		$form = new Form_Troncon($troncon);
		$formvalues = $form->getValues();

		$formvalues ['id'] = $troncon->get ('id');echo $formvalues['id'];

		$message = '';

		try {
			$troncon->delete();
				
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalues = $form->getTypedValues($formvalues);
			$historisation->addhistory('Troncon', self::MODE_DELETE, $formvalues);
			// historisation fin
			$message = 'Troncon supprimé';
		} catch (Exception $e) {
			$message = 'Troncon n\'a pas pu etre supprimé';
		}

		// $this->getSession()->set('flash_title', '');
		// $this->getSession()->set('flash_status', 'info');
		// $this->getSession()->set('flash_message', $message);

		//$this->forward('Log', 'index');

	}

}