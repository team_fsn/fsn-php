<?php

class Controller_Contour_Fai extends Yab_Controller_Action {
	
	public function actionAjaxGetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$fai = (!empty($_POST['values'])) ? $_POST['values'] : null;
		//$contours = array();
		$contours['entity'] = array();
		$count = 0;
		foreach ($fai as $element){
			$fai_contours = new Model_Contour_Fai();
			$values = $fai_contours->fetchAll()->where('fai_id="'.$element.'"')->orderBy('sequence');
			$fai_contours = $values->toArray();
			$current_fai = new Model_Fai($element);
			if (!empty($fai_contours)) {
				$contours['entity'][$count]['name'] = $current_fai['identification'];
				$contours['entity'][$count]['id'] = $current_fai['id'];
				$contours['entity'][$count]['categorie'] = 'fai';
				$str = array();
				$val = 0;
				$poly = array();
				foreach ($fai_contours as $item) {
	
					//$contours[$current_us['identification']][$point][] = ;
					//$contours[$current_us['identification']][$point][] =
					//$contours['us'][$count]['points'][]  = '['.$item['x'] . ', ' . $item['y'].']';
					$contours['entity'][$count]['points'][$val]['x']  = $item['x'];
					$contours['entity'][$count]['points'][$val]['y']  =  $item['y'];
					$poly[] = array($item['x'],$item['y']);
					$str[] = '['.$item['x'] . ', ' . $item['y'].']';
					$val++;
				}
				//$contours['us'][$count]['points'][] = $this->setPoly($str);
				$contours['entity'][$count]['poly'] = $poly;
				$count++;
			}
		}
		echo json_encode($contours);
	}
	
	/*
	public function actionAjaxGetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$fai = (!empty($_POST['values'])) ? $_POST['values'] : array();
		//$contours = array();
		$contours['entity'] = array();
		$count = 0;
		foreach ($fai as $element){
			$fai_contours = new Model_Contour_Fai();
			$values = $fai_contours->fetchAll()->where('fai_id="'.$element.'"');
			$fai_contours = $values->toArray();
			$current_fai = new Model_Fai($element);
			if (!empty($fai_contours)) {
				$contours['entity'][$count]['name'] = $current_fai['identification'];
				$contours['entity'][$count]['id'] = $current_fai['id'];
				$contours['entity'][$count]['categorie'] = 'fai';
				
				$val = 0;
				$poly = array();
				foreach ($fai_contours as $item) {
					$poly[] = array($item['x'],$item['y']);
					$val++;
				}
				$contours['entity'][$count]['poly'] = $poly;
				$count++;
			}
		}
		echo json_encode($contours);
	}
	*/
	public function actionAjaxSetContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$tab = (!empty($_POST['contour_modif'])) ? $_POST['contour_modif'] : null;
		if (!empty($tab)) {
			$identification = $tab[count($tab)-1];
			unset($tab[count($tab)-1]);
			$fai_fetch = new Model_Fai();
			$fai = $fai_fetch->fetchAll()->where('identification="'.$identification.'"');
			$fai = $fai->toArray();
			if (!empty($fai)) {
				$fai_id = $fai[0]['id'];
	
				$contour_fetch = new Model_Contour_Fai();
				$contour_test = $contour_fetch->fetchAll()->where('fai_id="'.$fai_id.'"');
				$contour_test = $contour_test->toArray();
				if (empty($contour_test)) {
					$generateGuuid = new Plugin_Guuid() ;
	
					$info_contour = array();
					$info_contour['fai_id'] = $fai_id;
	
					foreach ($tab as $cle => $value ) {
						$contour = new Model_Contour_Fai();
						$guuid = $generateGuuid->GetUUID() ;
						$info_contour['id'] = $guuid;
						$coor = explode('-', $value);
						$info_contour['x'] = trim($coor[0]);
						$info_contour['y'] = trim($coor[1]);
						$info_contour['sequence'] = $cle + 1;
							
						$contour->populate($info_contour)->save();
					}
					$result = array();
					$result[] = $fai_id;
					$result[] = "les contours du fai ".$identification." enregistres avec succes";
					echo json_encode($result);
				} else {
					$result = array();
					$result[] = 'vide';
					$result[] = "Les contours du fai".$identification+" existent déjà";
					echo json_encode($result);
				}
	
			} else {
				$result = array();
				$result[] = 'vide';
				$result[] = 'cet Identificant est incorrect';
				echo json_encode($result);
	
			}
	
		}
	}
	
	public function actionAjaxEditContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
		$sitefouille = new Model_Sitefouille();
		$tab = (!empty($_POST['contour_modif'])) ? $_POST['contour_modif'] : null;
		if (!empty($tab)) {
			$id = $tab[count($tab)-1];
			unset($tab[count($tab)-1]);
				
			$fai = new Model_Fai($id);
				
			$contour_fetch = new Model_Contour_Fai();
			$contour_test = $contour_fetch->fetchAll()->where('fai_id="'.$fai->get('id').'"');
			$contour_test = $contour_test->toArray();
			if (!empty($contour_test)) {
				foreach ($contour_test as $item) {
					$element = new Model_Contour_Fai($item['id']);
					$element->delete();
				}
			}
			$generateGuuid = new Plugin_Guuid() ;
				
			$info_contour = array();
			$info_contour['fai_id'] = $fai->get('id');
	
			foreach ($tab as $cle => $value ) {
				$contour = new Model_Contour_Fai();
				$guuid = $generateGuuid->GetUUID() ;
				$info_contour['id'] = $guuid;
				$coor = explode('-', $value);
				$info_contour['x'] = trim($coor[0]);
				$info_contour['y'] = trim($coor[1]);
				$info_contour['sequence'] = $cle + 1;
					
				$contour->populate($info_contour)->save();
			}
	
			echo "Modification du fai ".$fai->get('identification')." avec succes";
		} else {
			echo 'une erreur est survenue';
		}
			
	
	}
	
	public function actionAjaxDeleteContours() {
		$this->getLayout()->setEnabled(false) ;
		$this->_view->setEnabled(false) ;
	
		$sitefouille = new Model_Sitefouille();
		$id = (!empty($_POST['id'])) ? $_POST['id'] : null;
		if (!empty($id)) {
			$fai = new Model_Fai($id);
				
			$contour_fetch = new Model_Contour_Fai();
			$contour_test = $contour_fetch->fetchAll()->where('fai_id="'.$fai->get('id').'"');
			$contour_test = $contour_test->toArray();
			if (!empty($contour_test)) {
				foreach ($contour_test as $item) {
					$element = new Model_Contour_Fai($item['id']);
					$element->delete();
				}
			}
				
			echo "les contours du fai ".$fai->get('identification')." supprimes avec succes";
		} else {
			echo 'une erreur est survenue';
		}
	}
	
	
	public function actionIndex() {

		$contour_fai = new Model_Contour_Fai();

		$contour_fais = $contour_fai->fetchAll();

		$this->_view->set('contour_fais', $contour_fais);
	}

	public function actionAdd() {

		$contour_fai = new Model_Contour_Fai();

		$form = new Form_Contour_Fai($contour_fai);

		if($form->isSubmitted() && $form->isValid()) {

			$contour_fai->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'contour_fai as been added');

			$this->forward('Contour_Fai', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$contour_fai = new Model_Contour_Fai($this->_request->getParams());

		$form = new Form_Contour_Fai($contour_fai);

		if($form->isSubmitted() && $form->isValid()) {

			$contour_fai->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'contour_fai as been edited');

			$this->forward('Contour_Fai', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$contour_fai = new Model_Contour_Fai($this->_request->getParams());

		$contour_fai->delete();

		$this->getSession()->set('flash', 'contour_fai as been deleted');

		$this->forward('Contour_Fai', 'index');

	}

}