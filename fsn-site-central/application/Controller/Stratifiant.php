<?php
class Controller_Stratifiant extends Yab_Controller_Action {
	public function actionIndex() {
		$session = $this->getSession();
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
	        
        // Mantis 345 : En cas de tous sites, empêcher l'utilisation du Dashboard
        if ((is_null($sitefouille_id)) || (empty($sitefouille_id))) {
        	$this->getSession()->set('flash_title', 'Suivi de chantiers');
        	$this->getSession()->set('flash_status', 'warning');
        	$this->getSession()->set('flash_message', 'Veuillez choisir un site de fouille');
        	return;
        }
	}
	public function actionAjaxTest() {
		echo('Controller Stratifiant actionAjaxTest');
	}
	/**
	 * Fonction qui permet l'appel ajax au service web
	 */
	public function actionAjaxStratifiant() {
		$session = $this->getSession();
		$sitefouille_id = $session->has('sitefouille_id') ? $session->get('sitefouille_id') : null ;
		$url = 'http://'.Yab_Loader::getInstance()->getConfig()['ws'].'/export/stratifiant-json/'.$sitefouille_id;
		
		//$header = array('Content-Type: multipart/form-data');
		$header = array('Content-Type: application/json;charset=utf-8');
		//               contentType: "application/json;charset=utf-8"
		/*		
		// Repertoire temporaire de stockage du fichier
		$tmp_dir = dirname(YAB_PATH) . DIRECTORY_SEPARATOR .'tmp'.DIRECTORY_SEPARATOR.$_FILES['file']['name'];
		
		// On deplace le fichier temporaire dans le dossier tmp de yab
		move_uploaded_file($_FILES['file']['tmp_name'], $tmp_dir);
		
		// Envoie du fichier au web service d'import en curl
		$cfile = new CURLFile($tmp_dir,'text/csv',$tmp_dir);
		$data = array('file'=>$cfile);
		*/
		
		$data = $_POST["donnees"];
		
		$resource = curl_init();
		curl_setopt($resource, CURLOPT_URL, $url);
		curl_setopt($resource, CURLOPT_HTTPHEADER, $header);
		curl_setopt($resource, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($resource, CURLOPT_POST, 1);
		curl_setopt($resource, CURLOPT_POSTFIELDS, $data);

		$result = curl_exec($resource);
		
		curl_close($resource);
		
		// Suppresion du fichier temporaire
		//unlink($tmp_dir);
		
		// Envoie du resultat a l'ajax
		echo($result);
		die();
	}
	
	public function actionShowUS() {
		$identification = $_GET["identification"];
		$us = new Model_Us();
		$uses = $us->getUsByIdentification($identification);
		$messageUSerror = "US non trouvée : " . $identification;
		if (!empty($uses)) {
			foreach ($uses as $us_key => $us_value) {
				//var_dump($us_value); die();
				$us_id = $us_value->get('id');
				$this->forward('Us', 'show', array($us_id));
				break;
			}
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->set('flash_message', $messageUSerror);
		}
		else {
			$this->getSession()->set('flash_title', '');
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->set('flash_message', $messageUSerror);
		}
	}
}
?>
