<?php

class Controller_Precisionproduction extends Yab_Controller_Action {

	public function actionIndex() {

		$precisionproduction = new Model_Precisionproduction();

		$precisionproductions = $precisionproduction->fetchAll();

		$this->_view->set('precisionproductions', $precisionproductions);
	}

	public function actionAdd() {

		$precisionproduction = new Model_Precisionproduction();

		$form = new Form_Precisionproduction($precisionproduction);

		if($form->isSubmitted() && $form->isValid()) {

			$precisionproduction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'precisionproduction as been added');

			$this->forward('Precisionproduction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$precisionproduction = new Model_Precisionproduction($this->_request->getParams());

		$form = new Form_Precisionproduction($precisionproduction);

		if($form->isSubmitted() && $form->isValid()) {

			$precisionproduction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'precisionproduction as been edited');

			$this->forward('Precisionproduction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$precisionproduction = new Model_Precisionproduction($this->_request->getParams());

		$precisionproduction->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'precisionproduction as been deleted');

		$this->forward('Precisionproduction', 'index');

	}

}