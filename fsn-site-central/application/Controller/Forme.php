<?php

class Controller_Forme extends Yab_Controller_Action {

	public function actionIndex() {

		$forme = new Model_Forme();

		$formes = $forme->fetchAll();

		$this->_view->set('formes', $formes);
	}

	public function actionAdd() {

		$forme = new Model_Forme();

		$form = new Form_Forme($forme);

		if($form->isSubmitted() && $form->isValid()) {

			$forme->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'forme as been added');

			$this->forward('Forme', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$forme = new Model_Forme($this->_request->getParams());

		$form = new Form_Forme($forme);

		if($form->isSubmitted() && $form->isValid()) {

			$forme->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'forme as been edited');

			$this->forward('Forme', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$forme = new Model_Forme($this->_request->getParams());

		$forme->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'forme as been deleted');

		$this->forward('Forme', 'index');

	}

}