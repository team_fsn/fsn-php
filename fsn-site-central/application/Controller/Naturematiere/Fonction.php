<?php

class Controller_Naturematiere_Fonction extends Yab_Controller_Action {

	public function actionIndex() {

		$naturematiere_fonction = new Model_Naturematiere_Fonction();

		$naturematiere_fonctions = $naturematiere_fonction->fetchAll();

		$this->_view->set('naturematiere_fonctions', $naturematiere_fonctions);
	}

	public function actionAdd() {

		$naturematiere_fonction = new Model_Naturematiere_Fonction();

		$form = new Form_Naturematiere_Fonction($naturematiere_fonction);

		if($form->isSubmitted() && $form->isValid()) {

			$naturematiere_fonction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'naturematiere_fonction as been added');

			$this->forward('Naturematiere_Fonction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$naturematiere_fonction = new Model_Naturematiere_Fonction($this->_request->getParams());

		$form = new Form_Naturematiere_Fonction($naturematiere_fonction);

		if($form->isSubmitted() && $form->isValid()) {

			$naturematiere_fonction->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'naturematiere_fonction as been edited');

			$this->forward('Naturematiere_Fonction', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$naturematiere_fonction = new Model_Naturematiere_Fonction($this->_request->getParams());

		$naturematiere_fonction->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'naturematiere_fonction as been deleted');

		$this->forward('Naturematiere_Fonction', 'index');

	}

}