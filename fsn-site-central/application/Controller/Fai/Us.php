<?php

class Controller_Fai_Us extends Yab_Controller_Action {

	public function actionIndex() {

		$fai_us = new Model_Fai_Us();

		$fai_uses = $fai_us->fetchAll();

		$this->_view->set('fai_uses', $fai_uses);
	}
		
	public function actionListFaiUs($layout=false, $fai_id, $v_action=false) {
		
		$this->getLayout()->setEnabled($layout);
		
		$fai_us = new Model_Fai_Us();
		$form_fai_us = new Form_Fai_Us($fai_us);
		
		$associatfai_us = $fai_us->getFai_us()->where('fai_id = "'.$fai_id.'" ');
		
		$this->_view->set('helper_form_associatfai_us',$associatfai_us);
		$this->_view->set('helper_form_fai_us',$form_fai_us);
		$this->_view->set('v_action', $v_action);
				
		if (!$layout) { $this->_view->setFile('View/fai/us/list_fai_us_simple.html') ; }
	
	}
	
	public function actionAdd() {

		$fai_us = new Model_Fai_Us();

		$form = new Form_Fai_Us($fai_us);

		if($form->isSubmitted() && $form->isValid()) {

			$fai_us->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fai_us as been added');

			$this->forward('Fai_Us', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fai_us = new Model_Fai_Us($this->_request->getParams());

		$form = new Form_Fai_Us($fai_us);

		if($form->isSubmitted() && $form->isValid()) {

			$fai_us->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fai_us as been edited');

			$this->forward('Fai_Us', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fai_us = new Model_Fai_Us($this->_request->getParams());

		$fai_us->delete();

		$this->getSession()->set('flash', 'fai_us as been deleted');

		$this->forward('Fai_Us', 'index');

	}

}