<?php

class Controller_Trait_Inter extends Yab_Controller_Action {

	public function actionIndex() {

		$trait_inter = new Model_Trait_Inter();

		$trait_inters = $trait_inter->fetchAll();

		$this->_view->set('trait_inters', $trait_inters);
	}

	public function actionAdd() {

		$trait_inter = new Model_Trait_Inter();

		$form = new Form_Trait_Inter($trait_inter);

		if($form->isSubmitted() && $form->isValid()) {

			$trait_inter->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'trait_inter as been added');

			$this->forward('Trait_Inter', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$trait_inter = new Model_Trait_Inter($this->_request->getParams());

		$form = new Form_Trait_Inter($trait_inter);

		if($form->isSubmitted() && $form->isValid()) {

			$trait_inter->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'trait_inter as been edited');

			$this->forward('Trait_Inter', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$trait_inter = new Model_Trait_Inter($this->_request->getParams());

		$trait_inter->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'trait_inter as been deleted');

		$this->forward('Trait_Inter', 'index');

	}

}