<?php

class Controller_Extorganisme extends Yab_Controller_Action {

	public function actionIndex() {

		$extorganisme = new Model_Extorganisme();

		$extorganismes = $extorganisme->fetchAll();

		$this->_view->set('extorganismes', $extorganismes);
	}

	public function actionAdd() {

		$extorganisme = new Model_Extorganisme();

		$form = new Form_Extorganisme($extorganisme);

		if($form->isSubmitted() && $form->isValid()) {

			$extorganisme->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'extorganisme as been added');

			$this->forward('Extorganisme', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$extorganisme = new Model_Extorganisme($this->_request->getParams());

		$form = new Form_Extorganisme($extorganisme);

		if($form->isSubmitted() && $form->isValid()) {

			$extorganisme->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'extorganisme as been edited');

			$this->forward('Extorganisme', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$extorganisme = new Model_Extorganisme($this->_request->getParams());

		$extorganisme->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'extorganisme as been deleted');

		$this->forward('Extorganisme', 'index');

	}

}