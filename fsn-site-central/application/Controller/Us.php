<?php

class Controller_Us extends Yab_Controller_Action {

    const CROQUIS_DIRECTORY = "croquis/";
    const LABEL_HISTO = "Us";

	public function actionIndex() {

		$us = new Model_Us();
        
        $uses = $us->getListUs(Yab_Loader::getInstance()->getSession()->get('sitefouille_id')) ;

        // Recuperation du nombre total de requete. L'objet "uses" contient un field "nbrRequete"
        Plugin_Fonctions::getRequeteNumber($uses, $this->_view);

		$this->_view->set('uses_index', $uses);
	}
    
    public function actionListSimple($layout=false, $sitefouille_id=null) {

        $this->getLayout()->setEnabled($layout);
		$us = new Model_Us();

		$uses = $us->fetchAll()->orderBy(array('identification' => 'ASC' ) ) ;
        if (!empty($sitefouille_id) ) {
          $uses = $uses->where('sitefouille_id = "'.$sitefouille_id.'" ' ) ;
        }
        
		$this->_view->set('uses_list', $uses);
    
	}
  
    public function actionAdd() {

    	$loader = Yab_Loader::getInstance();
    	$session = $loader->getSession();
    	$sitefouille_id = $session->get('sitefouille_id');
    	if (empty($sitefouille_id)) {
			$this->getSession()->set('flash_status', 'warning');
			$this->getSession()->setFlashError('keysf', 'Veuillez sélectionner un site de fouille !');
			$this->forward('Us', 'index');
    	}
    	
        $us = new Model_Us();

        $newSup = new Model_Superposition();
        $form_add_ant = new Form_Superposition($newSup);
        $form_add_post = new Form_Superposition($newSup);
        $newSync= new Model_Synchronisation();
        $form_add_sync = new Form_Synchronisation($newSync);
        $newInc= new Model_Inclusion();
        $form_add_inc = new Form_Inclusion($newInc);
        $newInt= new Model_Intervention_Us();
        $form_add_int = new Form_Intervention_Us($newInt);

        $mode = self::ACTION_MODE_CREATE ;
        $form_add = new Form_Us($us, $mode);

        $form_add->setElement('numero_ident', array(
            'type' => 'text',
            'id' => 'numero_ident',
            'label' => $form_add->getElement('identification')->get('label'),
            'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_add->getElement('identification')->get('numero'),
            'validators' => array('NotEmpty','Int'),
            'errors' => array('NotEmpty' => array('Le champ identification ne doit pas être vide'),
                'Int' => array('Ce champs doit être un entier')),
        ));
		if($form_add->isSubmitted()){
            $errors_messages = array();
            $exist_us = $us->getUsByIdentification($form_add->getElement('identification')->get('prefix') . "." . intval($_POST['numero_ident']))->toArray();
            if(!empty($exist_us)) {
            	$errors_messages[$form_add->getElement('identification')->get('id')] = "l'identifiant doit être unique";
            }else
                $form_add->getElement('identification')->set('value',$form_add->getElement('identification')->get('prefix') . "." . intval($_POST['numero_ident']));

            if ($form_add->isValid() && empty($errors_messages)) {
                $formvalue = $form_add->getValues();
                $historisation = new Plugin_Historique();
                unset($formvalue['numero_ident']);
                unset($formvalue['croquis_id']);

                // Transformation des Dates
                $transformDateFrUs = new Plugin_DateFrUs();
                if($formvalue['debut'] && $formvalue['debut'] != '')
                    $formvalue['debut'] = $transformDateFrUs->convertitDateFRUs($formvalue['debut']);
                else
                    unset($formvalue['debut']);
                if($formvalue['fin'] && $formvalue['fin'] != '')
                    $formvalue['fin'] = $transformDateFrUs->convertitDateFRUs($formvalue['fin']);
                else
                    unset($formvalue['fin']);

                // Ajout du GUUID
                $generateGuuid = new Plugin_Guuid();
                $guuid = $generateGuuid->GetUUID();
                //$form->setElement('id', array('type'=>'text', 'value' => $guuid ) ) ;
                $formvalue['id'] = $guuid;
                $formvalue = $form_add->setNullValues($formvalue);
                $us->populate($formvalue)->save();
				
				//Debut création arborescence multimédia 				
					//Recuperation nomabrege de l'organisme
					$user_session= $this->getSession()->get('session');
					$entiteadmin_id= $user_session['entiteadmin_id'];
					
					$organismes = $us->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
					$nb_row = $organismes->count();
					
					if($nb_row > 0){						
						foreach($organismes as $organisme){								
							$arborescence = '../mediatheque/albums/'.$organisme['nomabrege_organisme'].'/us/'.$formvalue['identification'];
						}

						if(!is_dir($arborescence)){
							mkdir($arborescence, 0777, true);
						}
					}				
				//Fin création arborescence multimédia 
				
                $formvalue = $form_add->getTypedValues($formvalue);

                // ajout des relations en bdd
                $title_relations = ["superposition" => "_pfanterieur_|_pfposterieur_", "synchronisation" => "_pfsynchronisation_", "inclusion" => "_pfregroup_", "intervention_Us" => "_pfinterventions_"];
                $regex_relations = array();
                foreach ($title_relations as $table => $title) {
                    $regex_relations[$table] = "#^([a-z0-9_]+)(" . $title . ")([0-9]+)$#";
                }
                $relations = array();
                foreach ($_POST as $key => $value) {
                    foreach ($regex_relations as $table => $regex) {
                        if (preg_match($regex, $key, $res)) {
                            $relations[$table][$res[3]][$res[1]] = $value;
                        }
                    }
                }
                foreach ($relations as $table => $relationsTable) {
                    $nomModel = "Model_" . ucfirst($table);
                    $nomForm = "Form_".ucfirst($table);
                    $nomController = "Controller_".ucfirst($table);
                    foreach ($relationsTable as $relation) {
                        $model = new $nomModel();
                        $form = new $nomForm($model);
                        $date_debut_save = null;
                        $date_fin_save = null;
                        if ($table == "superposition") {
                            if (!$relation['anterieur_id'])
                                $relation['anterieur_id'] = $formvalue['id'];
                            else if (!$relation['posterieur_id'])
                                $relation['posterieur_id'] = $formvalue['id'];
                        } else if ($table == "synchronisation") {
                            $relation['synchro1_id'] = $formvalue['id'];
                        } else if ($table == "inclusion") {
                            $relation['inclusion2_id'] = $us->get('id');
                        } else if ($table == "intervention_Us") {
							if($relation['debut'] && $relation['debut'] != '')
							{
                        		$date_debut_save = $relation['debut']; // jfb 2016-04-26 mantis 239 : sauvegarde date début
								$relation['debut'] = $transformDateFrUs->convertitDateFRUs($relation['debut']);
							}
							else
								$relation['debut'] = null;
							
							if($relation['fin'] && $relation['fin'] != '')
							{
                        		$date_fin_save = $relation['fin']; // jfb 2016-04-26 mantis 239 : sauvegarde date fin
								$relation['fin'] = $transformDateFrUs->convertitDateFRUs($relation['fin']);
							}
							else 
								$relation['fin'] = null;
							
                            $relation['us_id'] = $us->get('id');
                        }
                        $relation['id'] = $generateGuuid->GetUUID();
                        $model->populate($relation)->save();
                        unset($relation['id']);
                        foreach($relation as $key => $value)
                            $form->getElement($key)->set('value',$value);
                        $relation = $form->getTypedValues($relation);
                        $relation['id'] = $model->get('id');
                        if ($table == "intervention_Us") {
	                        if ($date_debut_save && $date_debut_save != '')
    	                    	$relation['debut'] = $date_debut_save;
        	                if ($date_fin_save && $date_fin_save != '')
            	            	$relation['fin'] = $date_fin_save;
                        }
                        $historisation->addhistory($nomController::LABEL_HISTO, self::MODE_CREATE, $relation);

                    }
                }
				
                // Historisation de la modif
                $id_us = $us->get('id');
                $formvalue['id'] = $id_us;
                $historisation->addhistory(self::LABEL_HISTO, self::MODE_CREATE, $formvalue);

                $registry = Yab_Loader::getInstance() -> getRegistry();
                $i18n = $registry -> get('i18n');
                $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');
                $this->getSession()->set('flash_message',str_replace("__ELEM__",$form_add->getRessourceType(),$i18n->say('added_message')));
                $this->forward('Us', 'Index', array('id' => $id_us));
            }
            else {
                $this->getSession()->set('flash_status', 'warning');
                $this->getSession()->setFlashErrors($errors_messages);
                $relation_error = Yab_Loader::getInstance()->getRegistry()->get('i18n')->say('warning_modif_not_saved');
                $this->getSession()->setFlashError("panel_relations", str_replace("__PART__", "relations inter-us", $relation_error));
                $this->getSession()->setFlashError("panel_interventions", str_replace("__PART__", "interventions", $relation_error));
                $this->getSession()->setFlashErrors($form_add->getErrors());
            }
		}

        $this->_view->set('form_add', $form_add);
        $this->_view->set('form_add_ant', $form_add_ant);
        $this->_view->set('form_add_post', $form_add_post);
        $this->_view->set('form_add_sync', $form_add_sync);
        $this->_view->set('form_add_inc', $form_add_inc);
        $this->_view->set('form_add_int', $form_add_int);

	}

	public function actionEdit()
    {
        $us = new Model_Us($this->_request->getParams());
        $mode = self::ACTION_MODE_UPDATE;
        $form_edit = new Form_Us($us, $mode);
        $newSup = new Model_Superposition();

        $courant_sitefouille_id = $us->get('sitefouille_id');

        $form_edit_ant = new Form_Superposition($newSup,$courant_sitefouille_id);
        $form_edit_post = new Form_Superposition($newSup,$courant_sitefouille_id);
        $newSync = new Model_Synchronisation();
        $form_edit_sync = new Form_Synchronisation($newSync,$courant_sitefouille_id);
        $newInc = new Model_Inclusion();
        $form_edit_inc = new Form_Inclusion($newInc,$courant_sitefouille_id);
        $newInt= new Model_Intervention_Us();
        $form_edit_int = new Form_Intervention_Us($newInt);
        $form_edit_ant->getElement('posterieur_id')->set('value', $us->get('id'));
        $form_edit_post->getElement('anterieur_id')->set('value', $us->get('id'));

        $session = $this->getSession();

        $form_edit->setElement('numero_ident', array(
            'type' => 'text',
            'id' => 'numero_ident',
            'label' => $form_edit->getElement('identification')->get('label'),
            'value' => isset($_POST['numero_ident']) ? $_POST['numero_ident'] : $form_edit->getElement('identification')->get('numero'),
            'validators' => array('NotEmpty','Int'),
            'disable' => '1',
            'readonly' => 'readonly',
            'errors' => array(
                'Int' => array('Ce champs doit être un entier')),
        ));

        if ($form_edit->isSubmitted()){
           
            // Mise en session sitefouille_id
            $session['sitefouille_id'] = $courant_sitefouille_id;

            $errors_messages = array();
            $baba = $us->getUsByIdentification($form_edit->getElement('identification')->get('prefix') . "." . intval($_POST['numero_ident']))->toArray();
            if($form_edit->getElement('identification')->get('prefix') . "." . $_POST['numero_ident'] != $form_edit->getElement('identification')->get('value')
                && !empty($baba)) {
                $errors_messages[$form_edit->getElement('identification')->get('id')] = "l'identifiant doit être unique";
            }else
                $form_edit->getElement('identification')->set('value',$form_edit->getElement('identification')->get('prefix') . "." . intval($_POST['numero_ident']));

            if ($form_edit->isValid() && empty($errors_messages)) {
                $formvalue = $form_edit->getValues();
                unset($formvalue['numero_ident']);
                unset($formvalue['croquis_id']);
                // Transformation des Dates
                $transformDateFrUs = new Plugin_DateFrUs();
                if($formvalue['debut'] && $formvalue['debut'] != '')
                    $formvalue['debut'] = $transformDateFrUs->convertitDateFRUs($formvalue['debut']);

                if($formvalue['fin'] && $formvalue['fin'] != '')
                    $formvalue['fin'] = $transformDateFrUs->convertitDateFRUs($formvalue['fin']);

                $formvalue = $form_edit->setNullValues($formvalue);
                $us->populate($formvalue)->save();
				
				//Debut création arborescence multimédia				
					//Recuperation nomabrege de l'organisme
					$user_session= $this->getSession()->get('session');
					$entiteadmin_id= $user_session['entiteadmin_id'];
					
					$organismes = $us->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
					$nb_row = $organismes->count();
					
					if($nb_row > 0){						
						foreach($organismes as $organisme){								
							$arborescence = '../mediatheque/albums/'.$organisme['nomabrege_organisme'].'/us/'.$formvalue['identification'];
						}

						if(!is_dir($arborescence)){
							mkdir($arborescence, 0777, true);
						}
					}				
				//Fin création arborescence multimédia
				
                $formvalue = $form_edit->getTypedValues($formvalue);

                // ajout des relations en bdd
                $historisation = new Plugin_Historique();
                $this->deleteRelationsUs($us->get('id'));
                $generateGuuid = new Plugin_Guuid();
                $title_relations = ["superposition" => "_pfanterieur_|_pfposterieur_", "synchronisation" => "_pfsynchronisation_", "inclusion" => "_pfregroup_", "intervention_Us" => "_pfinterventions_"];
                $regex_relations = array();
                foreach ($title_relations as $table => $title) {
                    $regex_relations[$table] = "#^([a-z0-9_]+)(" . $title . ")([0-9]+)$#";
                }
                $relations = array();
                foreach ($_POST as $key => $value) {
                    foreach ($regex_relations as $table => $regex) {
                        if (preg_match($regex, $key, $res)) {
                            $relations[$table][$res[3]][$res[1]] = $value;
                        }
                    }
                }
                foreach ($relations as $table => $relationsTable) {
                    $nomModel = "Model_" . ucfirst($table);
                    $nomForm = "Form_".ucfirst($table);
                    $nomController = "Controller_".ucfirst($table);
                    foreach ($relationsTable as $relation) {
                        $model = new $nomModel();
                        $form = new $nomForm($model);
						$date_debut_save = null;
						$date_fin_save = null;
                        if ($table == "superposition") {
                            if (!$relation['anterieur_id'])
                                $relation['anterieur_id'] = $us->get('id');
                            else if (!$relation['posterieur_id'])
                                $relation['posterieur_id'] = $us->get('id');
                        } else if ($table == "synchronisation") {
                            $relation['synchro1_id'] = $us->get('id');
                        }else if ($table == "inclusion") {
                            $relation['inclusion2_id'] = $us->get('id');
                        }else if ($table == "intervention_Us") {

							if($relation['debut'] && $relation['debut'] != '')
							{
								$date_debut_save = $relation['debut']; // jfb 2016-04-26 mantis 239 : sauvegarde date début
								$relation['debut'] = $transformDateFrUs->convertitDateFRUs($relation['debut']);
							}
							else
								$relation['debut'] = null;
							
							if($relation['fin'] && $relation['fin'] != '')
							{
								$date_fin_save = $relation['fin']; // jfb 2016-04-26 mantis 239 : sauvegarde date fin
								$relation['fin'] = $transformDateFrUs->convertitDateFRUs($relation['fin']);
							}
							else 
								$relation['fin'] = null;
							
                            $relation['us_id'] = $us->get('id');
                        }
                        $relation['id'] = $generateGuuid->GetUUID();
                        $model->populate($relation)->save();
                        unset($relation['id']);
                        foreach($relation as $key => $value)
                            $form->getElement($key)->set('value',$value);
                        $relation = $form->getTypedValues($relation);
                        $relation['id'] = $model->get('id');
                        if ($table == "intervention_Us") {
	                        if ($date_debut_save && $date_debut_save != '')
    	                    	$relation['debut'] = $date_debut_save;
        	                if ($date_fin_save && $date_fin_save != '')
            	            	$relation['fin'] = $date_fin_save;
                        }
                        $historisation->addhistory($nomController::LABEL_HISTO, self::MODE_CREATE, $relation);
                    }
                }

                // Historisation de la modif
                $id_us = $us->get('id');
                $formvalue['id'] = $id_us;
                $historisation->addhistory(self::LABEL_HISTO, self::MODE_UPDATE, $formvalue);

                $registry = Yab_Loader::getInstance() -> getRegistry();
                $i18n = $registry -> get('i18n');
                $this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info');
                $this->getSession()->set('flash_message',str_replace("__ELEM__",$form_edit->getRessourceType(),$i18n->say('added_message')));
                $this->forward('Us', 'Index', array('id' => $id_us));

                $this->forward('Us', 'index');

            }else {
                $this->getSession()->set('flash_status', 'warning');
                $relation_error = Yab_Loader::getInstance()->getRegistry()->get('i18n')->say('warning_modif_not_saved');
                $this->getSession()->setFlashError("panel_relations", str_replace("__PART__", "relations inter-us", $relation_error));
                $this->getSession()->setFlashError("panel_interventions", str_replace("__PART__", "interventions", $relation_error));
                $this->getSession()->setFlashErrors($errors_messages);
                $this->getSession()->setFlashErrors($form_edit->getErrors());
            }
		}

        $this->_view->set('form_edit', $form_edit);
        $this->_view->set('form_edit_ant', $form_edit_ant);
        $this->_view->set('form_edit_post', $form_edit_post);
        $this->_view->set('form_edit_sync', $form_edit_sync);
        $this->_view->set('form_edit_inc', $form_edit_inc);
        $this->_view->set('form_edit_int', $form_edit_int);

        $us_id = $us->get('id') ;
        $this->_view->set('us_id', $us_id);

        /* Nombre ER / US */
        $elementrecueilli = new Model_Elementrecueilli();
        $elementrecueillis = $elementrecueilli->fetchAll()->where('us_id = "'.$us_id.'" ') ;
        $us_nb_er = $elementrecueillis->count() ;

        $this->_view->set('us_nb_er', $us_nb_er);


        // Elements Multimedia
        $document = new Model_Us_Doc();
        $documents = $document->listUsDoc()->where('us_id = "'.$us_id.'" ') ;
        $this->_view->set('documents', $documents);


	}

    private function deleteRelationsUs($us_id){
        $histo = array();
        $historisation = new Plugin_Historique();
        $m_sups = new Model_Superposition();
        $histo[Controller_Superposition::LABEL_HISTO] = $m_sups->getAnteroposterite()->where('posterieur_id = "'.$us_id.'" ')->toArray();
        foreach($m_sups->getAnteroposterite()->where('anterieur_id = "'.$us_id.'" ')->toArray() as $relation)
            $histo[Controller_Superposition::LABEL_HISTO][] = $relation;
        $m_sync = new Model_Synchronisation();
        $histo[Controller_Synchronisation::LABEL_HISTO] = $m_sync->ListUsSynchronisation()->where('synchro1_id = "'.$us_id.'" ' )->toArray();
        $m_inc = new Model_Inclusion();
        $histo[Controller_Inclusion::LABEL_HISTO] = $m_inc->getInclusions($us_id);
        $m_interv = new Model_Intervention_Us();
        $histo[Controller_Intervention_Us::LABEL_HISTO] = $m_interv->getInterventionsHisto($us_id)->toArray();
        foreach($histo as $name => $relations){
            foreach($relations as $relation){
                $historisation->addhistory($name, self::MODE_DELETE, $relation->getAttributes());
            }
        }
        $m_sups->deleteRelations($us_id);
        $m_sync->deleteRelations($us_id);
        $m_inc->deleteRelations($us_id);
        $m_interv->deleteRelations($us_id);

    }
	
	public function actionShow() {

        $us = new Model_Us($this->_request->getParams());
        $us_id = $us->get('id');
        
        $mode = self::ACTION_MODE_SHOW ;
        $form = new Form_Us($us, $mode);
        $formvalue = $form->getValues();
        $newUsAnt = new Model_Superposition();
        $form_show_ant = new Form_Superposition($newUsAnt);
        $form_show_ant->getElement('posterieur_id')->set('value',$us->get('id'));

        
        /**
         * Nombre ER / US 
         */
        $elementrecueilli = new Model_Elementrecueilli();
        $elementrecueillis = $elementrecueilli->fetchAll()->where('us_id = "'.$us_id.'" ') ;
        $us_nb_er = $elementrecueillis->count() ;
        
        $this->_view->set('us_nb_er', $us_nb_er);
        
        $this->_view->set('us_id', $us_id);
        
		$this->_view->set('form_show', $form);
        $this->_view->set('form_show_ant', $form_show_ant);
        
        $this->_view->set('helper_form_show', new Yab_Helper_Form($form));
            
        /*Elements Multimedia*/
        $document = new Model_Document();
        $documents = $document->fetchAll() ;
                 
		/*Recuperation nomabrege de l'organisme*/
		$user_session= $this->getSession()->get('session');
		$entiteadmin_id= $user_session['entiteadmin_id'];
		
		$organismes = $us->getEntiteOrganisme()->where('enti.id="'.$entiteadmin_id.'" ');
		$nb_row = $organismes->count();
		
		if($nb_row > 0){						
			foreach($organismes as $organisme){			
				$codeorganisme = $organisme['nomabrege_organisme'];
			}
		}
		else 				
			$codeorganisme = "UASD";
		
		$this->_view->set('documents', $documents);
		$this->_view->set('codesite', $formvalue['identification']);
		$this->_view->set('codeorganisme', $codeorganisme);
		
	}

	public function actionDelete() {

		$us = new Model_Us($this->_request->getParams());
        $form_us = new Form_Us($us);
		$formvalue = $form_us->getValues();
		
		$formvalue['id'] = $us->get('id');

		$message = ''; // init. à blanc message de deletion

		try {
        	$us->delete(); // try delete avant historisation
			
			// historisation début
			$historisation = new Plugin_Historique();
			$formvalue = $form_us->getTypedValues($formvalue);
			$historisation->addhistory('Us', self::MODE_DELETE, $formvalue);
        	// historisation fin

        	$message='us supprimée';
        } catch (Exception $e) {
       		$message = 'Suppression us impossible';
       	}

		//$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'us as been deleted');
       	$this->getSession()->set('flash_title', '');
       	$this->getSession()->set('flash_status', 'info');
       	$this->getSession()->set('flash_message', $message);
       	
		$this->forward('Us', 'index');

	}

    public function actionRelations($layout=false, $us_id) {
        
        $this->getLayout()->setEnabled($layout);

        $superposition = new Model_Superposition();
        $us_anterieurs = $superposition->getAnteroposterite()->where('posterieur_id = "'.$us_id.'" ');
        $us_posterieurs = $superposition->getAnteroposterite()->where('anterieur_id = "'.$us_id.'"');
        
    }
    
    
    public function actionEditDebug() {

        $us = new Model_Us($this->_request->getParams());
        
        $mode = 'edit' ;
        $form = new Form_Us($us, $mode);
        
        $formvalue=$form->getValues();        

        if($form->isSubmitted() && $form->isValid()) {
            
            $date_debut = $form->getElement('debut')->get('value') ;
            $date_fin = $form->getElement('fin')->get('value') ;
            // Transformation des Dates
            $transformDateFrUs = new Plugin_DateFrUs() ;
            $date_debut_us = $transformDateFrUs->convertitDateFRUs($date_debut) ;
            $date_fin_us = $transformDateFrUs->convertitDateFRUs($date_fin) ;
            $formvalue['debut'] = $date_debut_us ; 
            $formvalue['fin'] = $date_fin_us ;
                                   
            $us->populate($formvalue)->save();
            
            $this->getSession()->set('flash', 'us as been edited');

            $this->forward('Us', 'index');

        }

        $this->_view->set('helper_form_editdebug', new Yab_Helper_Form($form));

    }

    public function actionShowErFromUs(){
        if(isset($this->_request->getParams()[0]))
            $this->getSession()->set('us_id', $this->_request->getParams()[0]);
        $this->forward('Elementrecueilli', 'index');

    }
}