<?php

class Controller_Fsn_Admin_Group extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_admin_group = new Model_Fsn_Admin_Group();

		$fsn_admin_groups = $fsn_admin_group->fetchAll();

		$this->_view->set('fsn_admin_groups', $fsn_admin_groups);
	}

	public function actionAdd() {

		$fsn_admin_group = new Model_Fsn_Admin_Group();

		$form = new Form_Fsn_Admin_Group($fsn_admin_group);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_group->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_group as been added');

			$this->forward('Fsn_Admin_Group', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_admin_group = new Model_Fsn_Admin_Group($this->_request->getParams());

		$form = new Form_Fsn_Admin_Group($fsn_admin_group);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_group->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_group as been edited');

			$this->forward('Fsn_Admin_Group', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_admin_group = new Model_Fsn_Admin_Group($this->_request->getParams());

		$fsn_admin_group->delete();

		$this->getSession()->set('flash', 'fsn_admin_group as been deleted');

		$this->forward('Fsn_Admin_Group', 'index');

	}

}