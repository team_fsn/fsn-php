<?php

class Controller_Fsn_Admin_Profil extends Yab_Controller_Action {

	public function actionIndex() {

		$fsn_admin_profil = new Model_Fsn_Admin_Profil();

		$fsn_admin_profils = $fsn_admin_profil->fetchAll();

		$this->_view->set('fsn_admin_profils', $fsn_admin_profils);
	}

	public function actionAdd() {

		$fsn_admin_profil = new Model_Fsn_Admin_Profil();

		$form = new Form_Fsn_Admin_Profil($fsn_admin_profil);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_profil->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_profil as been added');

			$this->forward('Fsn_Admin_Profil', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$fsn_admin_profil = new Model_Fsn_Admin_Profil($this->_request->getParams());

		$form = new Form_Fsn_Admin_Profil($fsn_admin_profil);

		if($form->isSubmitted() && $form->isValid()) {

			$fsn_admin_profil->populate($form->getValues())->save();

			$this->getSession()->set('flash', 'fsn_admin_profil as been edited');

			$this->forward('Fsn_Admin_Profil', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$fsn_admin_profil = new Model_Fsn_Admin_Profil($this->_request->getParams());

		$fsn_admin_profil->delete();

		$this->getSession()->set('flash', 'fsn_admin_profil as been deleted');

		$this->forward('Fsn_Admin_Profil', 'index');

	}

}