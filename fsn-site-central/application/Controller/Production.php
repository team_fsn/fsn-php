<?php

class Controller_Production extends Yab_Controller_Action {

	public function actionIndex() {

		$production = new Model_Production();

		$productions = $production->fetchAll();

		$this->_view->set('productions', $productions);
	}

	public function actionAdd() {

		$production = new Model_Production();

		$form = new Form_Production($production);

		if($form->isSubmitted() && $form->isValid()) {

			$production->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'production as been added');

			$this->forward('Production', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionEdit() {

		$production = new Model_Production($this->_request->getParams());

		$form = new Form_Production($production);

		if($form->isSubmitted() && $form->isValid()) {

			$production->populate($form->getValues())->save();

			$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'production as been edited');

			$this->forward('Production', 'index');

		}

		$this->_view->set('helper_form', new Yab_Helper_Form($form));

	}

	public function actionDelete() {

		$production = new Model_Production($this->_request->getParams());

		$production->delete();

		$this->getSession()->set('flash_title', ''); $this->getSession()->set('flash_status', 'info'); $this->getSession()->set('flash_message', 'production as been deleted');

		$this->forward('Production', 'index');

	}

}