<?php
/**
 * FSN
 *
 * @category   Extend Yab_Validator
 * @package    FSN
 * @author     Cyril GICQUEL
 * @copyright  (c) 2015 SOGETI
 * @date       26/05/2015 
 */

class Validator_PeriodOaExist extends Yab_Validator_Abstract {

	const NOT_EXISTS = 'Date "$1" doesn\'t exists in a O.A. Period';
  
  /**
   * 1 - select liste Oa pour ce sitefouille
   * 2 - defini les intervalles de dates de fouilles
   * 3 - verifie que date debut fouille US inclus dans date fouille Oa      
   */        
	public function _validate($value) {

		$db = Yab_Loader::getInstance()->getRegistry()->get('db');
    /**
     * Récupération de l'identifiant du site de fouille en cours 
     */         
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    $sitefouille_id = $session->get('sitefouille_id');
    
    $value = date("Y-m-d", strtotime(trim($value) ));
    
    if ($value != '') {
			
      $sql = 'SELECT 
        	id AS oa_id, 
        	sitefouille_id, 
        	identification, 
        	arretedesignation, 
        	arreteprescription, 
        	debut, 
        	fin, 
        	oastatut_id, 
        	nature_id, 
        	conditionfouille, 
        	organismeratachement, 
        	maitriseouvrage, 
        	surfacefouille, 
        	commentaire, 
        	entiteadmin_id
        FROM oa' ;
        
      $statement = $db->prepare($sql)  
          ->where('sitefouille_id = "'.$sitefouille_id.'" ' )
          ->where('debut <= "'.$value.'" AND fin >= "'.$value.'" ') ;
      	
			if(!($statement instanceof Yab_Db_Statement))
				throw new Yab_Exception('statement must be an instance of Yab_Db_Statement');
			
			$statement->bind('?', $value);
      
			if(!count($statement))
			   $this->addError('NOT_EXISTS', self::NOT_EXISTS, $value);
			
	   }	
	}

}

// Do not clause PHP tags unless it is really necessary