<?php

class Plugin_Session extends Yab_Controller_Plugin_Abstract {
	
	public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		$registry = $loader->getRegistry ();
		$Action = $request->getAction ();
		$Controller = $request->getController ();
		//Vérifie que l'utilisteur est connecté.
		if (strtolower($Action)=="ws" && strtolower($Controller)=="index")
			return;
		if ((strtolower($Action)=="unzipjson" or strtolower($Action)=="gestionphotojson" or strtolower($Action)=="model") and (strtolower($Controller)=="sitefouille" or strtolower($Controller)=="elementrecueilli"))
			return;
		if (!$session->has('session') && $Controller != "Connexion" && $Action != "login") {
			header('location:'.$request->getBaseUrl().'/Connexion/login' ) ;
			die();
		}
		if($session->has('session')){
			//Habilitation

			//Ignore l'ajax.
			if (strpbrk(strtolower($Action), "ajax"))
				return;

			//Tous controller diff�rent du controlleur "error"
			if (strtolower($Controller)!="error") {
				//R�cup�ration des infos de l'utilisateur en session.
				$user_info = $session->get('session');
				$login = $user_info['login'] ;

				//R�cup�ration de toutes les ihm autoris� pour un profil
				$profil = new Model_Profil($user_info['profil_id']);
				$profilIhm = $profil->getProfilIhms();
				$ihms = array();
				foreach ($profilIhm->toArray() as $value){
					$profilIhm_NomIHM = $value->getIhm()->toArray() ;
					$ihms[] = $profilIhm_NomIHM["NomIHM"];

				}


				//Test si l'utilisateur poss�de l'ihm demand�.
				$monIhm = strtolower($Controller."-".$Action);
				$test = array_search($monIhm, $ihms);

				if (!$test) {
					//S'il n'est pas habilit� alors.
					$ihm = new Model_Ihm();
					$existe = (int) $ihm->search(array("NomIHM"=>$monIhm))->count();
					//On regarde si l'ihm existe dans la table ihm
					if ($existe==0) {
						$ihm = $ihm->populate(array("NomIHM"=>$monIhm, "Description"=>"A saisir."))->save();
						try {
							//On tente d'associer l'ihm au profil Administrato "1"
							$profilIhm = new Model_Profil_Ihm();
							//$profil = new Model_Profil(1);
							$profilIhm = $profilIhm->populate(array('profil_id'=>1, 'ihm_id'=>$ihm->get("Id")))->save();
							//Si le profil en cours est admin, on l'autorise d'accès.
							if ($user_info["profil_id"]==1)
								return;
						} catch (Exception $e) {
							echo $e->getMessage();
						}
					}
				}
			}
		}
	}
	
	public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {
		
	}
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {
		
	}
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {
		
	}
	
	/**
	 * Recupere les infos utilisateur en BDD
	 * 
	 * @param string $remote_user
	 *        	login utilisateur ex:"dom\login" OU "login"
	 * @return yabObject Objet LDAP
	 *        
	 * @todo Attention : toute modif est aussi a reporté aussi dans le controlleur Costream_Admin_User
	 */
	private function _getUser($remote_user, $force_ldap = false) {
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();
		$identification = $session->has ( 'identification' ) ? $session->get ( 'identification' ) : array ();
		// REMOTE_USER : DOMAIN\login
		$remote_user_domain = $remote_user;
		$remote_user = explode ( '\\', $remote_user );
		$remote_user = array_reverse ( $remote_user );
		// $domain = strtolower(array_shift($remote_user));
		$cn = array_shift ( $remote_user );
		$domain = strtolower ( array_shift ( $remote_user ) );
		$domain = (! empty ( $domain )) ? $domain : "cib";
		$CAUser = new Model_Fsn_Admin_User ();
		$res = $CAUser->getUser ( $cn );
		$res_count = $res->count ();
		// ----------- On veut recharger le ldap du user trouvé
		if (isset ( $res ) && $res_count == 1 && $force_ldap == true && $cn != "admin") {
			$ldap_user = $this->_getLdapUser ( $remote_user_domain );
			// Construction de l'objet LDAP (reduit au info utile) � retourner
			$rawYabObj = $this->_getLdapUser ();
			$ldap_objet = array (
					'cn',
					'sn',
					'description',
					'postaladdress',
					'postalcode',
					'telephonenumber',
					'givenname',
					'distinguishedname',
					'displayname',
					'memberof',
					'co',
					'department',
					'streetaddress',
					'name',
					'samaccountname',
					'mail',
					'dn',
					'employeeid' 
			);
			foreach ( $ldap_objet as $value_ldap_objet ) {
				if ($ldap_user->has ( $value_ldap_objet ))
					$attr = $ldap_user->get ( $value_ldap_objet );
				else
					$attr = "";
				$rawYabObj->set ( $value_ldap_objet, $attr );
			}
			// $ldap_info = array_slice($ldap_user->toArray(), 0, 130, true ) ;
			$ldap_info = base64_encode ( serialize ( $rawYabObj ) );
			$resRow = $res->toRow ();
			$id_user = $resRow ['id_user'];
			$costream_admin_user = new Model_Costream_Admin_User ( $id_user );
			// Mise � jours de ldap_info
			$costream_admin_user->set ( 'ldap_info', $ldap_info )->save ();
			$result = $ldap_user;
		}
		// ----------- user non trouv�, � creer
		if (isset ( $res ) && $res_count == 0 && $cn != "admin") {
			$ldap_user = $this->_getLdapUser ( $remote_user_domain );
			/*
			 * tronque la clef ldap retourne par la recherche dans l'annuaire
			 * index 2/3 => cn
			 * index 4/5 => sn
			 * index 21/22 => givenname
			 * index 37/38 => memberof
			 * index 43/44 => department
			 * index 120 => mail
			 */
			// Construction de l'objet LDAP (reduit aux infos utiles)
			$rawYabObj = $this->_getLdapUser ();
			$ldap_objet = array (
					'cn',
					'sn',
					'description',
					'postaladdress',
					'postalcode',
					'telephonenumber',
					'givenname',
					'distinguishedname',
					'displayname',
					'memberof',
					'co',
					'department',
					'streetaddress',
					'name',
					'samaccountname',
					'mail',
					'dn' 
			);
			foreach ( $ldap_objet as $value_ldap_objet ) {
				if ($ldap_user->has ( $value_ldap_objet ))
					$attr = $ldap_user->get ( $value_ldap_objet );
				else
					$attr = "";
				$rawYabObj->set ( $value_ldap_objet, $attr );
			}
			// $ldap_info = array_slice($ldap_user->toArray(), 0, 130, true ) ;
			$ldap_info = base64_encode ( serialize ( $rawYabObj ) );
			// On enregistre en BDD
			$response_value ['user_login'] = $cn;
			$response_value ['user_password'] = "";
			$response_value ['user_name'] = $ldap_user->getFirst ( 'sn' ) . " " . $ldap_user->getFirst ( 'givenname' ); // recup LDAP
			$response_value ['last_login'] = date ( "Y-m-d H:i:s" );
			$response_value ['is_active'] = 1;
			$response_value ['ldap_info'] = $ldap_info; // recup LDAP
			$response_value ['parameter'] = "";
			$response_value ['comment'] = "";
			$response_value ['user_email'] = $ldap_user->getFirst ( 'mail' ); // recup LDAP
			$response_value ['color'] = "";
			$CAUser->safePopulate ( $response_value );
			$CAUser->save ();
			// Trace de localisation des information memoire / costream /ldap
			$identification ['ldap_user'] = 'ldap';
			$session->set ( 'identification', $identification );
			// On charge ses groupes
			$this->_hasCostreamGroup ( $ldap_user, true );
			$result = $ldap_user;
		}		// ---------- user existe, maj date login
		elseif (isset ( $res ) && $res_count == 1 && $cn != "admin") {
			$resRow = $res->toRow ();
			$id_user = $resRow ['id_user'];
			$costream_admin_user = new Model_Costream_Admin_User ( $id_user );
			// Mise � jours de la date de login
			$costream_admin_user->set ( 'last_login', date ( "Y-m-d H:i:s" ) )->save ();
			// Construction de l'objet LDAP � retourner
			$rawYabObj = $this->_getLdapUser ();
			$ldap_info = base64_decode ( $resRow ['ldap_info'] );
			if ($ldap_info != serialize ( false )) {
				$ldap_info = unserialize ( $ldap_info );
			}
			if (! empty ( $ldap_info )) {
				foreach ( $ldap_info as $key => $value ) {
					$rawYabObj->set ( $key, $value );
				}
			}
			// Trace de localisation des information memoire / costream /ldap
			$identification ['ldap_user'] = 'costream';
			$session->set ( 'identification', $identification );
			$result = $rawYabObj;
		}		// ----------- Cas particulier admin
		elseif (isset ( $res ) && ($res_count == 0 || $force_ldap == true) && $cn == "admin") {
			// On recree le ldap_info
			$rawYabObj = $this->_getLdapUser ();
			$rawYabObj->set ( "cn", array (
					"admin",
					'count' => 1 
			) );
			$rawYabObj->set ( "givenname", array (
					"",
					'count' => 1 
			) );
			$rawYabObj->set ( "sn", array (
					"Super Utilisateur",
					'count' => 1 
			) );
			$CAGroup = new Model_COP_ADMIN_GROUP ();
			$list_groups = $CAGroup->fetchAll ()->where ( 'is_active = 1 ' );
			$ldapGroups = array (
					'count' => $list_groups->count () 
			);
			foreach ( $list_groups as $key_group => $value_group ) {
				$ldap_group_name = $value_group->get ( 'group_label' );
				$ldapGroups [] = 'CN=' . $ldap_group_name;
			}
			$rawYabObj->set ( "memberof", $ldapGroups );
			$ldap_info = base64_encode ( serialize ( $rawYabObj ) );
			// On enregistre en BDD
			if ($force_ldap == true && $res_count > 0) { // update
				$resRow = $res->toRow ();
				$id_user = $resRow ['id_user'];
				$response_value ['id_user'] = $id_user;
			}
			$response_value ['user_login'] = "admin";
			$response_value ['user_password'] = "password";
			$response_value ['user_name'] = "Super Utilisateur";
			$response_value ['last_login'] = date ( "Y-m-d H:i:s" );
			$response_value ['is_active'] = 1;
			$response_value ['ldap_info'] = $ldap_info;
			$response_value ['parameter'] = "";
			$response_value ['comment'] = "";
			$response_value ['user_email'] = ""; // recup LDAP
			$response_value ['color'] = "000000";
			$CAUser->safePopulate ( $response_value );
			$CAUser->save ();
			$result = $rawYabObj;
		}
		// var_dump($result);
		return $result;
	}
	
	/**
	 * Recupere les infos utilisateur du LDAP
	 */
	private function _getLdapUser($remote_user = null) {
		$domain = "cib";
		$cn = NULL;
		$class = NULL;
		if (! empty ( $remote_user )) {
			$remote_user = explode ( '\\', $remote_user );
			$remote_user = array_reverse ( $remote_user );
			// $domain = strtolower(array_shift($remote_user));
			$cn = array_shift ( $remote_user );
			$domain = strtolower ( array_shift ( $remote_user ) );
			$domain = (! empty ( $domain )) ? $domain : "cib";
			$class = 'User';
		}
		$loader = Yab_Loader::getInstance ();
		$config = $loader->getConfig ();
		$config_ldap = new Yab_Config ();
		$config_ldap->setEnvironment ( $domain );
		$config->get('ldap_config');
		$config_ldap->setFile ( $config->get ( 'ldap_config' ) );
		$ldap_server = new Yab_Ldap_Server ( $config_ldap->get ( 'host' ), $config_ldap->get ( 'login' ), $config_ldap->get ( 'password' ), $config_ldap->get ( 'port' ) );
		$ldap_user = new Yab_Ldap_Object ( $ldap_server, $cn, $class );
		return $ldap_user;
	}
	
	/**
	 * Recupere les infos group en BDD
	 * 
	 * @param objet $ldap_user        	
	 *
	 * @todo Attention : toute modif est aussi a reporté aussi dans le controlleur Costream_Admin_User
	 */
	private function _hasCostreamGroup($ldap_user, $force_ldap = false) {
		$loader = Yab_Loader::getInstance ();
		// $config = $loader->getConfig();
		$session = $loader->getSession ();
		// $registry = $loader->getRegistry();
		$identification = $session->has ( 'identification' ) ? $session->get ( 'identification' ) : array ();
		if ($session->get ( 'REMOTE_USER' ) == "admin")
			$authenticatelogin = array (
					"admin" 
			);
		else
			$authenticatelogin = $ldap_user->get ( 'cn' );
		$CAUser = new Model_Costream_Admin_User ();
		$user_info = $CAUser->fetchAll ()->where ( 'user_login = :userlogin ' )->bind ( ':userlogin', $authenticatelogin [0] );
		$user_info = $user_info->toRow ();
		$id_user = $user_info->get ( 'id_user' );
		$costream_admin_group = new Model_Costream_Admin_Group ();
		$list_groups = $costream_admin_group->fetchAll ()->where ( 'is_active = 1 ' );
		foreach ( $list_groups as $key_group => $value_group ) {
			$id_group = $value_group->get ( 'id_group' );
			$group_name = $value_group->get ( 'group_name' );
			$ldap_group_name = $value_group->get ( 'group_label' );
			$CARelationGroupUser = new Model_Costream_Admin_Usergroup ();
			$list_usergroups = $CARelationGroupUser->getRelationGroupUser ( $id_user, $id_group );
			$RelationGroupUser = $list_usergroups->count ();
			if ($RelationGroupUser != 0) {
				$identification ['group'] [$group_name] = 'costream';
				$session->set ( $group_name, 1 );
				$session->set ( 'g_costream', 1 );
			} else { // $force_ldap == true ????
			       // force le chargement depuis ldap
				if ($force_ldap == true) {
					if ($ldap_user->hasGroup ( $ldap_group_name )) {
						$identification ['group'] [$group_name] = 'ldap';
						// Ajout de la nouvelle relation Group <=> User
						$new_RelationGroupUser = array (
								'id_user' => $id_user,
								'id_group' => $id_group 
						);
						$CARelationGroupUser->setRelationGroupUser ( $id_user, $id_group );
						$session->set ( $group_name, 1 );
						$session->set ( 'g_costream', 1 );
					}					// On set tous les groupes a 1 pour le user admin
					elseif ($authenticatelogin [0] == "admin") {
						$identification ['group'] [$group_name] = 'admin';
						// Ajout de la nouvelle relation Group <=> User
						$new_RelationGroupUser = array (
								'id_user' => $id_user,
								'id_group' => $id_group 
						);
						$CARelationGroupUser->setRelationGroupUser ( $id_user, $id_group );
						$session->set ( $group_name, 1 );
						$session->set ( 'g_costream', 1 );
					}
				}
			} // var_dump($identification['group']);
		}
		$session->set ( 'force_ldap', false );
		$session->set ( 'identification', $identification );
		// var_dump($session);
	}
	
	public function GroupsUser() {
		$ldap_user_info = Yab_Loader::getInstance ()->getRegistry ()->get ( 'ldap_user' );
		$groups = $ldap_user_info->get ( 'memberof' );
		$result = array ();
		for($i = 0; $i < ($groups ['count']); $i ++) {
			$cn_group = explode ( ',', $groups [$i] );
			$groupname = explode ( '=', $cn_group [0] );
			array_push ( $result, $groupname [1] );
		}
		return $result;
	}
	
	public function isAdmin() {
		$groupsuser = $this->GroupsUser ();
		$groupsadmin = Yab_Loader::getInstance ()->getConfig ()->get ( 'group_admin' );
		foreach ( $groupsuser as $group ) {
			foreach ( $groupsadmin as $groupadmin ) {
				if ($group == $groupadmin) {
					return true;
				}
			}
		}
		return false;
	}
}