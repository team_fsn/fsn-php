<?php

class Plugin_I18n extends Yab_Controller_Plugin_Abstract {

	public function preDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {

		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
		$registry = $loader->getRegistry();
		
		if(!$registry->has('i18n'))
			return $this;
		
		$i18n = $registry->get('i18n');
		
		$lang = $session->has('lang') ? $session->get('lang') : $i18n->getLanguage();
	
		if($request->getGet()->has('lang'))
			$lang = $request->getGet()->get('lang');

		$i18n->setLanguage($lang);
		
		$session->set('lang', $lang);

	}
	
	public function postDispatch(Yab_Controller_Request $request, Yab_Controller_Response $response) {}
	public function postRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {}
	public function preRender(Yab_Controller_Request $request, Yab_Controller_Response $response) {}

}