<!-- Inclusion des scripts nécessaires a JQXTree -->
<link rel="stylesheet"
	href="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/styles/jqx.base.css"
	type="text/css" />
	
	<link rel="stylesheet"
	href="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/styles/jqx.darkblue.css"
	type="text/css" />

<script type="text/javascript"
	src="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/jqxcore.js"></script>
<script type="text/javascript"
	src="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript"
	src="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript"
	src="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript"
	src="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/jqxtree.js"></script>
<script type="text/javascript"
	src="<?php echo $request->getBaseUrl() ; ?>/js/jqwidgets/jqxcheckbox.js"></script>

<script type="text/javascript">
	$(document).ready(
			function() {
				// Create jqxTree 
				var tree = $('#jqxTree');
            var source = null;
            $.ajax({
                async: false,
                url: "<?php echo $this->getRequest('emplacement','getSite' ) ; ?>",
                success: function (data, status, xhr) {
                    source = jQuery.parseJSON(data);
                }
            });

            tree.jqxTree({ theme:'darkblue', source: source,  height: 500, width: 500 });

            // On expand
            			tree.on('expand', function (event) {
                var label = tree.jqxTree('getItem', event.args.element).label;
                var $element = $(event.args.element);
                var loader = false;
                var loaderItem = null;
                var children = $element.find('ul:first').children();
                $.each(children, function () {
                    var item = tree.jqxTree('getItem', this);
                    if (item && item.label == 'Loading...') {
                        loaderItem = item;
                        loader = true;
                        return false
                    };
                });
                if (loader) {
                    $.ajax({
                        url: loaderItem.value,
                        success: function (data, status, xhr) {
                            var items = jQuery.parseJSON(data);
                            tree.jqxTree('addTo', items, $element[0]);
                            tree.jqxTree('removeItem', loaderItem.element);
                        }
                    });
                }
            });

            			tree.on('select', function (event) {
            				var selectedItem = $('#jqxTree').jqxTree('selectedItem');
            				var div_description = $('#div_description') ;
            				if (selectedItem.id != 0)
                					updateDescription() ;
            				else
            					div_description.html('') ;
            			});

				// Create and initialize Buttons
				$('#Add').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Edit').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Remove').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Disable').jqxButton({
					height : '25px',
					width : '100px'
				});
				
				$('#Content').jqxButton({
					height : '25px',
					width : '100px'
				});
				
				$('#EnableAll').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Expand').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Collapse').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#ExpandAll').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#CollapseAll').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Next').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#Previous').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#qrcodeEMP').jqxButton({
					height : '25px',
					width : '100px'
				});
				$('#qrcodeCNT').jqxButton({
					height : '25px',
					width : '100px'
				});

				// Add 
				$('#Add').click(function() {
					var selectedItem = $('#jqxTree').jqxTree('selectedItem');
					var id = selectedItem.id ;
					var label = selectedItem.label ;
  					  
					if (selectedItem != null && !selectedItem.disabled) {

					// D�but Affichage du modal (boite de dialogue)
					
					$.ajax({
		  	type:"POST",
			url: "<?php echo $this->getRequest('emplacement','ajaxAjouter' ) ; ?>",
			data:"id=" + id,
      			success: function(returnData) {
      				if (returnData != "null") {
	        				// Affichage du modal (boite de dialogue)
	        				$('#modalTitle').html("Ajout d'un emplacement");
	        				$('#modalBody').html(returnData);
	        				$('#myModal').modal();
      			} else
          			alert("Vous ne pouvez pas ajouter de sous-emplacement à : " + label) ;
        				if(data.error)
          					alert(data.error);
       			},
       			error: function(returnData) {
        				alert('Une erreur est survenue.'); 
       			}
      		});
					
					// Fin Affichage du modal
					} else
						alert("Veuillez sélectionner un emplacement !") ;
				});

				// Edit
				$('#Edit').click(function() {
					var selectedItem = $('#jqxTree').jqxTree('selectedItem');
					var id = selectedItem.id ;
					
					if (selectedItem != null && selectedItem.id != "0" && !selectedItem.disabled) {

					// D�but Affichage du modal (boite de dialogue)
					
					$.ajax({
		  	type:"POST",
			url: "<?php echo $this->getRequest('emplacement','ajaxModifier' ) ; ?>",
			data:"label=" + selectedItem.label + "&id=" + id,
      			success: function(returnData) {
        				// Affichage du modal (boite de dialogue)
        				$('#modalTitle').html("Modification de : " + selectedItem.label);
        				$('#modalBody').html(returnData);
        				$('#myModal').modal();
        				if(data.error)
          					alert(data.error);
       			},
       			error: function(returnData) {
        				alert('Une erreur est survenue.'); 
       			}
      		});
					
					// Fin Affichage du modal
					} else
						alert("Veuillez sélectionner un emplacement !") ;
				});
				
				// Remove 
				$('#Remove').click(
						function() {
							var selectedItem = $('#jqxTree').jqxTree(
									'selectedItem');
							var id = selectedItem.id ;
							
							
							
							if (selectedItem != null && id != "0" && !selectedItem.disabled) {
								var answer = confirm("Veuillez confirmer la suppression !");
								if (answer) {
								// On supprime en base
								$.ajax({
								type:"POST",
									url: "<?php echo $this->getRequest('emplacement','ajaxSauver' ) ; ?>",
								data:"action=supprimer" + "&id=" + id
								+ "&tree=" + JSON.stringify($('#jqxTree').jqxTree('source')),
										success: function(returnData) {
											if (returnData == "0") {
												alert("Attention ! Veuillez supprimer les sous-emplacement de : " + selectedItem.label + " d'abord.") ;
											} else if (returnData == "2") {
												alert("L'emplacement sélectionné ne peut pas être supprimé car il est relié à un ou plusieurs conteneurs/éléments recueillis.") ;
											} else {
												// removes the selected item. The last parameter determines whether to refresh the Tree or not.
												// If you want to use the 'removeItem' method in a loop, set the last parameter to false and call the 'render' method after the loop.
												
												$('#jqxTree').jqxTree('removeItem',
												selectedItem.element, false);
												// update the tree.
												$('#jqxTree').jqxTree('render');												
											}
											if(data.error)
							  					alert(data.error);
										},
										error: function(returnData) {
											alert('Une erreur est survenue.'); 
										}
									});
								}
							} else
								alert("Veuillez sélectionner un emplacement !") ;
						});

				// Disable
				$('#Disable').click(
						function() {
							var selectedItem = $('#jqxTree').jqxTree(
									'selectedItem');
							if (selectedItem != null) {
								$('#jqxTree').jqxTree('disableItem',
										selectedItem.element);
							}
						});

				// Content
				
								$('#Content').click(
						function() {
							var selectedItem = $('#jqxTree').jqxTree('selectedItem');
							if (selectedItem != null) {
							

								$.ajax({
								  	type:"POST",
									url: "<?php echo $this->getRequest('emplacement','ajaxGetER' ) ; ?>",
									data:"label=" + selectedItem.label + "&id=" + selectedItem.id,
						      			success: function(returnData) {
						        				// Affichage du modal (boite de dialogue)
						        				$('#modalTitle').html("Eléments rangés dans l'emplacement : " + selectedItem.label);
						        				$('#modalBody').html(returnData);
						        				$('#myModal').modal();
						        				if(data.error)
						          					alert(data.error);
						       			},
						       			error: function(returnData) {
						        				alert('Une erreur est survenue.'); 
						       			}
						      		});
							} else
								alert("Veuillez sélectionner un emplacement !") ;
						});

				// Expand
				$('#Expand').click(
						function() {
							var selectedItem = $('#jqxTree').jqxTree(
									'selectedItem');
							if (selectedItem != null) {
								$('#jqxTree').jqxTree('expandItem',
										selectedItem.element);
							}
						});

				// Expand
				$('#Collapse').click(
						function() {
							var selectedItem = $('#jqxTree').jqxTree(
									'selectedItem');
							if (selectedItem != null) {
								$('#jqxTree').jqxTree('collapseItem',
										selectedItem.element);
							}
						});

				// Expand All
				$('#ExpandAll').click(function() {
					$('#jqxTree').jqxTree('expandAll');
				});

				// Collapse All
				$('#CollapseAll').click(function() {
					$('#jqxTree').jqxTree('collapseAll');
				});

				// Enable All
				$('#EnableAll').click(function() {
					$('#jqxTree').jqxTree('enableAll');
				});

				// Select Next Item
				$('#Next').click(
						function() {
							var selectedItem = $("#jqxTree").jqxTree(
									'selectedItem');
							var nextItem = $("#jqxTree").jqxTree('getNextItem',
									selectedItem.element);
							if (nextItem != null) {
								$("#jqxTree").jqxTree('selectItem',
										nextItem.element);
								$("#jqxTree").jqxTree('ensureVisible',
										nextItem.element);
							}
						});

				// Select Previous Item
				$('#Previous').click(
						function() {
							var selectedItem = $("#jqxTree").jqxTree(
									'selectedItem');
							var prevItem = $("#jqxTree").jqxTree('getPrevItem',
									selectedItem.element);
							if (prevItem != null) {
								$("#jqxTree").jqxTree('selectItem',
										prevItem.element);
								$("#jqxTree").jqxTree('ensureVisible',
										prevItem.element);
							}
						});

				// QRCode Emplacement
				$('#qrcodeEMP').click(
						function() {
							var selectedItem = $("#jqxTree").jqxTree(
									'selectedItem');
							if (selectedItem != null) {
								window.open("<?php echo $this->getRequest('emplacement','genererQRCode' ) ; ?>?id=" + selectedItem.id) ;
							}
						});

				// QRCode Conteneur
				$('#qrcodeCNT').click(
						function() {
							var selectedItem = $("#jqxTree").jqxTree(
									'selectedItem');
							if (selectedItem != null) {
								window.open("<?php echo $this->getRequest('conteneur','genererQRCode' ) ; ?>?id=" + selectedItem.id) ;
							}
						});

// Au chargement de la page
				
				$('#jqxTree').jqxTree('selectItem',
				$("#jqxTree").find('li:first')[0]);
				$('#jqxTree').css('visibility', 'visible');
			});
	
	function ajouter() {
		var selectedItem = $('#jqxTree').jqxTree('selectedItem');
		var label = $('#label').val() ;
		var type_id = $('#type').val() ;
		var parent_id = null ;
		var description = $('#description').val() ;
		
		if ((selectedItem.id != null) && (selectedItem.id != 0))
			parent_id = selectedItem.id ;
		if (selectedItem.id == 0)
			parent_id = null ;
		if (selectedItem != null && label != "") {
			// On ajoute en base
			$.ajax({
			type:"POST",
				url: "<?php echo $this->getRequest('emplacement','ajaxSauver' ) ; ?>",
			data:"action=ajouter" + "&label=" + label + "&type_id=" + type_id + "&parent_id=" + parent_id
			+ "&tree=" + JSON.stringify($('#jqxTree').jqxTree('source'))
			+"&description=" + description,
      			success: function(returnData) {
      				$('#resultat').html(returnData) ;
      				var id = returnData ;
      			// adds an item with label: 'item' as a child of the selected item. The last parameter determines whether to refresh the Tree or not.
      				// If you want to use the 'addTo' method in a loop, set the last parameter to false and call the 'render' method after the loop.
      				$('#jqxTree').jqxTree('addTo', {
      					label : label, id:id
      				}, selectedItem.element, false);
      				// update the tree.
      				$('#jqxTree').jqxTree('render');
      				$('#jqxTree').jqxTree('expandItem', selectedItem.element);
        				if(data.error)
          					alert(data.error);
       			},
       			error: function(returnData) {
        				alert('Une erreur est survenue.'); 
       			}
      		});
			
			// On ferme le modal
			$('#myModal').modal('hide') ;
		} else {
			alert("Veuillez saisir un nom !") ;
		}
	}
	
	function modifier() {
		var selectedItem = $('#jqxTree').jqxTree(
		'selectedItem');
		var label = selectedItem.label ;
		var label_modified = $('#label_modified').val() ;
		var id = selectedItem.id ;
		var description_modified = $('#description_modified').val() ;
		
if (selectedItem != null && label_modified != "") {	
	// On sauvegarde en base
	$.ajax({
	type:"POST",
		url: "<?php echo $this->getRequest('emplacement','ajaxSauver' ) ; ?>",
	data:"action=modifier" + "&label=" + label  + "&label_modified=" + label_modified + "&id=" + id
	+ "&tree=" + JSON.stringify($('#jqxTree').jqxTree('source'))
	+ "&description_modified=" + description_modified,
			success: function(returnData) {
				$('#resultat').html(returnData) ;
				$('#jqxTree').jqxTree('updateItem',
						{label:label_modified}, selectedItem.element);
					// update the tree.
					$('#jqxTree').jqxTree('render');
					updateDescription() ;
				if(data.error)
  					alert(data.error);
			},
			error: function(returnData) {
				alert('Une erreur est survenue.'); 
			}
		});
	
	// On ferme le modal
	$('#myModal').modal('hide') ;
} else
	alert("Veuillez saisir un nom !") ;
	}

	function updateDescription() {
		var selectedItem = $('#jqxTree').jqxTree('selectedItem');
		var id = selectedItem.id ;
		var label = selectedItem.label ;
		var div_description = $('#div_description') ;

			$.ajax({
			  	type:"POST",
				url: "<?php echo $this->getRequest('emplacement','ajaxGetDescription' ) ; ?>",
				data:"id=" + id,
	      			success: function(returnData) {
		      			var contenu_div = returnData ;
    					
    					div_description.html(contenu_div) ;
	        				if(data.error)
	          					alert(data.error);
	       			},
	       			error: function(returnData) {
	        				alert('Une erreur est survenue.'); 
	       			}
	      		});
	}
</script>

<br/><br/><br/><br/>
<div id='jqxWidget' style="text-align: center; padding-left:20%;">
	<div style='float: left;'>
		<div id='jqxTree' style="float:left;">
		</div>
		<div style='margin-left: 60px; float: left;'>
			<div style='margin-top: 10px;'>
				<input type="button" id='Add' value="Ajouter" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Edit' value="Modifier" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Remove' value="Supprimer" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Content' value="Contenu" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Disable' value="Désactiver" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Expand' value="Déplier" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Collapse' value="Plier" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='ExpandAll' size="400" value="Déplier tout" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='CollapseAll' value="Plier tout" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='EnableAll' value="Activer tout" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Next' value="suivant" />
			</div>
			<div style='margin-top: 10px;'>
				<input type="button" id='Previous' value="Précédent" />
			</div>
						<div style='margin-top: 10px;'>
				<input type="button" id='qrcodeEMP' value="QRCode (EMP)" />
			</div>
						<div style='margin-top: 10px;'>
				<input type="button" id='qrcodeCNT' value="QRCode (CNT)" />
			</div>
		</div>
		<div id="div_description" style="float:left; Overflow:auto; max-height:1000px; max-width:400px; padding:100px;">
		</div>
	</div>
</div>
</div>