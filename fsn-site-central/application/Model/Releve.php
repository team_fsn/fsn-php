<?php

class Model_Releve extends Yab_Db_Table {

	protected $_name = 'releve';

	public function getCroquis() {

		return new Model_Croquis($this->get('croquis_id'));

	}

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

	public function getAll(){
		$loader = Yab_Loader::getInstance ();  		
		$session = $loader->getSession ();
		$requeteur = "requete_" . "releve";
		$requeteurSession = ($session->has($requeteur) && !empty($session->get($requeteur))) ? $session->get($requeteur) : null;
		$whereRequeteur = (!is_null($requeteurSession) && !empty($requeteurSession)) ? "AND id IN (" . $requeteurSession . ") " : "";

		$db = $this->getTable()->getAdapter();
		$sql = "SELECT *, COUNT(id) AS nbrRequete
				FROM releve
				WHERE 1 $whereRequeteur 
				GROUP BY id";
				
		$result = $db->prepare($sql);
		return $result ;
	}

}