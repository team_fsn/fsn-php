<?php

class Model_Association_Er extends Yab_Db_Table {

	protected $_name = 'association_er';

	public function getErContenant() {

		return new Model_Er_Contenant($this->get('er_contenant_id'));

	}

	public function getErContenu() {

		return new Model_Er_Contenu($this->get('er_contenu_id'));

	}

	public function getTypeasso() {

		return new Model_Typeasso($this->get('typeasso_id'));

	}

}