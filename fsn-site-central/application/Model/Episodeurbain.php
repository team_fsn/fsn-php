<?php

class Model_Episodeurbain extends Yab_Db_Table {

	protected $_name = 'episodeurbain';

	public function getPhasechronos() {

		return $this->getTable('Model_Phasechrono')->search(array('episodeurbain_id' => $this->get('id')));

	}

	public function getAll(){
		$db = $this->getTable ()->getAdapter ();
		$loader = Yab_Loader::getInstance ();
		$session = $loader->getSession ();		

		$requeteur = "requete_" . "episodeurbain";
		$requeteurSession = ($session->has($requeteur) && !empty($session->get($requeteur))) ? $session->get($requeteur) : null;
		$whereRequeteur = (!is_null($requeteurSession) && !empty($requeteurSession)) ? "AND id IN (" . $requeteurSession . ") " : "";
		$session[$requeteur] = '';

		$sql = "SELECT *, COUNT(id) AS nbrRequete
				FROM episodeurbain 
				WHERE 1 $whereRequeteur
				GROUP BY id";
	
		$result = $db->prepare ( $sql ) ;
	
		return $result ;
	}

}