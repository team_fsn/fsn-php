<?php

class Model_ZenAlbums extends Yab_Db_Table {

	protected $_name = 'zen_albums';

	public function getSubalbums($album_id = null) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT *
					FROM zen_albums za
					WHERE za.parentid ".($album_id?"= ".$album_id:"IS NULL")." ORDER BY za.title";
		$result = $db->prepare($sql);
		return $result ;
	}

}