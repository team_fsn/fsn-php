<?php

class Model_Elementpoterie extends Yab_Db_Table {

	protected $_name = 'elementpoterie';

	public function getCaracterepate() {

		return new Model_Caracterepate($this->get('caracterepate_id'));

	}

	public function getForme() {

		return new Model_Forme($this->get('forme_id'));

	}

	public function getClassification() {

		return new Model_Classification($this->get('classification_id'));

	}

	public function getProduction() {

		return new Model_Production($this->get('production_id'));

	}

	public function getPrecisionproduction() {

		return new Model_Precisionproduction($this->get('precisionproduction_id'));

	}

	public function getElementrecueillis() {

		return $this->getTable('Model_Elementrecueilli')->search(array('elementpoterie_id' => $this->get('id')));

	}

	public function getEpInfosExport() {
    
		$db = $this->getTable()->getAdapter();
    $loader = Yab_Loader::getInstance();
    $session = $loader->getSession();
    
    $sql = 'SELECT
      	sdf.nom AS sitefouille_nom,
      	sdf.nomabrege AS sitefouille_nomabrege,
      	us.identification AS us_identification, 
      	er.numero,
      	-- ep.id, 
      	-- ep.caracterepate_id,
      	cp.caracterepate, 
      	-- ep.forme_id, 
      	fo.description AS forme,
      	-- ep.classification_id, 
      	cl.classification,
      	-- ep.production_id, 
      	pr.production,
      	ep.decor, 
      	ep.traceusage, 
      	ep.proportionconserve, 
      	-- ep.precisionproduction_id,
      	pp.`precision` 
      FROM elementpoterie ep
      INNER JOIN elementrecueilli er ON er.elementpoterie_id = ep.id
      INNER JOIN us ON us.id = er.us_id
      INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
      -- LEFT JOIN fsn_categorie fcs ON fcs.id = ep.caracterepate_id AND fcs.categorie_type = "elementpoterie_caracterepate"
      LEFT JOIN caracterepate cp ON cp.id = ep.caracterepate_id
      -- LEFT JOIN fsn_categorie fcs ON fcs.id = ep.forme_id AND fcs.categorie_type = "elementpoterie_forme"
      LEFT JOIN forme fo ON fo.id = ep.forme_id
      -- LEFT JOIN fsn_categorie fcs ON fcs.id = ep.classification_id AND fcs.categorie_type = "elementpoterie_classification"
      LEFT JOIN classification cl ON cl.id = ep.classification_id
      -- LEFT JOIN fsn_categorie fcs ON fcs.id = ep.production_id AND fcs.categorie_type = "elementpoterie_production"
      LEFT JOIN production pr ON pr.id = ep.production_id
      -- LEFT JOIN fsn_categorie fcs ON fcs.id = ep.precisionproduction_id AND fcs.categorie_type = "elementpoterie_precisionproduction"
      LEFT JOIN precisionproduction pp ON pp.id = ep.precisionproduction_id
      ORDER BY
        sitefouille_nom,
        us_identification,
        numero
          
    ';
        
    $result = $db->prepare($sql);
    
    $sitefouille_id = $session->get('sitefouille_id');
    if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
    
    return $result ;  

	}
	
	public function getEpProduction() {
	
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
	
		$sql = 'SELECT
				CASE 
  	  				WHEN fcs.categorie_value IS NULL
  	  				THEN "NA"
  	  				ELSE fcs.categorie_value
    				END AS production,
				count(*) as nb_er
				FROM elementpoterie ep
      			INNER JOIN elementrecueilli er ON er.elementpoterie_id = ep.id 
				LEFT JOIN production pr ON pr.id = ep.production_id
				LEFT JOIN fsn_categorie fcs ON fcs.id = ep.production_id AND fcs.categorie_type = "elementpoterie_production"
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY fcs.categorie_value
				ORDER BY fcs.ordre';
		
		$result = $db->prepare($sql);
	
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
	
		return $result ;
	}
	
	public function getEpForme() {
	
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
	
		$sql = 'SELECT
				CASE
  	  				WHEN fcs.categorie_value IS NULL
  	  				THEN "NA"
  	  				ELSE fcs.categorie_value
    				END AS forme,
				count(*) as nb_er
				FROM elementpoterie ep
      			INNER JOIN elementrecueilli er ON er.elementpoterie_id = ep.id
				LEFT JOIN forme ON forme.id = ep.forme_id
				LEFT JOIN fsn_categorie fcs ON fcs.id = ep.forme_id AND fcs.categorie_type = "elementpoterie_forme"
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY fcs.categorie_value
				ORDER BY fcs.ordre';
	
		$result = $db->prepare($sql);
	
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
	
		return $result ;
	}
	
	public function getEpClassification() {
	
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
	
		$sql = 'SELECT
				CASE
  	  				WHEN fcs.categorie_value IS NULL
  	  				THEN "NA"
  	  				ELSE fcs.categorie_value
    				END AS classification,
				count(*) as nb_er
				FROM elementpoterie ep
      			INNER JOIN elementrecueilli er ON er.elementpoterie_id = ep.id
				LEFT JOIN classification cs ON cs.id = ep.forme_id
				LEFT JOIN fsn_categorie fcs ON fcs.id = ep.classification_id AND fcs.categorie_type = "elementpoterie_classification"
				INNER JOIN us ON us.id = er.us_id
      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				GROUP BY fcs.categorie_value
				ORDER BY fcs.ordre';
				
	
		$result = $db->prepare($sql);
	
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
	
		return $result ;
	}
	
	public function getEpCaracteristiqueCounts(){
	
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();
	
		$sql = 'SELECT 
				CASE 
					WHEN fc.categorie_value IS NULL
					THEN "NA"
				ELSE fc.categorie_value
				END AS caracteristique,
					COUNT(*) AS nb_er
					FROM elementpoterie ep
					INNER JOIN elementrecueilli er ON er.elementpoterie_id = ep.id
					INNER JOIN us ON us.id = er.us_id
	      			INNER JOIN sitefouille sdf ON sdf.id = us.sitefouille_id
				LEFT JOIN caracterepate cr ON cr.id = ep.caracterepate_id
					LEFT JOIN fsn_categorie fc ON ep.caracterepate_id = fc.id AND fc.categorie_type = "elementpoterie_caracterepate"
					GROUP BY fc.categorie_value
					ORDER BY fc.ordre
					';
	
		$result = $db->prepare($sql);
	
		$sitefouille_id = $session->get('sitefouille_id');
		if (!empty($sitefouille_id) ) { $result = $result->where('us.sitefouille_id = "'.$sitefouille_id.'" ' ) ;  }
	
		return $result ;
	}

}