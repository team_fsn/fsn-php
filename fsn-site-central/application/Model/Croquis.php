<?php

class Model_Croquis extends Yab_Db_Table {

	protected $_name = 'croquis';

	public function getCroquisImports() {

		return $this->getTable('Model_Croquis_Import')->search(array('croquis_id' => $this->get('id')));

	}

	public function getReleves() {

		return $this->getTable('Model_Releve')->search(array('croquis_id' => $this->get('id')));

	}

	public function getUses() {

		return $this->getTable('Model_Us')->search(array('croquis_id' => $this->get('id')));

	}

	public function getAll(){
		$loader = Yab_Loader::getInstance ();  		
		$session = $loader->getSession ();
		$requeteur = "requete_" . "croquis";
		$requeteurSession = ($session->has($requeteur) && !empty($session->get($requeteur))) ? $session->get($requeteur) : null;
		$whereRequeteur = (!is_null($requeteurSession) && !empty($requeteurSession)) ? "AND id IN (" . $requeteurSession . ") " : "";

		$db = $this->getTable()->getAdapter();
		$sql = "SELECT *, COUNT(id) AS nbrRequete
				FROM croquis
				WHERE 1 $whereRequeteur 
				GROUP BY id";
				
		$result = $db->prepare($sql);
		return $result ;
	}

}