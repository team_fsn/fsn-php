<?php

class Model_UsZpwgImages extends Yab_Db_Table {

	protected $_name = 'us_zpwg_images';

	public function getUsImages($us_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "SELECT zi.id, zi.file, zi.name, zi.path
				FROM ".$this->_name." uzi
				INNER JOIN zpwg_images zi ON uzi.zpwg_images_id = zi.id
				WHERE uzi.us_id = '".$us_id."'";
		$result = $db->prepare($sql);
		return $result ;
	}

	public function addUsImages($us_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$generateGuuid = new Plugin_Guuid();
		$guuid = $generateGuuid->GetUUID();
		$sql = "INSERT INTO ".$this->_name." (id,zpwg_images_id, us_id) VALUES ('".$guuid."','".$media_id."','".$us_id."')";
		$result = $db->query($sql);
		return $result;
	}

	public function remUsImages($us_id,$media_id) {
		$db = $this->getTable()->getAdapter();
		$sql = "DELETE FROM ".$this->_name." WHERE zpwg_images_id = '".$media_id."' AND us_id = '".$us_id."'";
		$result = $db->query($sql);
		return $result;
	}

}