<?php

class Model_Phase_Us extends Yab_Db_Table {

	protected $_name = 'phase_us';

	public function getPhase() {

		return new Model_Phase($this->get('phase_id'));

	}

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

}