<?php

class Model_Troncon extends Yab_Db_Table {

	protected $_name = 'troncon';

	public function getListTroncon($unitesondage_id) {
		$db = $this->getTable()->getAdapter();
		// jfb 2016-06 mantis 318
		$loader = Yab_Loader::getInstance();
		$session = $loader->getSession();

		$where = (!is_null($unitesondage_id) && !empty($unitesondage_id)) ? " AND troncon.unitesondage_id = '$unitesondage_id' " : "" ;

		// jfb 2016-06 mantis 318 Correction de la requête ci-après : WHERE unitesondage.sitefouille_id = "'.$unitesondage_id.'"
		$sql = "SELECT
		troncon.id,
		unitesondage_id,
		troncon.identification,
		nom,
		fai_id,
		fai.identification as fai_nom,
		troncon.description,
		troncon.date_debut,
		troncon.date_fin,
		troncon.longueur,
		troncon.largeur,
		troncon.profondeur
		FROM troncon
		LEFT JOIN fai ON troncon.fai_id=fai.id
		WHERE 1 $where
		";
		return $db->prepare($sql);
	}

	public function getTronconByIdentification($ident) {
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM troncon WHERE id = '".$ident."';");
	}

	public function getNextSequenceTronconId($unitesondage_id) {
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT MAX(CAST(compteur AS UNSIGNED)) + 1 AS next_compteur
                FROM (
                SELECT
                    unitesondage.id AS unitesondage_id,
                    SUBSTRING_INDEX(troncon.identification, ".", 1) AS prefixe,
                    SUBSTRING_INDEX(troncon.identification, ".", -1) AS compteur,
                    troncon.id AS troncon_id,
                    troncon.identification
                    FROM troncon
                    INNER JOIN unitesondage ON unitesondage.id = troncon.unitesondage_id
                    WHERE unitesondage_id = "'.$unitesondage_id.'"
                ) AS T';

		$result = $db->prepare($sql)->toRow() ;
		return $result == ""? 1: $result ;
	}
	
	public function getIdentificationFAI($id_fai) {
		$db = $this->getTable('fai')->getAdapter();
		$count = $db->prepare("SELECT identification FROM fai WHERE id = '".$id_fai."'")->count();
		return $count == 0 ? "" : $db->prepare("SELECT identification FROM fai WHERE id = '".$id_fai."'")->count();
	}
	
	// récuperer la liste des US qui sont liées à ce tronçon:
	public function getListLinkedUS($id_troncon) {
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM us WHERE id IN (SELECT id_us FROM troncon_us WHERE id_troncon = '".$id_troncon."');");
	}
	
	// récuperer la liste des US qui ne sont pas liées à ce tronçon, et qui se trouvent sur ce site:
	public function getListNonLinkedUS($id_troncon, $sitefouille_id) {
		$db = $this->getTable()->getAdapter();
		return $db->prepare("SELECT * FROM us WHERE id NOT IN (SELECT id_us FROM troncon_us WHERE id_troncon = '".$id_troncon."') AND sitefouille_id = '".$sitefouille_id."';");
	}
	
	public function updateTronconUS($liste_us, $id_troncon) {
		$db = $this->getTable()->getAdapter();
		$db->delete($db->getTable('troncon_us', null), "id_troncon = '".$id_troncon."'");
		foreach ($liste_us as $id_us) {
			$db->getTable('troncon_us', null)->insert(array('id_troncon'=>$id_troncon, 'id_us'=>$id_us));
		}
	}

}