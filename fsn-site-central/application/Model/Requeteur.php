<?php

class Model_Requeteur extends Yab_Db_Table {

	protected $_name = 'requeteur';

	/*
	public function getRequeteur($select,$from,$where){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();

		$sql = $select." ".$from." ".$where;
		$result = $db->prepare($sql);
		$result = $result->toArray();
		return $result;
	}
	*/

	public function getTablesRequeteur(){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		
		// Filtre sur la Liste des tables à afficher (,"croquis","releve")
		$liste_tables_entites = array("conteneur","document","elementrecueilli","emplacement","episodeurbain","fai","intervenant","log","oa","phasechrono","sitefouille","traitement","troncon","unitesondage","us","user");
		
		//$sql = "SHOW TABLES";
		$sql = "SHOW TABLE STATUS WHERE ENGINE IS NOT NULL AND Name IN('".implode("','",$liste_tables_entites)."')";
		$result = $db->prepare($sql);
		$result = $result->toArray();
		return $result;
	}
	
	public function getTitreRequeteur($titre = null){
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT * FROM requeteur WHERE titre = "'.$titre.'"';
		$result = $db->prepare($sql);
		return $result->toArray() ;
	}

	/*
	public function getRequete($id = null){
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT * FROM requeteur WHERE id = "'.$id.'"';
		$result = $db->prepare($sql);
		return $result->toArray()[0];
		//return $result;
	}
	*/
	
	public function enregistrerRequeteur($id=null,$titre,$description,$critere,$requete,$modifiee){
		$db = $this->getTable()->getAdapter();
		$loader = Yab_Loader::getInstance();
		$user_id = Yab_Loader::getInstance()->getSession()->get('session_user_id');
		$generateGuuid = new Plugin_Guuid();
		$guuid = $generateGuuid->GetUUID();
		
		if($id == null)
			$sql = "INSERT INTO requeteur (id,user_id,titre,description,critere,requete,modified) VALUES ('".$guuid."','".$user_id."','".$titre."','".$description."','".$critere."','".$requete."','".$modifiee."')";
		else
			$sql = "UPDATE requeteur SET titre='".$titre."',description='".$description."',critere='".$critere."',requete='".$requete."',modified='".$modifiee."' WHERE id='".$id."'";
		$result = $db->query($sql);
		return $result;
	}

	public function getListeRequetes(){
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT * FROM requeteur ORDER BY titre';
		$result = $db->prepare($sql);
		return $result;
	}
	
}