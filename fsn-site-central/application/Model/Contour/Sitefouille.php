<?php

class Model_Contour_Sitefouille extends Yab_Db_Table {

	protected $_name = 'contour_sitefouille';

	public function getSitefouille() {

		return new Model_Sitefouille($this->get('sitefouille_id'));

	}

}