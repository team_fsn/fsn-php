<?php

class Model_Precisionproduction extends Yab_Db_Table {

	protected $_name = 'precisionproduction';

	public function getElementpoteries() {

		return $this->getTable('Model_Elementpoterie')->search(array('precisionproduction_id' => $this->get('id')));

	}

	public function getAllPrecisions(){
		$db = $this->getTable()->getAdapter();
		$sql = 'SELECT *
				FROM prod_precisionprod, precisionproduction
				WHERE prod_precisionprod.precisionprod_id = precisionproduction.id';
		return $db->prepare($sql);
	}

}