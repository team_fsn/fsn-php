<?php

class Model_Us_Intervenant extends Yab_Db_Table {

	protected $_name = 'us_intervenant';

	public function getIntervenant() {

		return new Model_Intervenant($this->get('intervenant_id'));

	}

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

}