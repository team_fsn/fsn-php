<?php

class Model_Us_Oa extends Yab_Db_Table {

	protected $_name = 'us_oa';

	public function getUs() {

		return new Model_Us($this->get('us_id'));

	}

	public function getOa() {

		return new Model_Oa($this->get('oa_id'));

	}

}