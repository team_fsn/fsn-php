<?php
class Model_Fai_Extension_Value extends Yab_Db_Table {

	protected $_name = 'fai_extension_value';
	
	public function deleteExtension($fai_id){
        $this->delete( "fai_id = '".$fai_id."'" );
    }
    
    public function getFaiExtensionValue($fai_id){
    	$db = $this->getTable ()->getAdapter ();
    	$loader = Yab_Loader::getInstance ();
    	$session = $loader->getSession ();
    
    	$sql = "select extension_id, extension_value from fai_extension_value where fai_id='".$fai_id."'" ;
    
    	$result = $db->prepare ( $sql );
    	$result = $result->toArray ();
    	return $result;
    }

    public function getFaiExtensionWithCategorieValue($fai_id){
        $db = $this->getTable ()->getAdapter ();
        $loader = Yab_Loader::getInstance ();
        $session = $loader->getSession ();
    
        $sql = "select cat.categorie_value as extension_field, fev.extension_id, fev.extension_value
                from fai_extension_value fev
                INNER JOIN fsn_categorie cat ON fev.extension_id = cat.id
                where fai_id='".$fai_id."'" ;
    
        $result = $db->prepare ( $sql );
        $result = $result->toArray ();
        return $result;
    }

    public function getTypeFieldFaiExtension($fai_id, $extension_id){
        $db = $this->getTable ()->getAdapter ();
        $loader = Yab_Loader::getInstance ();
        $session = $loader->getSession ();
    
        $sql = "select ext.valeur as type_extension_field 
                from fai_extension_value fev
                INNER JOIN fai_extension ext ON fev.extension_id = ext.id
                where fai_id='".$fai_id."' and extension_id=".$extension_id ;
    
        $result = $db->prepare ( $sql );
        // $result = $result->toRow();
        return $result;
    }

}