<?php

	if(!empty($_POST)){
		$root_path = dirname(dirname($_SERVER['SCRIPT_FILENAME']));

		$config = file_get_contents("../config/config_template.yaml");
		foreach($_POST as $key => $value){
			$config = str_replace("__".$key."__",$value,$config);
		}
		$config = str_replace("__fsn_env_path__",$root_path,$config);
		$root_explode = explode("/",$root_path);
		$config = str_replace("__fsn_env_filename__",$root_explode[count($root_explode)-1],$config);
		$config = str_replace("__fsn_site_serveur__",$_SERVER['HTTP_HOST'],$config);
		$config = str_replace('${version.name}','Local',$config);
		$config = str_replace('${livrable.version.name}','Local',$config);
		
		file_put_contents("../config/application.yaml",$config);
		header("Location: ../");
		die;
	}

?>

<html lang="fr-FR">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<header>
			<H1>Fiche Stratigraphique Numérique</H1>
		</header>
		<nav>
		</nav>
		<section>
			<H2>Installation</H2>
			<form method="POST" action="install.php">
				<div class="control-group">
					<label for="fsn_env_nom">Nom de votre environnement FSN</label>
					<input id="fsn_env_name" type="text" name="fsn_env_name" value="" required="true" />
				</div>
				<div class="control-group">
					<label for="fsn_env_nom">Token d'accès au Web Service</label>
					<input id="fsn_env_token" type="text" name="fsn_ws_token" value="" required="true" />
				</div>
				<div>
					<label for="fsn_port_ws">Port du Web Service</label>
					<input id="fsn_port_ws" type="text" name="fsn_port_ws" value="8084" required="true" />
				</div>
				<div class="control-group">
					<label for="fsn_bdd">ip serveur de la base de données</label>
					<input id="fsn_bdd_server" type="text" name="fsn_bdd_server" value="localhost" required="true" />
				</div>
				<div>
					<label for="fsn_bdd">Nom de la base de données</label>
					<input id="fsn_bdd" type="text" name="fsn_bdd" value="" required="true" />
				</div>
				<div class="control-group">
					<label for="fsn_bdd_login">Login de la base de données</label>
					<input id="fsn_bdd_login" type="text" name="fsn_bdd_login" value="root" required="true" />
				</div>
				<div class="control-group">
					<label for="fsn_bdd_password">Mot de passe de la base de données</label>
					<input id="fsn_bdd_password" type="password" name="fsn_bdd_password" value="" required="true" />
				</div>
				<div class="control-group">
					<label for="fsn_bdd_port">Port de la base de données</label>
					<input id="fsn_bdd_port" type="number" name="fsn_bdd_port" value="3306" required="true" />
				</div>
				<div class="control-group">
					<input type="submit" value="Installer" />
				</div>
			</form>
		</section>
		<footer>
		</footer>
	</body>
</html>