/**
 * Contrôle de la saisie de l'utilisateur
 */
$(function() {
	    var maxlglogin = 8; // longueur maximale du login : 8 c.
	    var msgerrlogin = "Le login ne doit pas dépasser 8 caractères.";
		$('#login').change(function() { // si le login change, contrôler sa longueur
			if ($(this).val().length > maxlglogin) {
				alert(msgerrlogin);
			}
		});
		$('form').submit(function(event) { // si le formulaire est submité, contrôler la longueur du login
			if ($('#login').val().length > maxlglogin) {
				alert(msgerrlogin);
				event.preventDefault(); // cancel de l'action de submit
	     	}
		});
});
