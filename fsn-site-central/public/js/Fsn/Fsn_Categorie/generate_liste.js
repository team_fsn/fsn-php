var data_list = [];
var displayed_list = [];
var nb_relation = 3;
var maxordre = 0;
var displayed_elements = 0;

var main_template =
        '<div>' +
            '<table id="tableReference" class="categorie_table_ref" style="margin: 0 auto; width: 100%">' +
                '<tbody id="tbodyReference"></tbody>' +
            '</table>' +
        '</div>';

var head_table_result = '<tr class="table_head gestion_bordures"></tr>'
    +                   '<tr class="white gestion_bordures border_ref">'
    +                       '<th class="ref_gestion_taille_affichage th_center txt-position padding_reference" id="l_libelle">Libellé</th>'
    +                       '<th class="ref_gestion_taille_affichage th_center" id="l_visible">Visible</th>'
    +                       '<th class="ref_gestion_taille_affichage th_center" id="l_ordre">Ordre'
    +                           '<input type="hidden" id="ordreMax" value="__MAXORDER__" />'
    +                       '</th>'
    +                       '<th colspan="2" class="ref_gestion_taille_affichage txt-position"></th>'
    +                   '</tr>';

function row_template(item) {
    var row =
        '<tr class="gestion_tr_color_ref gestion_bordures">' +
        '<td class="txt-position">' +
        '<input class="input-transparent input_style" ' +
        'type="text" name="libelle" ' +
        'id="libelle_' + item.id + '"' +
        'value="' + item.categorie_value + '" ' +
        'onchange="ajaxReference(\'updateLibelle\',' + item.id + ');" ' +
        'onFocus="notifyUpdate()" />' +
        '</td>' +
        '<td class="ref_gestion_taille_affichage th_center">' +
        '<input type="checkbox" name="visibleCreer" id="visible_' + item.id + '" ' +
        ((item.visible) ? 'checked="checked"' : '' ) +
        'onClick="ajaxReference(\'updateVisible\',' + item.id + ');" />' +
        '</td>' +
        '<td class="ref_gestion_taille_affichage th_center">' +
        '<input type="hidden" id="ordreA_'+item.id+'" value="'+item.ordre+'"/>' +
        '<select class="color select_style" style="width:50px;" ' +
        'id="ordreN_'+item.id+'" ' +
        'name="ordre"' +
        'onchange="ajaxReference(\'updateOrdre\','+item.id+');">' +
        afficherOrdre(item.ordre) +
        '</select>' +
        '</td>' +
        '<td class="th_center">' +
        (nb_relation ? '<span style="font-size: 18px;" ' +
                'class="glyphicon glyphicon-link" ' +
                'onClick="ajoutRelation(\'ajaxAfficher\', \'consulter\', \''+item.id+'\')"  ' +
                'title="Ajouter une relation" >' +
                '</span>'
                : ''
        ) +
        '</td>' +
        '<td class="th_center">' +
        '<span name="delete" id="delete" value="" ' +
        'class="glyphicon glyphicon-remove-circle alert-danger" ' +
        'style="font-size: 18px;" alt="Supprimer" ' +
        'title="Supprimer cette cat&eacute;gorie"' +
        (item.system === "0" ?'onClick="ajaxReference(\'delete\','+item.id+')"'
            :'') +'>'
        '</span>' +
        '</td>' +
        '</tr>';
    return row;
}

function afficherOrdre(num_selected){
    var opt = "";
    for (var i = 1; i <= data_list.length; i++) {
        if (i === num_selected)
            opt += "<option selected>"+i+"</option>";
        else
            opt += "<option>"+i+"</option>";
    }
    return opt;
}

function generatehtml(){
    var main_html = main_template.replace("__MAXORDER__", maxordre);
    var body = document.getElementById("listeRef");
    body.innerHTML = main_html;
    refreshResult(data_list);
}

function appendNextResult(){
    var next = "";
    var nbNextElem = displayed_list.length > displayed_elements+20? displayed_elements+20: displayed_list.length;
    for(;displayed_elements < nbNextElem; displayed_elements++ )
        next += row_template(displayed_list[displayed_elements],displayed_list[displayed_elements].ordre);
    var tbody = document.getElementById("tbodyReference");
    tbody.innerHTML = tbody.innerHTML + next;
}

var scrollLoader = function() {
    if($(window).scrollTop() + $(window).height() > $(document).height()-200
        && document.getElementById("tbodyReference")) {
        $(window).unbind('scroll');
        appendNextResult();
        $(window).scroll(scrollLoader);
    }
};

$(window).scroll(scrollLoader);

function searchDico(list,terme){
    var result = [];
    list.forEach(function(elem){
        if(elem.categorie_value.startsWith(terme))
            result.push(elem);
    });
    return result;
}

function refreshResult(list){
    displayed_list = list;
    displayed_elements = 0;
    document.getElementById("tbodyReference").innerHTML = head_table_result
    appendNextResult();
}

function searchAndResult(terme) {
    refreshResult(searchDico(data_list,terme));
}

function localeStratWith(chaine,terme){
    return !(terme.localeCompare(chaine.substr(0,terme.length)));
}