/**
 * Requêteur (JFB 2016-07)
 */
    $("#form_selection").on('submit', function(e) {
	    e.preventDefault(); // empêcher le comportement par défaut du navigateur sur le formulaire
    });
    
    var topRequeteModifiee = false;
	var topRequeteEditee = false;
	var requeteIsValid = false;
	
	var moduleFonctionnel = ''; // variable de travail pour l'accès à fsn_categorie
	var notSort = false; // Empeche le tri des lignes de requete

	var requeteEnBase = {};

    var listeTableReflect = {};
    var optionsElementRequeteur = '';
	var baseUrl = '';

	var objCategorieEntete = ''; //obj categorie correspondant aux types poteries uniquement
	var texteRequeteurGlobal = '';

	var autoCompleteArray = new Array(); // Tableau de travail à 2 dimensions pour mémoriser les autocomplete
	
	var requeteObjO = { // Requête à enregistrer
			requeteNat : { // requête en langage naturel
				selectRequeteur : '',
				fromRequeteur : '',
				whereRequeteur : ''
			},
			requeteTec : { // requête technique
				selectTechnique : '',
				fromTechnique : '',
				innerTechnique: '',
				whereTechnique : ''
			}
	};
	var criteresArrayO = []; // Tableau des critères (critereObjO)
	var critereObjO = { // Objet critère de la requête à enregistrer
			categorieRequeteurValue : '',
			categorieTechniqueValue : '',
			typeRequeteur : '',
			aliasTable : '',
			aliasTableJoin : '',
			niveau : '',
			elementRequeteurValue : '',
			elementTechniqueValue : '',
			datedebutRequeteur : '',
			datefinRequeteur : '',
			minRequeteur : 0,
			maxRequeteur : 0,
			radioCheckedRequeteur : '',
			radioCheckedTechnique : '',
			texteRequeteur : ''
	};	
	
	function lancerTableReflect(table) {
		// classe utilisée pour transmettre un fichier en ajax
		var form = new FormData();
		form.append("table",table);

		var url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/ajaxTableReflect';
        $.ajax({
          async: false, // synchrone
          url: url,
          data: form,
          processData : false,
          type: "POST",
          contentType : false,
          success: function (data) {
           	//alert('Appel ajax success');
        	// alert(data);
        	if(data)
        		listeTableReflect = JSON.parse(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseJSON) {
              alert(jqXHR.responseJSON);
            }
            else {
              alert(jqXHR.responseText);
            }
            console.log("1:", jqXHR, "2:", textStatus, "3:", errorThrown);
          }
        }); // fin $.ajax({
	}

	function optionsRequeteur() { // Constituer les balises d'option du select de l'élément à partir de la liste des colonnes du table reflect
		for (var i in listeTableReflect) { // pour chaque élément de la liste des colonnes du table reflect
			if (listeTableReflect[i].nom) { // pour la colonne identifiée par son nom
				var optionElementRequeteur = '';
				optionElementRequeteur += listeTableReflect[i].nom;
				optionElementRequeteurT = traduireRequeteur(optionElementRequeteur);
				if (listeTableReflect[i].type == 'Foreign') { // si l'on a affaire à une clef étrangère
					//optionElementRequeteur += listeTableReflect[i].table + ':';
					optionElementRequeteur += ' (' + listeTableReflect[i].table + ')';
					optionElementRequeteurT += ' (' + traduireRequeteur(listeTableReflect[i].table) + ')';
				}
				optionsElementRequeteur += '<option value="'
											+ optionElementRequeteur
											+ '"'
											+ ' data-reqtype="'
											+ listeTableReflect[i].type
											+ '"'
											+ ' data-reqcategorie="'
											+ listeTableReflect[i].categorie_type
											+ '"'
											+ '>'
											+ optionElementRequeteurT
											+ '</option>';
			}
		}
	}
	
    $(function() {
    	
    	$('#spanLegende_Requeteur').text('');
    	
    	$('#requeteArea').hide();
    	// $('#selectRequeteurArea').prop('disabled', true);
    	// $('#fromRequeteurArea').prop('disabled', true);
    	// $('#whereRequeteurArea').prop('disabled', true);

    	// Pour le moment, pour des raisons de sécurité la requête SQL ne peut pas être modifiée
    	// $('#selectTechniqueArea').prop('disabled', true);
    	// $('#fromTechniqueArea').prop('disabled', true);
    	// $('#innerTechniqueArea').prop('disabled', true);
    	// $('#whereTechniqueArea').prop('disabled', true);
    	$("#requeteurArea").prop('disabled', true);
    	$("#techniqueArea").prop('disabled', true);

    	baseUrl = document.getElementById("baseUrl").value;
    	
    	$('[name*="categorie_Requeteur"]:first').change(function() {
    		//var nouvelle_valeur = $('[name*="categorie_Requeteur"]:first > option:selected').attr('value');
    		var valeurTechnique = $(this).children('option:selected').attr('value');
    		//var valeurRequeteur = $(this).children('option:selected').val();
    		$('#comparaison_Requeteur1').html('<img src="http://'+window.location.hostname+baseUrl+'/images/loading.gif">');
    		var valeurRequeteur = traduireRequeteur(valeurTechnique);
    		$('#categorieprincipale_Requeteur').attr('value',valeurRequeteur);
    		$('#categorieprincipale_Technique').attr('value',valeurTechnique);
    		//$('#categorie_Technique1').attr('value',valeurTechnique);
    		afficherLegendeRequeteur();
    		lancerTableReflect(valeurTechnique);
    		optionsElementRequeteur = '<option value="">Champs...</option>';
    		optionsRequeteur();
    		$('#comparaison_Requeteur1').html('');
       		$('#element_Requeteur1').html(optionsElementRequeteur);
    	});    	 
    });

    function afficherLegendeRequeteur() {
		$('#spanLegende_Requeteur').hide().fadeIn(3000);
		$('#spanLegende_Requeteur').text('"'+$('#categorieprincipale_Requeteur').attr('value')+'"');
		$('#spanLegende_Requeteur').append(" de la requête");
    }
    
    function traduireRequeteur(libelle) {
    	var returnData = '';
		// classe utilisée pour transmettre un fichier en ajax
		var form = new FormData();
		form.append("donnee",libelle);
		baseUrl = document.getElementById("baseUrl").value;
		var url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/traduireRequeteur';
        $.ajax({
          async: false, // synchrone
          url: url,
          data: form,
          processData : false,
          type: "POST",
          contentType : false,
          success: function (data) {
        	  //alert(data);
        	  returnData = data;
          },
          error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseJSON) {
              alert(jqXHR.responseJSON);
            }
            else {
              alert(jqXHR.responseText);
            }
            console.log("1:", jqXHR, "2:", textStatus, "3:", errorThrown);
          }
        }); // fin $.ajax({
        return returnData;
    }
    
    function validerRowRequeteur(obj,recuperation) { // Validation de la ligne
    	topRequeteModifiee = true;
		requeteIsValid = true;
		
		var rowLigneValidee = $(obj).parent().parent();
		var topLigneValidee = rowLigneValidee.children('input[name*="topLigneValidee"]');
		var topLigneValideeSave = topLigneValidee.attr('value');
		var topOperateurComparaison = false;
		topLigneValidee.attr('value','Validante');
		var selectedCategorieValidant = rowLigneValidee.find('select[name*="categorie_Requeteur"]').children('option:selected').attr('value');
		var selectedElementValidant = rowLigneValidee.find('select[name*="element_Requeteur"]').children('option:selected').attr('value');
		var niveauLigneValidee = rowLigneValidee.children('input[name*="niveauLigneValidee"]').attr('value');
		var aliasTableLigneValidee = rowLigneValidee.children('input[name="aliasTable"]').attr('value');

		if (!selectedElementValidant) {
			alert('Veuillez sélectionner un élément !');
			return false;
		}
		
		var topPosForeign = selectedElementValidant.indexOf(' (');
		var ligneValideeSansClefEtrangere = (topPosForeign == -1);
		var ligneValideeAvecClefEtrangere = !ligneValideeSansClefEtrangere;

		$('#table_Requeteur').children('tbody').children('tr').each(function(){ // trier la ligne validée

				if ((topLigneValideeSave == 'Validée') || recuperation || notSort)
					return false; // ne pas retrier les lignes déjà validées
				var selectedCategorieCourant = $(this).find('select[name*="categorie_Requeteur"]').children('option:selected').attr('value');
				var selectedElementCourant = $(this).find('select[name*="element_Requeteur"]').children('option:selected').attr('value');
				var niveauLigneCourant = $(this).children('input[name*="niveauLigneValidee"]').attr('value');
				var aliasTableLigneCourant = $(this).children('input[name="aliasTable"]').attr('value');
				var topLigneCourant = $(this).children('input[name*="topLigneValidee"]').attr('value');
				if ((niveauLigneValidee == niveauLigneCourant)
				 && (selectedCategorieValidant == selectedCategorieCourant)
				 && (aliasTableLigneValidee == aliasTableLigneCourant)
				 && (topLigneCourant == 'Validée')) {
					if (selectedElementValidant == selectedElementCourant) {
						if ((topLigneValideeSave == 'Validée') || ligneValideeAvecClefEtrangere)
							return false;
						else {
							$(this).after(rowLigneValidee);
							topOperateurComparaison = true; // mêmes catégorie élément que le précédent : mettre "ou"
						}
					}
					else if (selectedElementValidant < selectedElementCourant) { // déplacer la ligne validée avant l'autre
							$(this).before(rowLigneValidee);
							return false;
					}
					else if (selectedElementCourant.indexOf(' (') == -1)
							$(this).after(rowLigneValidee); // déplacer la ligne validée après l'autre
					else { // déplacer la ligne validée avant l'autre
							$(this).before(rowLigneValidee);
							return false;
					}
				}
		});		

		if (topOperateurComparaison) {
			// préfixer l'opérateur de comparaison de la ligne validée par "ou"
			rowLigneValidee.children('td[name*="comparaison_Requeteur"]').prepend('ou ');
		}
		
		// Verrouiller la catégorie et l'élement
		rowLigneValidee.find('select[name*="categorie_Requeteur"]').prop('disabled', true);
		rowLigneValidee.find('select[name*="element_Requeteur"]').prop('disabled', true);
		topLigneValidee.attr('value','Validée');
		/* Remplacer le bouton de validation par un bouton de signe + d'ajout
		spanAjouterRequeteur = rowLigneValidee.find('span[name="valider_Requeteur"]');
		spanAjouterRequeteur.attr('class', "glyphicon glyphicon-plus-sign");
		spanAjouterRequeteur.attr('alt', "Ajouter");
		spanAjouterRequeteur.attr('title', "Ajouter");
		*/
		spanAjouterRequeteur = rowLigneValidee.find('span[name="ajouter_Requeteur"]');
		spanAjouterRequeteur.attr('class', "glyphicon glyphicon-plus-sign");
		
		if (!$('#modeComplet').prop('checked')) { // hors mode complet d'affichage, masquer les lignes de travail
			if (selectedCategorieValidant == "fsn_categorie") {
				if ((selectedElementValidant == "categorie_object")
				 || (selectedElementValidant == "categorie_type")) 
					rowLigneValidee.hide();
			}
		}
		
		//alert(topLigneValideeSave);alert(selectedCategorieValidant);alert(selectedElementValidant);
		if ((topLigneValideeSave != 'Validée') && !recuperation
			&& (selectedCategorieValidant == "fsn_categorie")
			&& (selectedElementValidant == "categorie_key")) {
			//alert("ajouter la ligne");
			ajouterRowRequeteur(obj,topLigneValideeSave,recuperation);
		}
    }
	    
	function ajouterRowRequeteur(obj,topLigneValideeSave,recuperation) { // Ajout de la ligne suivante
		// Préparation de la nouvelle ligne à ajouter après la ligne validée
		var rowLigneValidee = $(obj).parent().parent();
		var selectedCategorieValidant = rowLigneValidee.find('select[name*="categorie_Requeteur"]').children('option:selected').attr('value');
		var selectedElementCategorieValidant = rowLigneValidee.find('select[name*="element_Requeteur"]').children('option:selected').attr('data-reqcategorie');
		var selectedElementValidant = rowLigneValidee.find('select[name*="element_Requeteur"]').children('option:selected').attr('value');
		var niveauLigneValidee = rowLigneValidee.children('input[name*="niveauLigneValidee"]').attr('value');
		var aliasTableLigneValidee = rowLigneValidee.children('input[name="aliasTable"]').attr('value');

		var niveau = 0;
		var topPosForeign = selectedElementValidant.indexOf(' (');
		var selectedCategorieNouvelleLigne = '';
		var aliasTableNouvelleLigne = '';
		var ligneValideeSansClefEtrangere = (topPosForeign == -1);
		var ligneValideeAvecClefEtrangere = !ligneValideeSansClefEtrangere;
		if (ligneValideeSansClefEtrangere || ((topLigneValideeSave == 'Validée') && !recuperation)) {
			/* Cas :
			 * la ligne validée ne pose pas une clef étrangère
			 * ou la ligne validée pose une clef étrangère
			 * 		mais était déjà validée sans provenir de la récupération de requête
			 * Dans ce cas on ajoute une nouvelle ligne de mêmes catégorie et alias
			 */
			selectedCategorieNouvelleLigne = selectedCategorieValidant;
			aliasTableNouvelleLigne = aliasTableLigneValidee;
		}
		else {
			/* Sinon :
			 * la ligne validée pose une clef étrangère et
			 * elle n'était pas déjà validée ou si elle l'était déjà provient de la récupération de la requête
			 * Dans ce cas on ajoute le niveau d'indentation requis par cette clef étrangère
			 */
			var posDebForeign = topPosForeign + 2;
			var posFinForeign = selectedElementValidant.indexOf(')') ;
			selectedCategorieNouvelleLigne = selectedElementValidant.substring(posDebForeign,posFinForeign);
			niveau++;
			var jetonRequeteur = parseInt($('#jeton_Requeteur').attr('value')) + 1;
			aliasTableNouvelleLigne = 'a' + jetonRequeteur;
			rowLigneValidee.children('input[name="aliasTableJoin"]').attr('value',aliasTableNouvelleLigne);
			$('#jeton_Requeteur').attr('value',jetonRequeteur);
    	}
		if (recuperation) // la catégorie/entité et l'élément/champs seront alimentés à la récupération
			selectedCategorieNouvelleLigne = "Récupération";
		
		// Formatage du niveau d'indentation
		var niveauNouvelleLigne = parseInt(niveauLigneValidee) + niveau;
		var indentationNiveau = '';
		for (var i = 0; i < niveauNouvelleLigne; i++)
			indentationNiveau = indentationNiveau + '&nbsp;&rsaquo;&nbsp;';
		
		// Formatage HTML de la nouvelle ligne
		var nouvelleTR = 
				'<tr name="nouvelleLigne">'
			+	'	<input type="hidden" name="topLigneValidee" value="">'
			+	'	<input type="hidden" name="niveauLigneValidee" value="' + niveauNouvelleLigne + '">'
			+	'	<input type="hidden" name="aliasTable" value="' + aliasTableNouvelleLigne + '">'
			+	'	<input type="hidden" name="aliasTableJoin" value="">'
			+	'	<td>'
			+	'		<span style="float:left;" name="indentationNiveau">' + indentationNiveau + '</span>'
			+	'		<select	name="categorie_Requeteur" class="col-md-10 col-xs-10" type="text">'
			+	'		</select>'
			+	'	</td>'
			+	'	<td>'
			+	'		<select name="element_Requeteur" class="col-md-10 col-xs-10" type="text"'
			+	'				onchange="changeElementRequeteur(this);">'
			+	'		</select>'
			+	'		<input type="hidden" name="type_Requeteur" value="">'
			+	'	</td>'
			+	'	<td name="comparaison_Requeteur">'
			+	'	</td>'
			+	'	<td name="valorisation_Requeteur">'
			+	'	</td>'
			+	'	<td>'
			+	'		<span	class="glyphicon glyphicon-ok-sign" style="font-size: 18px;"'
			+	'				alt="Valider" title="Valider" name="valider_Requeteur"'
			+	'				onclick="validerRowRequeteur(this);"></span>'
			+	'	</td>'
			+	'	<td>'
			+	'		<span	style="font-size: 18px;"'
			+	'				alt="Ajouter" title="Ajouter" name="ajouter_Requeteur"'
			+	'				onclick="ajouterRowRequeteur(this,\'Validée\');"></span>'
			+	'	</td>'
			+	'	<td>'
			+	'		<span	class="glyphicon glyphicon-remove-circle alert-danger" style="font-size: 18px;"'
			+	'				alt="Supprimer" title="Supprimer" name="supprimer_Requeteur"'
			+	'				onclick="supprimerRowRequeteur(this);"></span>'
			+	'	</td>'
			+	'</tr>';

		var rowLastLigne = $('#table_Requeteur').find('tr:last');

		if(selectedCategorieValidant == "fsn_categorie"){
			rowLigneValidee.after(nouvelleTR);
			notSort = false;
		}
		else{
			rowLastLigne.after(nouvelleTR);
			notSort = true;
		}
		
		//var typeLigneValidee = rowLigneValidee.find('input[name*="type_Requeteur"]').attr('value');

		// Forcer la catégorie de la nouvelle ligne (option selected)
		var optionsCategorieRequeteur =
			'<option value="' + selectedCategorieNouvelleLigne + '" selected>' + traduireRequeteur(selectedCategorieNouvelleLigne) + '</option>';

		// Intégration dans la page HTML de la catégorie de la nouvelle ligne et verrouillage de la catégorie
		var firstNouvelleLigne = $('#table_Requeteur').find('tr[name*="nouvelleLigne"]:first');
		var firstNouvelleLigneCategorie = firstNouvelleLigne.find('select[name*="categorie_Requeteur"]');
		firstNouvelleLigneCategorie.html(optionsCategorieRequeteur);
		firstNouvelleLigneCategorie.prop('disabled', true);
		
		if (!$('#modeComplet').prop('checked')) // hors mode complet
			if (selectedCategorieNouvelleLigne == "fsn_categorie") // masquer fsn_categorie
				firstNouvelleLigneCategorie.hide();

		// Etablir quand il faut alimenter les options de l'élement pour la catégorie de la nouvelle ligne
		var firstNouvelleLigneElement = firstNouvelleLigne.find('select[name="element_Requeteur"]');
		var alimOptionsElementRequeteur = true;		
		if (topLigneValideeSave == 'Validée') { // si la ligne validée était déjà validée
			if (selectedCategorieNouvelleLigne == "fsn_categorie") {
				alimOptionsElementRequeteur = false;
				optionsElementRequeteur =
					'<option value="' + 'categorie_value' + '"' + ' data-reqtype="String"' + 'selected>' + traduireRequeteur('categorie_value') + '</option>';
			}
			else {
				alimOptionsElementRequeteur = true;
			}
		}
		else if (recuperation) { // s'il s'agit d'une récupération de requête
			alimOptionsElementRequeteur = false;
		}
		else if (selectedCategorieNouvelleLigne == "fsn_categorie") {
			if (ligneValideeAvecClefEtrangere) {
				alimOptionsElementRequeteur = false;
				optionsElementRequeteur =
					'<option value="' + 'categorie_object' + '"' + ' data-reqtype="String"' + 'selected>' + traduireRequeteur('categorie_object') + '</option>';
			}
			else { // ligneValideeSansClefEtrangere et selectedCategorieNouvelleLigne = "fsn_categorie"
				if (selectedElementValidant == "categorie_object") {
					alimOptionsElementRequeteur = false;
					optionsElementRequeteur =
						'<option value="' + 'categorie_type' + '"' + ' data-reqtype="String"' + 'selected>' + traduireRequeteur('categorie_type') + '</option>';
				}
				else if (selectedElementValidant == "categorie_type") {
					alimOptionsElementRequeteur = false;

					var thisElementRequeteur = $('#table_Requeteur').find("tr[id='tr_Requeteur1']").find('select[name*="element_Requeteur"]');
					var elementTechniqueValue = thisElementRequeteur.children('option:selected').attr('value');
										
					// Cas champs Type fai 
					if(elementTechniqueValue == "typefai_id (fsn_categorie)")					
						rowLigneValidee.find('input[name="texte_Requeteur"]').attr('value',elementTechniqueValue);
					
					if (rowLigneValidee.find('input[name="texte_Requeteur"]').val() == "categorie_type") {
						optionsElementRequeteur =
							'<option value="' + 'categorie_key' + '"' + ' data-reqtype="String"' + 'selected>' + traduireRequeteur('categorie_key') + '</option>';
					}
					else {
						optionsElementRequeteur =
							'<option value="' + 'categorie_value' + '"' + ' data-reqtype="String"' + 'selected>' + traduireRequeteur('categorie_value') + '</option>';
					}
				}
				else if (selectedElementValidant == "categorie_key") {
					alimOptionsElementRequeteur = false;
					optionsElementRequeteur =
						'<option value="' + 'categorie_type' + '"' + ' data-reqtype="String"' + 'selected>' + traduireRequeteur('categorie_type') + '</option>';
				}
				else {
					alimOptionsElementRequeteur = true;
				}
			}
		}
		else {
			alimOptionsElementRequeteur = true;
		}

		var firstNouvelleLigneComparaison = firstNouvelleLigne.children('td[name="comparaison_Requeteur"]');
		// Alimentation des options de l'élément de la nouvelle ligne le cas échéant avec un gif animé d'attente
		if (alimOptionsElementRequeteur) {
			firstNouvelleLigneComparaison.html('<img src="http://'+window.location.hostname+baseUrl+'/images/loading.gif">');
			lancerTableReflect(selectedCategorieNouvelleLigne);
			optionsElementRequeteur = '<option value="">Champs...</option>';
			optionsRequeteur();
			firstNouvelleLigneComparaison.html('');
		}

		// Intégration dans la page HTML de l'élement de la nouvelle ligne
		firstNouvelleLigneElement.html(optionsElementRequeteur);
		firstNouvelleLigne.removeAttr('name');
		
		if (!alimOptionsElementRequeteur && !recuperation
				&& (selectedCategorieNouvelleLigne == "fsn_categorie")) {
			var autoCompleteFsnCategorie = [];
			firstNouvelleLigneComparaison.html('contient');
			var valorisationRequeteur = firstNouvelleLigne.children('td[name="valorisation_Requeteur"]');
			var codeHTMLvaleur = '';
			var inputTypeRequeteur = firstNouvelleLigne.find('input[name="type_Requeteur"]');
			if (selectedElementValidant == "categorie_key") { // nouvelle ligne categorie_type
				codeHTMLvaleur = 
					'<input name="texte_Requeteur" class="text" type="text" value="' + rowLigneValidee.find('input[name="texte_Requeteur"]').val() + '">';
				valorisationRequeteur.html(codeHTMLvaleur);
				inputTypeRequeteur.attr('value','String');
				rowLigneValidee.find('[name="supprimer_Requeteur"]').trigger('onclick'); // supprimer ligne categorie_key
				validerRowRequeteur(firstNouvelleLigne.find('span[name="valider_Requeteur"]'),recuperation);
				firstNouvelleLigne = ajouterRowRequeteur(firstNouvelleLigne.find('span[name="ajouter_Requeteur"]'),"Validante",recuperation);
			}
			else if ((selectedElementValidant == "categorie_type")
					&& (rowLigneValidee.find('input[name="texte_Requeteur"]').val() != "categorie_type")) {
					autoCompleteFsnCategorie = alimAutoCompletionFsnCategorie(moduleFonctionnel,rowLigneValidee.find('input[name="texte_Requeteur"]').val(),'categorie_value');
					if (!Array.isArray(autoCompleteArray[aliasTableNouvelleLigne]))
						autoCompleteArray[aliasTableNouvelleLigne] = new Array();
					autoCompleteArray[aliasTableNouvelleLigne]['categorie_value'] = autoCompleteFsnCategorie.slice();
					codeHTMLvaleur = 
						'<input name="texte_Requeteur" class="text" type="text" value="' + '">';
					valorisationRequeteur.html(codeHTMLvaleur);
					firstNouvelleLigne.find('input[name="texte_Requeteur"]').autocomplete({
						source: autoCompleteFsnCategorie
					});
					inputTypeRequeteur.attr('value','String');
			}
			else {
					//autoCompleteFsnCategorie = alimAutoCompletionFsnCategorie(moduleFonctionnel,rowLigneValidee.find('input[name="texte_Requeteur"]').val(),'categorie_value');
					codeHTMLvaleur = 
						'<input name="texte_Requeteur" class="text" type="text" value="' + '">';
					valorisationRequeteur.html(codeHTMLvaleur);
					inputTypeRequeteur.attr('value','String');
					if (Array.isArray(autoCompleteArray[aliasTableNouvelleLigne])) {
						if (autoCompleteArray[aliasTableNouvelleLigne]['categorie_value']) {
							firstNouvelleLigne.find('input[name="texte_Requeteur"]').autocomplete({
								source: autoCompleteArray[aliasTableNouvelleLigne]['categorie_value']
							});
						}
					}
			}
		}
		
		return firstNouvelleLigne;
    }
    
    function changeElementRequeteur(obj,recuperation) {
		var selectedElementRequeteur = $(obj).children('option:selected');
		var inputTypeRequeteur = $(obj).parent().children('input[name*="type_Requeteur"]');
		inputTypeRequeteur.attr('value',selectedElementRequeteur.attr('data-reqtype'));
		
		var rowElementRequeteur = $(obj).parent().parent();
		var selectedCategorieRequeteur = rowElementRequeteur.find('select[name*="categorie_Requeteur"]').children('option:selected').attr('value');
		var comparaisonRequeteur = rowElementRequeteur.children('td[name*="comparaison_Requeteur"]');
		var valorisationRequeteur = rowElementRequeteur.children('td[name*="valorisation_Requeteur"]');
		var codeHTMLvaleur = '';
		
		switch (inputTypeRequeteur.attr('value')) {

		case "3de370a2-999a-3238-b59f-f7628fb18951":
		case 'Date':
			comparaisonRequeteur.html('comprise entre');
			codeHTMLvaleur = 
					'<table>'
				+		'<tr>'
				+			'<td>'
				//+				'<input name="datedebut_Requeteur" class="datepicker" type="text" date-language="fr" placeholder="Début" value="">'
				//+				'<input name="datedebut_Requeteur" class="datepicker" type="text" date-language="us" placeholder="Début" value="">'
				+				'<input name="datedebut_Requeteur" class="datepicker form-control text" readonly="1" type="text" value="'+getNowDate()+'">'
				+			'</td>'
				+			'<td>&nbsp;et&nbsp;</td>'
				+			'<td>'
				//+				'<input name="datefin_Requeteur" class="datepicker" type="text" date-language="fr" placeholder="Fin" value="">'
				//+				'<input name="datefin_Requeteur" class="datepicker" type="text" date-language="us" placeholder="Fin" value="">'
				+				'<input name="datefin_Requeteur" class="datepicker form-control text" readonly="1" type="text" value="'+getNowDate()+'">'
				+			'</td>'
				+		'</tr>'
				+	'</table>';
			valorisationRequeteur.html(codeHTMLvaleur);
			valorisationRequeteur.find('input[class*="datepicker"]').datepicker({
		    	language: 'fr',
		    	format: 'dd/mm/yyyy',
	        	autoclose: true
			});
			break;
		case "38633cee-86c7-65bf-8949-03d68ad5b94d":
		case 'Integer':
		case 'Float':
			comparaisonRequeteur.html('compris entre');
			codeHTMLvaleur = 
					'<table>'
				+		'<tr>'
				+			'<td>'
				+				'<input name="min_Requeteur" class="text" type="text" placeholder="Min" value="">'
				+			'</td>'
				+			'<td>&nbsp;et&nbsp;</td>'
				+			'<td>'
				+				'<input name="max_Requeteur" class="text" type="text" placeholder="Max" value="">'
				+			'</td>'
				+		'</tr>'
				+	'</table>';
			valorisationRequeteur.html(codeHTMLvaleur);
			break;
		case "05c33c09-8556-9519-73c9-372c5fb496a0":
			comparaisonRequeteur.html('');
			var nameBoutonRadio = selectedCategorieRequeteur + selectedElementRequeteur.attr('value') + 'radio' + '_Requeteur';
			codeHTMLvaleur = 
				'<input type="radio" name="' + nameBoutonRadio + '" value="1" data-etat="true">&nbsp;Oui&nbsp;'
			+	'<input type="radio" name="' + nameBoutonRadio + '" value="0" data-etat="false">&nbsp;Non&nbsp;'
			valorisationRequeteur.html(codeHTMLvaleur);
			break;
		case 'Boolean':
			comparaisonRequeteur.html('');
			var nameBoutonRadio = selectedCategorieRequeteur + selectedElementRequeteur.attr('value') + 'radio' + '_Requeteur';
			codeHTMLvaleur = 
				'<input type="radio" name="' + nameBoutonRadio + '" value="true">&nbsp;Oui&nbsp;'
			+	'<input type="radio" name="' + nameBoutonRadio + '" value="false">&nbsp;Non&nbsp;'
			+	'<input type="radio" name="' + nameBoutonRadio + '" value="null">&nbsp;Indéterminé';
			valorisationRequeteur.html(codeHTMLvaleur);
			break;
		case "59089e61-2fe7-f2da-64a1-50173dcea268":
		case 'String':
			comparaisonRequeteur.html('contient');
			codeHTMLvaleur = 
				'<input name="texte_Requeteur" class="text" type="text" value="">';
			valorisationRequeteur.html(codeHTMLvaleur);
			break;
		case 'Foreign':
			comparaisonRequeteur.html('');
			valorisationRequeteur.html('');

			validerRowRequeteur(obj,recuperation);
			var nouvelleLigne = ajouterRowRequeteur(obj,"Validante",recuperation);

			if ((nouvelleLigne.find('select[name="categorie_Requeteur"]').children('option:selected').attr('value') == "fsn_categorie")
					&& !recuperation) {

				// nouvelle ligne sur categorie_object
				codeHTMLvaleur = 
					'<input name="texte_Requeteur" class="text" type="text" value="' + selectedCategorieRequeteur + '">';
				valorisationRequeteur = nouvelleLigne.find('td[name="valorisation_Requeteur"]');
				valorisationRequeteur.html(codeHTMLvaleur);
				inputTypeRequeteur = nouvelleLigne.find('input[name="type_Requeteur"]');
				inputTypeRequeteur.attr('value','String');
				validerRowRequeteur(nouvelleLigne.find('span[name="valider_Requeteur"]'),recuperation);
				nouvelleLigne = ajouterRowRequeteur(nouvelleLigne.find('span[name="ajouter_Requeteur"]'),"Validante",recuperation);
				// nouvelle ligne sur categorie_type
				moduleFonctionnel = selectedCategorieRequeteur; // mémorisation du module fonctionnel pour l'accès à fsn_categorie

				var ligneCategorieType = nouvelleLigne;
				var autoCompleteFsnCategorie = [];
				var aliasTableNouvelleLigne = '';
				
				if (selectedCategorieRequeteur == "fai") {
					codeHTMLvaleur = 
						'<input name="texte_Requeteur" class="text" type="text" value="' + 'categorie_type' + '">';
					valorisationRequeteur = ligneCategorieType.find('td[name="valorisation_Requeteur"]');
					valorisationRequeteur.html(codeHTMLvaleur);
					inputTypeRequeteur = ligneCategorieType.find('input[name="type_Requeteur"]');
					inputTypeRequeteur.attr('value','String');
					validerRowRequeteur(ligneCategorieType.find('span[name="valider_Requeteur"]'),recuperation);
					nouvelleLigne = ajouterRowRequeteur(ligneCategorieType.find('span[name="ajouter_Requeteur"]'),"Validante",recuperation);
					// nouvelle ligne sur categorie_key
					aliasTableNouvelleLigne = nouvelleLigne.children('input[name="aliasTable"]').attr('value');
					
					// autoCompleteFsnCategorie = alimAutoCompletionFsnCategorie(moduleFonctionnel,'categorie_type','categorie_key');						

					// if (!Array.isArray(autoCompleteArray[aliasTableNouvelleLigne]))
					// 	autoCompleteArray[aliasTableNouvelleLigne] = new Array();
					// autoCompleteArray[aliasTableNouvelleLigne]['categorie_key'] = autoCompleteFsnCategorie.slice();
					ligneCategorieType.find('[name="supprimer_Requeteur"]').trigger('onclick'); // supprimer ligne categorie_type
				}
				else {
					var topPosForeign = selectedElementRequeteur.attr('value').indexOf('_'); // ie fonction_id
					var segmentCategorieType = selectedElementRequeteur.attr('value').substring(0,topPosForeign); // ie fonction
					var elementRequeteurParam = moduleFonctionnel + '_' + segmentCategorieType;
					codeHTMLvaleur = 
						'<input name="texte_Requeteur" class="text" type="text" value="' + segmentCategorieType + '">';
					valorisationRequeteur = ligneCategorieType.find('td[name="valorisation_Requeteur"]');
					valorisationRequeteur.html(codeHTMLvaleur);
					inputTypeRequeteur = ligneCategorieType.find('input[name="type_Requeteur"]');
					inputTypeRequeteur.attr('value','String');
					validerRowRequeteur(ligneCategorieType.find('span[name="valider_Requeteur"]'),recuperation);
					nouvelleLigne = ajouterRowRequeteur(ligneCategorieType.find('span[name="ajouter_Requeteur"]'),"Validante",recuperation);
					// nouvelle ligne sur categorie_value
					aliasTableNouvelleLigne = nouvelleLigne.children('input[name="aliasTable"]').attr('value');
					autoCompleteFsnCategorie = alimAutoCompletionFsnCategorie(moduleFonctionnel,elementRequeteurParam,'categorie_value');
					if (!Array.isArray(autoCompleteArray[aliasTableNouvelleLigne]))
						autoCompleteArray[aliasTableNouvelleLigne] = new Array();
					autoCompleteArray[aliasTableNouvelleLigne]['categorie_value'] = autoCompleteFsnCategorie.slice();
				}
				
				comparaisonRequeteur = nouvelleLigne.children('td[name="comparaison_Requeteur"]');
				valorisationRequeteur = nouvelleLigne.find('td[name="valorisation_Requeteur"]');
				comparaisonRequeteur.html('contient');
				
				codeHTMLvaleur = 
					'<input name="texte_Requeteur" class="text" type="text" value="">';
				valorisationRequeteur.html(codeHTMLvaleur);
				nouvelleLigne.find('input[name="texte_Requeteur"]').autocomplete({
					source: autoCompleteFsnCategorie
				});
				
				inputTypeRequeteur = nouvelleLigne.find('input[name="type_Requeteur"]');
				inputTypeRequeteur.attr('value','String');
			}
			break;
		default:
			comparaisonRequeteur.html('contient');
			codeHTMLvaleur = 
				'<input name="texte_Requeteur" class="text" type="text" value="">';
			valorisationRequeteur.html(codeHTMLvaleur);
			break;
		}    
		
    }
    
    // Recuperation date du jour en FR
    function getNowDate(){
    	var d = new Date();
    	dd = d.getDate();
		mm = d.getMonth()+1;  

		if (dd < 10) {   
			dd = "0"+dd;  
		}  
		if (mm < 10) {   
			mm = "0"+mm;  
		}  
		
    	return  dd + '/' + mm + '/' + d.getFullYear();
    }

	function alimAutoCompletionFsnCategorie(moduleFonctionnel,categorieType,categorieColonne) {
		// Bouchonnage
		//var autoCompleteFsnCategorie = ["auto 1", "auto 2", "auto 3"];
		var autoCompleteFsnCategorie = [];
    	// classe utilisée pour transmettre un fichier en ajax
		var form = new FormData();
		form.append("moduleFonctionnel",moduleFonctionnel);
		if (categorieType)
			form.append("categorieType",categorieType);
		if (categorieColonne)
			form.append("categorieColonne",categorieColonne);
		var url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/getFsnCategorie';
		var returnData = {};

		$.ajax({
          async: false, // synchrone
          url: url,
          data: form,
          processData : false,
          type: "POST",
          contentType : false,
          success: function (data) {
        	//alert(data);
        	returnData = JSON.parse(data);
			for (var i in returnData) {
				if (returnData[i]) {
					autoCompleteFsnCategorie.push(returnData[i]);
				}
			}
          },
          error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseJSON) {
              alert(jqXHR.responseJSON);
            }
            else {
              alert(jqXHR.responseText);
            }
            console.log("1:", jqXHR, "2:", textStatus, "3:", errorThrown);
          }
        }); // fin $.ajax({

		return autoCompleteFsnCategorie;
	}
    
    function supprimerRowRequeteur1(obj) {
		if (confirm('Attention, la suppression de la ligne principale entrainera celle des autres.\n\nVeuillez Cliquer sur "OK" pour confirmer la suppression.')) {
			//supprimerRowRequeteur(obj);
			supprimerLignesFilles($('#tr_Requeteur1'));
			$('#element_Requeteur1').prop('disabled', false);
			spanAjouterRequeteur = $('#tr_Requeteur1').find('span[name="valider_Requeteur"]');
			spanAjouterRequeteur.attr('class', "glyphicon glyphicon-ok-sign");
			spanAjouterRequeteur.attr('alt', "Valider");
			spanAjouterRequeteur.attr('title', "Valider");
		}
    }
    
    function supprimerRowRequeteur(obj) {
		var ligneSupprimer = $(obj).parent().parent();
		 $('#table_Requeteur').children('tbody').children('tr').each(function(){
		 	if ($(this).children('input[name="aliasTable"]').attr('value') == ligneSupprimer.children('input[name="aliasTableJoin"]').attr('value')) {
			 	$(this).find('[name="supprimer_Requeteur"]').trigger('onclick');
    		}
		 });
		ligneSupprimer.remove();
    }
    
    function supprimerLignesFilles(ligneMere) {
    	if (ligneMere.find('input[name="type_Requeteur"]').attr('value') == "Foreign") {
    		$('#table_Requeteur').children('tbody').children('tr').each(function(){
    			if ($(this).children('input[name="aliasTable"]').attr('value') == ligneMere.children('input[name="aliasTableJoin"]').attr('value')) {
    				$(this).find('[name="supprimer_Requeteur"]').trigger('onclick');
    			}
    		});
    		location.reload();
    	}
		else{
			var id_requeteur = $('#idRequete').val();
			$.ajax({
			  url: 'http://'+window.location.hostname+baseUrl+'/Requeteur/deleteRequete',
			  async: true,
			  type: "POST",
			  data: {"id": id_requeteur},
			  success: function (data) {
			  	if(data){
			  		alert(data);	
			  	}
			  	location.reload();
			  },
			  error: function (data) {				
				  alert(data);	
			  }
			});
		}
    }
    
    function annulerformRequeteur(url) {
    	if ((topRequeteEditee == false) && (topRequeteModifiee == false))
    		return;
		if (confirm('Veuillez Cliquer sur "OK" pour confirmer l\'annulation de votre saisie.')) {
			if (url)
				document.location.href = url;
			else {
				$('#form_selection').trigger('reset');
				$('#categorie_Requeteur1').prop('disabled', false);
				$('#element_Requeteur1').prop('disabled', false);
				$('#table_Requeteur').children('tbody').children('tr').each(function(index){
					if(index > 0)
						$(this).find('[name*="supprimer_Requeteur"]').trigger('onclick');
				});		
				$('#spanLegende_Requeteur').text('');
				$('#requeteurArea').text('');
				$('#techniqueArea').text('');
				// $('#selectRequeteurArea').text('');
				// $('#fromRequeteurArea').text('');
				// $('#whereRequeteurArea').text('');
				// $('#selectTechniqueArea').text('');
				// $('#fromTechniqueArea').text('');
				// $('#innerTechniqueArea').text('');
				// $('#whereTechniqueArea').text('');
				//$('#requeteArea').toggle(false);
				$('#requeteArea').hide();
			}
		}
		else
			return false;
    }
    
    function retourformRequeteur(url) {
		if (confirm('Veuillez Cliquer sur "OK" pour confirmer l\'abandon de votre saisie.')) {
			document.location.href = url;
		}
    }
    
    function enregistrerRequete() {
    	if (!$('#titreRequete').val()) {
    		alert('Veuillez saisir un titre pour votre requête !');
    		return;
    	}
		
		// Recuperation de la requete
		scanRequete();
		
		if (topRequeteEditee == true){
			if(confirm('Voulez vous enregistrer cette requête éditée ?'))
				enregistrerRequeteAjax();
		}
    	else if (topRequeteModifiee == true) {
			if(confirm('Voulez vous enregistrer cette requête modifiée ?'))
				enregistrerRequeteAjax();
        }
    	else{
			enregistrerRequeteAjax();
		}		
	
		// Validation de la requete enregistree si c'est pas fait
		if(requeteIsValid == false){
			var trRequeteur = $('#tr_Requeteur1');
			validerRowRequeteur(trRequeteur.find('span[name="valider_Requeteur"]'),'recuperation');
		}
		
		topRequeteModifiee = false;
    }
    
	function enregistrerRequeteAjax() {
		// classe utilisée pour transmettre un fichier en ajax
		var form = new FormData();
		form.append("titre",$('#titreRequete').val());
		form.append("description",$('#descriptionRequete').val());
		form.append("modifiee",topRequeteEditee);
		form.append("critere",JSON.stringify(criteresArrayO));
		form.append("requete",JSON.stringify(requeteObjO));

		var url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/enregistrerRequete';
        $.ajax({
          async: false, // synchrone
          url: url,
          data: form,
          processData : false,
          type: "POST",
          contentType : false,
          success: function (data) {
        	alert(data);
          },
          error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseJSON) {
              alert(jqXHR.responseJSON);
            }
            else {
              alert(jqXHR.responseText);
            }
            console.log("1:", jqXHR, "2:", textStatus, "3:", errorThrown);
          }
        }); // fin $.ajax({
	}
    
    function lancerRequete() {
		if(requeteIsValid == false){
			alert("Veuillez valider votre requête en cliquant sur l'icone dans 'Action'");
			return false;
		}
		if (topRequeteModifiee == true) {
    		scanRequete();
    		if (!confirm('Veuillez Cliquer sur "OK" pour lancer la requête sans l\'enregistrer.')) {
    			return false;
    		}
    	}
		$('#listerRequetes').attr('value','');
		$('#form_selection').submit();
    }

    function recupererRequete() {
    	if (topRequeteModifiee == true) {
    		if (!confirm('Veuillez Cliquer sur "OK" pour quitter cette requête sans l\'enregistrer.')) {
    			return false;
    		}
    	}
		$('#listerRequetes').attr('value','lister');
   		$('#form_selection').submit();
    }

    function alimRequete(id,user,modifiee) {
        $("#chargement").modal({
            backdrop: 'static'
        });
    	$('#chargement').modal('show');

    	setTimeout( // On retarde l'execution de ce code pour afficher le modal box
    		function() {
		    	if(annulerformRequeteur() != false){
					topRequeteModifiee = false;
					$('#comparaison_Requeteur1').html('<img src="http://'+window.location.hostname+baseUrl+'/images/loading.gif">');
					
					// classe utilisée pour transmettre un fichier en ajax
					var form = new FormData();
					form.append("id",id);
					var url = '';

					url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/getRequete?colonne=titre';
					var titre = recupererRequeteAjax(url,form);
					
					url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/getRequete?colonne=description';
					var description = recupererRequeteAjax(url,form);
					
					url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/getRequete?colonne=critere';
					var critere = recupererRequeteAjax(url,form);

					url = 'http://'+window.location.hostname+baseUrl+'/Requeteur/getRequete?colonne=requete';
					var requete = recupererRequeteAjax(url,form);
					
					$('#titreRequete').val(titre);
					$('#descriptionRequete').val(description);
					$('#idRequete').val(id);
					
					if(requete != ""){
						var requeteObjI = JSON.parse(requete);
						$('#select_Requeteur').val(requeteObjI.requeteNat.selectRequeteur);
						$('#from_Requeteur').val(requeteObjI.requeteNat.fromRequeteur);
						$('#where_Requeteur').val(requeteObjI.requeteNat.whereRequeteur);
						$('#select_Technique').val(requeteObjI.requeteTec.selectTechnique);
						$('#from_Technique').val(requeteObjI.requeteTec.fromTechnique);
						$('#inner_Technique').val(requeteObjI.requeteTec.innerTechnique);
						$('#where_Technique').val(requeteObjI.requeteTec.whereTechnique);
					}
					//$('#headingFiltre').trigger('click');
					
					var criteresArrayI = JSON.parse(critere); // Tableau des critères
					var trRequeteur = $('#tr_Requeteur1');
					var optionsCategorieRequeteur = '';
					var optionsElementRequeteur = '';
					var categoriePrecedente = '';
					var elementPrecedent = '';
					for (var i in criteresArrayI) {
						if (criteresArrayI[i].categorieRequeteurValue && criteresArrayI[i].categorieTechniqueValue) {
							//alert(criteresArrayI[i].categorieRequeteurValue);
							optionsCategorieRequeteur =
								'<option value="' + criteresArrayI[i].categorieTechniqueValue + '" selected>' + criteresArrayI[i].categorieRequeteurValue + '</option>';
							trRequeteur.find('select[name="categorie_Requeteur"]').html(optionsCategorieRequeteur);
							if (i == 0) {
								$('#categorieprincipale_Requeteur').attr('value',criteresArrayI[i].categorieRequeteurValue);
								$('#categorieprincipale_Technique').attr('value',criteresArrayI[i].categorieTechniqueValue);
								afficherLegendeRequeteur();
							}
							if (!$('#modeComplet').prop('checked')) // hors mode complet
								if (criteresArrayI[i].categorieTechniqueValue == "fsn_categorie") // masquer fsn_categorie
									trRequeteur.find('select[name="categorie_Requeteur"]').hide();
						}
						trRequeteur.find('input[name="type_Requeteur"]').attr('value',criteresArrayI[i].typeRequeteur);
						trRequeteur.children('input[name="aliasTable"]').attr('value',criteresArrayI[i].aliasTable);
						trRequeteur.children('input[name="aliasTableJoin"]').attr('value',criteresArrayI[i].aliasTableJoin);
						trRequeteur.children('input[name="niveauLigneValidee"]').attr('value',criteresArrayI[i].niveau);
						trRequeteur.children('input[name="topLigneValidee"]').attr('value','Validée');
						
						var niveauNouvelleLigne = parseInt(criteresArrayI[i].niveau);
						var indentationNiveau = '';
						for (var j = 0; j < niveauNouvelleLigne; j++)
							indentationNiveau = indentationNiveau + '&nbsp;&rsaquo;&nbsp;';
						trRequeteur.find('span[name="indentationNiveau"]').html(indentationNiveau);
						
						var selectElementRequeteur = trRequeteur.find('select[name="element_Requeteur"]');
						if (criteresArrayI[i].elementRequeteurValue && criteresArrayI[i].elementTechniqueValue) {
							//alert(criteresArrayI[i].elementRequeteurValue);
							optionsElementRequeteur = '<option value="'
								+ criteresArrayI[i].elementTechniqueValue
								+ '"'
								+ ' data-reqtype="'
								+ criteresArrayI[i].typeRequeteur
								+ '" selected>'
								+ criteresArrayI[i].elementRequeteurValue
								+ '</option>';
							selectElementRequeteur.html(optionsElementRequeteur);
						}
						changeElementRequeteur(selectElementRequeteur,'recuperation');
						
						switch (criteresArrayI[i].typeRequeteur) {
							case 'Date':
								trRequeteur.find('input[name="datedebut_Requeteur"]').val(criteresArrayI[i].datedebutRequeteur);
								trRequeteur.find('input[name="datefin_Requeteur"]').val(criteresArrayI[i].datefinRequeteur);
								validerRowRequeteur(trRequeteur.find('span[name="valider_Requeteur"]'),"recuperation");
								ajouterRowRequeteur(trRequeteur.find('span[name="ajouter_Requeteur"]'),"Validante","recuperation");
								break;
							case 'Integer':
							case 'Float':
								trRequeteur.find('input[name="min_Requeteur"]').val(criteresArrayI[i].minRequeteur);
								trRequeteur.find('input[name="max_Requeteur"]').val(criteresArrayI[i].maxRequeteur);
								validerRowRequeteur(trRequeteur.find('span[name="valider_Requeteur"]'),"recuperation");
								ajouterRowRequeteur(trRequeteur.find('span[name="ajouter_Requeteur"]'),"Validante","recuperation");
								break;
							case 'Boolean':
								var inputRadioValue = 'input:radio[value="' + criteresArrayI[i].radioCheckedTechnique + '"]';
								trRequeteur.find(inputRadioValue).prop("checked",true);
								validerRowRequeteur(trRequeteur.find('span[name="valider_Requeteur"]'),"recuperation");
								ajouterRowRequeteur(trRequeteur.find('span[name="ajouter_Requeteur"]'),"Validante","recuperation");
								break;
							case 'String':
								trRequeteur.find('input[name="texte_Requeteur"]').val(criteresArrayI[i].texteRequeteur);
								validerRowRequeteur(trRequeteur.find('span[name="valider_Requeteur"]'),"recuperation");
								ajouterRowRequeteur(trRequeteur.find('span[name="ajouter_Requeteur"]'),"Validante","recuperation");
								break;
							case 'Foreign':
								trRequeteur.children('input[name="aliasTable"]').attr('value',criteresArrayI[i].aliasTable);
								trRequeteur.children('input[name="aliasTableJoin"]').attr('value',criteresArrayI[i].aliasTableJoin);
								break;
							default:
								break;
						}
						
						if (criteresArrayI[i].typeRequeteur != 'Foreign') {
							if ((criteresArrayI[i].categorieTechniqueValue == categoriePrecedente)
							 && (criteresArrayI[i].elementTechniqueValue == elementPrecedent))
								trRequeteur.children('td[name="comparaison_Requeteur"]').prepend('ou ');
						}
						
						categoriePrecedente = criteresArrayI[i].categorieTechniqueValue;
						elementPrecedent = criteresArrayI[i].elementTechniqueValue;
						trRequeteur = trRequeteur.parent().children('tr:last');
					}
					
					// Editer la requete courante
					editerRequete();
					
					// Suuprimer la dernière ligne ajoutée superflue
					trRequeteur.find('[name*="supprimer_Requeteur"]').trigger('onclick');
					topRequeteModifiee = false;
				}
				$('#chargement').modal('hide');
			}, 100
		);
    }
    
	function recupererRequeteAjax(url,form) {
		var returnData = '';
		$.ajax({
          async: false, // synchrone
          url: url,
          data: form,
          processData : false,
          type: "POST",
          contentType : false,
          success: function (data) {
        	returnData = data;
          },
          error: function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.responseJSON) {
              alert(jqXHR.responseJSON);
            }
            else {
              alert(jqXHR.responseText);
            }
            console.log("1:", jqXHR, "2:", textStatus, "3:", errorThrown);
          }
        }); // fin $.ajax({
		return returnData;
	}
    
    function editerRequete() { 
    	
    	if($('#categorieprincipale_Requeteur').val() == "" && $('#categorieprincipale_Technique').val() == ""){
    		$('#requeteurArea').text("Aucune requête à afficher.");
    		$('#techniqueArea').remove();
    	}
    	else{
    		// if (topRequeteModifiee == true)
    		// $('#requeteurArea').empty();

    		scanRequete();

    		if($('#techniqueArea').length == 0){
    			$('#requeteArea').append('<textarea id="techniqueArea" class="form-control textarea" name="techniqueArea" placeholder="" type="textarea" rows="4" disabled></textarea>');
    		}

    		$('#table_Requeteur').children('tbody').children('tr').each(function(){
				var valorisationRequeteurTD = $(this).children('td[name*="valorisation_Requeteur"]');

				// Conditions pour afficher le texte generique par defaut
				if((valorisationRequeteurTD.find('input').length == 0 || topRequeteModifiee == false) 
					&& (!valorisationRequeteurTD.find('input[type=radio]').is(':checked')
					|| valorisationRequeteurTD.find('input[name*="texte_Requeteur"]').val() == "") && texteRequeteurGlobal == ""){
					var entiteRequeteur = $(this).find('select[name*="categorie_Requeteur"]').children('option:selected').val();
					$('#requeteurArea').text('Lister tous les éléments "'+traduireRequeteur(entiteRequeteur)+'"');
				}
				else{					
		    		$('#requeteurArea').text($('#select_Requeteur').val() + "\n" + $('#where_Requeteur').val());	
		    		texteRequeteurGlobal += $('#requeteurArea').text(); 
				}					    		

	    		if($('#inner_Technique').val() != "")
		    		$('#techniqueArea').text($('#select_Technique').val() + "\n" + $('#from_Technique').val() + "\n" + $('#inner_Technique').val() + "\n" + $('#where_Technique').val());
		    	else
		    		$('#techniqueArea').text($('#select_Technique').val() + "\n" + $('#from_Technique').val() + "\n" + $('#where_Technique').val());

				topRequeteEditee = true;
		    });	    	
		}		

		$('#requeteArea').show();
		if (!$('#modeComplet').prop('checked'))
			$('#fromRequeteurArea').hide();	
    }

    function convertDateToFrFromUs(ddate){
		var pdebut = ddate.split('/');		
		if(pdebut != "")			
			ddate = pdebut[2] + '-' + pdebut[1] + '-' + pdebut[0];			
		return ddate;
	}
    
    function scanRequete() {
    	var selectRequeteur = "Lister les " + $('#categorieprincipale_Requeteur').attr('value') + " ";
    	var selectTechnique = "SELECT a1.id";
    	var fromRequeteur = "à partir des " + $('#categorieprincipale_Requeteur').attr('value');
    	var fromTechnique = "FROM " + $('#categorieprincipale_Technique').attr('value') + " AS a1";	
    	/*Si jointure*/
		var innerTechnique = "";
		var innerPoterieDefined = '';
		var innerFaiExtDefined = '';
		var innerFaiExtSelectDefined = "";
    	var whereRequeteur = "pour lesquels ";
    	var whereTechnique = "WHERE 1 ";
    	var elementRequeteurPrec = "";
    	var topElementNatPrec = false;
    	var categorieRequeteurPrec = "";
    	var parentheseOuverteTech = false;
    	var parentheseOuverteNat = false;
    	var elementNatSurcharge = "";
    	var categorieRequeteurFirst = "";
    	var categorieRequeteurForeignFirst = "";
    	var categoriePrincipaleValue = "";
    	criteresArrayO.length = 0; // Réinitialisation du tableau
		var completerWhereRequeteur = true;
    	var categorieRequeteurObj = $('#table_Requeteur').find("tr[id='tr_Requeteur1']");

    	// Recuperation de la categorie principale de la requete
		var categorieRequetePrincipale = categorieRequeteurObj.find('select[name="categorie_Requeteur"]:first');
		var categorieTechniquePrincipale = categorieRequetePrincipale.children('option:selected').val();
		categoriePrincipaleValue = traduireRequeteur(categorieTechniquePrincipale);

    	$('#table_Requeteur').children('tbody').children('tr').each(function(){
    		if ($(this).children('input[name*="topLigneValidee"]').attr('value') == "Validée") {
    			var thisCategorieRequeteur = $(this).find('select[name="categorie_Requeteur"]');
    			var categorieTechniqueValue = thisCategorieRequeteur.children('option:selected').attr('value');
    			var categorieRequeteurValue = '';
    			//var categorieTechniqueValue = $(this).find('input[name*="categorie_Technique"]').attr('value');
    			//var categorieTechniqueValue = $(this).find('select[name*="categorie_Requeteur"]').children('option:selected').attr('value');
    			//var categorieRequeteurValue = $(this).find('select[name*="categorie_Requeteur"]').children('option:selected').val();
    			//if (thisCategorieRequeteur == $('#categorie_Requeteur1'))
    				categorieRequeteurValue = traduireRequeteur(categorieTechniqueValue);
    			//else
    			//	categorieRequeteurValue = thisCategorieRequeteur.children('option:selected').text();
    			var typeRequeteur = $(this).find('input[name*="type_Requeteur"]').attr('value');
    			var aliasTable = $(this).children('input[name="aliasTable"]').attr('value');
    			var aliasTableJoin = $(this).children('input[name="aliasTableJoin"]').attr('value');
    			var niveauLigneValidee = $(this).children('input[name="niveauLigneValidee"]').attr('value');
    			/*
    			if ((categorieRequeteurValue != $('#categorieprincipale_Requeteur').attr('value'))
    			 && (typeRequeteur != "Clef"))
    				fromRequeteur = fromRequeteur + " JOIN " + categorieRequeteurValue;
    			*/
    			var thisElementRequeteur = $(this).find('select[name*="element_Requeteur"]');
    			var elementTechniqueValue = thisElementRequeteur.children('option:selected').attr('value');
    			var categorieTechnique = thisElementRequeteur.children('option:selected').attr('data-reqcategorie');
 
    			//var elementRequeteurValue = $(this).find('select[name*="element_Requeteur"]').children('option:selected').val();
    			var elementRequeteurValue = thisElementRequeteur.children('option:selected').text();
    			
    			var critereObjPush = Object.create(critereObjO);
    			critereObjPush.categorieRequeteurValue = categorieRequeteurValue;
    			critereObjPush.categorieTechniqueValue = categorieTechniqueValue;
    			critereObjPush.typeRequeteur = typeRequeteur;
    			critereObjPush.aliasTable = aliasTable;
    			critereObjPush.aliasTableJoin = aliasTableJoin;
    			critereObjPush.niveau = niveauLigneValidee;
    			critereObjPush.elementRequeteurValue = elementRequeteurValue;
    			critereObjPush.elementTechniqueValue = elementTechniqueValue;
    			
				var valorisationRequeteurTD = $(this).children('td[name*="valorisation_Requeteur"]');

				completerWhereRequeteur = true;
				if (!$('#modeComplet').prop('checked')) { // hors mode complet d'affichage, masquer les infos de travail
					if (categorieTechniqueValue == "fsn_categorie") {
						if ((elementTechniqueValue == "categorie_object")
						 || (elementTechniqueValue == "categorie_type")) 
							completerWhereRequeteur = false;
					}
				}
				
   			   	texteRequeteur = valorisationRequeteurTD.find('input[name*="texte_Requeteur"]').val();	

				/*Requete element poterie*/
				if (categorieTechniqueValue == "elementrecueilli") {
					if ((elementTechniqueValue == "caracterepate_id (fsn_categorie)")
					 || (elementTechniqueValue == "forme_id (fsn_categorie)")
					 || (elementTechniqueValue == "classification_id (fsn_categorie)")
					 || (elementTechniqueValue == "production_id (fsn_categorie)")
					 || (elementTechniqueValue == "decor")
 					 || (elementTechniqueValue == "traceusage") 
					 || (elementTechniqueValue == "proportionconserve") 
					 || (elementTechniqueValue == "precisionproduction_id (fsn_categorie)")){
						aliasTable = "p1";
						critereObjPush.aliasTable = aliasTable;
						objCategorieEntete += elementTechniqueValue;
						
						if(innerPoterieDefined == "")
							innerTechnique = "INNER JOIN elementpoterie AS p1 ON a1.elementpoterie_id = p1.id " + innerTechnique;	
						
						innerPoterieDefined += "yes";					
					} 					
					else{
						// aliasTable = "a1";
						critereObjPush.aliasTable = aliasTable;
						objCategorieEntete = ""; // Vide l'obj categorie correspondant aux types poteries
					}
				}			

				/*poterie avec valeur fsn_categorie*/
				if (categorieTechniqueValue == "fsn_categorie") {
					if (elementTechniqueValue == "categorie_object"){
						if(texteRequeteur == "elementrecueilli" && objCategorieEntete != ""){
							texteRequeteur = "elementpoterie";
						}
						objCategorieEntete = texteRequeteur;
					}
				}

				/*Fai extensions*/
				if(categorieTechniqueValue == "fai" && categorieTechnique == "fai_extension"){

					var jetonRequeteur = parseInt($('#jeton_Requeteur').attr('value')) + 1;
					aliasTableJoin = 'a' + jetonRequeteur;
					$('#jeton_Requeteur').attr('value',jetonRequeteur);

					lastaliasTable = aliasTable;
					aliasTable = "categ" + jetonRequeteur;		
    					
					innerTechnique += 'INNER JOIN fai_extension_value AS ' + aliasTableJoin + ' ON ' + aliasTableJoin + '.fai_id = ' + lastaliasTable +'.id ';
    				innerTechnique += 'INNER JOIN fsn_categorie '+aliasTable+' on '+aliasTable+'.id = '+aliasTableJoin+'.extension_id ';

				}

				if (typeRequeteur != "Foreign") {
					var whereOu = false;
    				if ((elementRequeteurValue == elementRequeteurPrec) && (categorieRequeteurFirst == categorieRequeteurPrec)) {
    					if (completerWhereRequeteur) {
    						whereRequeteur += " ou ";
    						whereOu = true;
    					}
    					whereTechnique += " OR ";
    				}
    				else {
    					if ((elementRequeteurPrec == "") || (parentheseOuverteTech == false)) {
    						if (completerWhereRequeteur) {
    							whereRequeteur += " (";
    							parentheseOuverteNat = true;
    						}
    						whereTechnique += " AND (";
    					}
    					else {
    						if (completerWhereRequeteur) {
    							if (parentheseOuverteNat)
        							whereRequeteur += ")";
    							if (topElementNatPrec)
    								whereRequeteur += " et";
    							whereRequeteur += " (";
    							parentheseOuverteNat = true;
    						}
    						whereTechnique += ") AND (";
    					}
						parentheseOuverteTech = true;
    				}

    				if (categorieTechniqueValue != "fsn_categorie"){
    						elementNatSurcharge = "";

    						// Recuperation des entites principales de la requete
			   				var thisCategorieRequeteurFirst = $(this).find('select[name="categorie_Requeteur"]:visible');
			    			var categorieTechniqueValueFirst = thisCategorieRequeteurFirst.children('option:selected').attr('value');
							
							if(categorieTechniqueValueFirst != 'undefined')
								categorieRequeteurFirst = traduireRequeteur(categorieTechniqueValueFirst);
			    	}

    				if (completerWhereRequeteur && !whereOu && !elementNatSurcharge)
    					whereRequeteur = whereRequeteur + elementRequeteurValue + " de " + categorieRequeteurFirst;
   			
   					if (completerWhereRequeteur && !whereOu && elementNatSurcharge)
   						if ((categorieTechniqueValue == "fsn_categorie") && (elementTechniqueValue == "categorie_value"))
   							whereRequeteur = whereRequeteur + elementNatSurcharge + " de " + categorieRequeteurForeignFirst; 					
   			
   					if (aliasTable != "a1")
   						if (completerWhereRequeteur && !whereOu && (categoriePrincipaleValue != categorieRequeteurForeignFirst))
   							whereRequeteur += " associé";

   					if(categorieTechnique != "fai_extension"){

						var thisElementRequeteur = categorieRequeteurObj.find('select[name*="element_Requeteur"]');
						var elementTechniqueValueFirst = thisElementRequeteur.children('option:selected').attr('value');
								
						// Cas champs=Typefai_id (on met categorie_type= fai_terme dans tout les cas) 
						if(categorieTechniqueValue == "fsn_categorie" && elementTechniqueValue == "categorie_value" && elementTechniqueValueFirst == "typefai_id (fsn_categorie)")
							whereTechnique = whereTechnique + aliasTable + ".categorie_type LIKE 'fai_terme') AND (";

   						whereTechnique = whereTechnique + aliasTable + "." + elementTechniqueValue;
   					}

        			if (completerWhereRequeteur)
        				topElementNatPrec = true;

        			/*Fai extensions*/
        			if(categorieTechniqueValue == "fai" && categorieTechnique == "fai_extension"){
						whereTechnique = whereTechnique + aliasTable+'.categorie_object like "' + categorieTechniqueValue + '" AND '+aliasTable+'.categorie_value like "' + elementTechniqueValue + '" AND ' ;
					}
    			}

    			switch (typeRequeteur) {

    			case "3de370a2-999a-3238-b59f-f7628fb18951":
    					whereTechnique += aliasTable+'.categorie_type like "' + categorieTechnique + '" AND '+ aliasTableJoin +'.extension_value ';
    			case 'Date':
    				datedebutRequeteur = convertDateToFrFromUs(valorisationRequeteurTD.find('input[name*="datedebut_Requeteur"]').val());
    				if (!datedebutRequeteur)
    					 datedebutRequeteur = "0001-01-01";
    				datefinRequeteur = convertDateToFrFromUs(valorisationRequeteurTD.find('input[name*="datefin_Requeteur"]').val());
    				if (!datefinRequeteur)
    					 datefinRequeteur = "9999-12-31";
   					whereRequeteur = whereRequeteur + " entre " + "'" + datedebutRequeteur + "'" + " et " + "'" + datefinRequeteur + "'";
    				whereTechnique = whereTechnique + " BETWEEN " + "'" + datedebutRequeteur + "'" + " AND " + "'" + datefinRequeteur + "'";

    				critereObjPush.datedebutRequeteur = datedebutRequeteur;
    				critereObjPush.datefinRequeteur = datefinRequeteur;
    				break;
    			case "38633cee-86c7-65bf-8949-03d68ad5b94d":
    				whereTechnique += aliasTable+'.categorie_type like "' + categorieTechnique + '" AND '+ aliasTableJoin +'.extension_value ';
    			case 'Integer':
    			case 'Float':
    				minRequeteur = parseFloat(valorisationRequeteurTD.find('input[name*="min_Requeteur"]').val());    								    
    				if (!minRequeteur)
    					 minRequeteur = 0;

    				maxRequeteur = parseFloat(valorisationRequeteurTD.find('input[name*="max_Requeteur"]').val());
    				if (!maxRequeteur)
  					     maxRequeteur = 99999999999;
    				
   					whereRequeteur = whereRequeteur + " entre " + minRequeteur + " et " + maxRequeteur;
    				whereTechnique = whereTechnique + " BETWEEN " + minRequeteur + " AND " + maxRequeteur;

    				critereObjPush.minRequeteur = minRequeteur;
    				critereObjPush.maxRequeteur = maxRequeteur;
    				break;
    			case "05c33c09-8556-9519-73c9-372c5fb496a0":
    				whereTechnique += aliasTable+'.categorie_type like "' + categorieTechnique + '" AND '+ aliasTableJoin +'.extension_value ';

    				radioCheckedRequeteur = valorisationRequeteurTD.find(':radio:checked').attr('data-etat');
    				radioCheckedTechnique = valorisationRequeteurTD.find(':radio:checked').attr('value');
    				whereRequeteur = whereRequeteur + " est " + radioCheckedRequeteur;
    				whereTechnique = whereTechnique + " = " + radioCheckedTechnique;
    				
    				critereObjPush.radioCheckedRequeteur = radioCheckedRequeteur;
    				critereObjPush.radioCheckedTechnique = radioCheckedTechnique;
    				break;
    			case 'Boolean':
    				//var nameBoutonRadio = categorieRequeteurValue + elementRequeteurValue + 'radio' + '_Requeteur';
    				//radioCheckedRequeteur = valorisationRequeteurTD.find(':radio:checked').val();
    				radioCheckedRequeteur = valorisationRequeteurTD.find(':radio:checked').val();
    				radioCheckedTechnique = valorisationRequeteurTD.find(':radio:checked').attr('value');
    				whereRequeteur = whereRequeteur + " est " + radioCheckedRequeteur;
    				whereTechnique = whereTechnique + " = " + radioCheckedTechnique;
					//	+ " = " + $(this).find('input[name*=nameBoutonRadio]:checked').attr('value');
    				
    				critereObjPush.radioCheckedRequeteur = radioCheckedRequeteur;
    				critereObjPush.radioCheckedTechnique = radioCheckedTechnique;
    				break;
    			case "59089e61-2fe7-f2da-64a1-50173dcea268":
    				whereTechnique += aliasTable+'.categorie_type like "' + categorieTechnique +'" AND '+  aliasTableJoin +'.extension_value ';
    			case 'String':
    				if (completerWhereRequeteur)
    					whereRequeteur = whereRequeteur + " contient '" + texteRequeteur + "'";	    		
    			
					if (categorieTechniqueValue == "fsn_categorie" && elementTechniqueValue == "categorie_type" && innerPoterieDefined != ""){			
						whereTechnique = whereTechnique + " LIKE '" + objCategorieEntete + "_" + texteRequeteur + "'"; // Selectionner uniquement des valeurs specifiques dans fsn_categorie
						objCategorieEntete = "";
					}
    				else
    					whereTechnique = whereTechnique + " LIKE '%" + texteRequeteur + "%'";
    				
    				critereObjPush.texteRequeteur = texteRequeteur;
    				break;
    			case 'Foreign':
    				//var posClef = elementTechniqueValue.indexOf(':');
    				var topPosForeign = elementTechniqueValue.indexOf(' (');

					var thisCategorieRequeteurForeignFirst = $(this).find('select[name="categorie_Requeteur"]:visible');
	    			var categorieTechniqueValueForeignFirst = thisCategorieRequeteurForeignFirst.children('option:selected').attr('value');
				
					if(categorieTechniqueValueForeignFirst != 'undefined')
						categorieRequeteurForeignFirst = traduireRequeteur(categorieTechniqueValueForeignFirst);

    				//if (posClef == -1) {
       				if (topPosForeign == -1) {
    					var joinCategorieTechnique = categorieTechniqueValue;
    					var joinElementTechnique = "";
    					var joinCategorieRequeteur = categorieRequeteurValue;
    					var joinElementRequeteur = "";
    				}
    				else {
    					var posDebForeign = topPosForeign + 2;
    					var posFinForeign = elementTechniqueValue.indexOf(')') ;
    					//var joinCategorieTechnique = elementTechniqueValue.substring(0,posClef); // ie : us (pour us:us_id)
    					var joinCategorieTechnique = elementTechniqueValue.substring(posDebForeign,posFinForeign); // ie : us (pour us_id (us))
    					//var joinElementTechnique = elementTechniqueValue.substring(posClef+1); // ie : us_id
    					var joinElementTechnique = elementTechniqueValue.substring(0,topPosForeign); // ie : us_id
    					/*
    					var posUnderscore = joinElementTechnique.indexOf('_'); // recherche de la position éventuelle d'un underscore
    					var joinElementTechniqueOn = joinElementTechnique; // ie : us_id
    					if (posUnderscore != -1) { // présence d'un underscore qui peut réveler un nom de table
    						if (joinElementTechnique.substring(0,posUnderscore) == joinCategorieTechnique) // ie : us
    							joinElementTechniqueOn = joinElementTechnique.substring(posUnderscore+1); // ie : id
    					}
    					*/
    					var joinElementTechniqueOn = "id"; // on force la clé de jointure à "id"
    					//var aliasTableJoin = $(this).children('input[name="aliasTableJoin"]').attr('value');
    					//var joinCategorieRequeteur = elementRequeteurValue.substring(0,posClef);
    					//var joinElementRequeteur = elementRequeteurValue.substring(posClef+1);
    					var joinCategorieRequeteur = traduireRequeteur(joinCategorieTechnique);
    					var joinElementRequeteur = traduireRequeteur(joinElementTechnique);
    					if (!$('#modeComplet').prop('checked') && (joinCategorieTechnique == "fsn_categorie"))
    						elementNatSurcharge = joinElementRequeteur;
    		    	}
    				// fromTechnique = fromTechnique
    				// + " JOIN " + joinCategorieTechnique + " AS " + aliasTableJoin + " ON " + aliasTableJoin + "." + joinElementTechniqueOn + " = " + aliasTable + "." + joinElementTechnique;
					
					innerTechnique += "JOIN " + joinCategorieTechnique + " AS " + aliasTableJoin + " ON " + aliasTableJoin + "." + joinElementTechniqueOn + " = " + aliasTable + "." + joinElementTechnique + " ";
    				//+ " JOIN " + joinCategorieTechnique + " ON " + joinCategorieTechnique + "." + joinElementTechniqueOn + " = " + categorieTechniqueValue + "." + joinElementTechnique;
    				fromRequeteur = fromRequeteur
    				+ " et " + joinCategorieRequeteur + " avec " + joinElementRequeteur + " de " + joinCategorieRequeteur + " correspondant à " + joinElementRequeteur + " de " + categorieRequeteurValue;
    				break;
    			default:
    				if(aliasTable != "a1"){    				
    					jetonRequeteur += 1;
    					aliasTable = "categ" + jetonRequeteur;
    					$('#jeton_Requeteur').attr('value',jetonRequeteur);
    					
    					categorieTechnique = "fai_extension_liste_valeur";

    					if (completerWhereRequeteur)
    						whereRequeteur = whereRequeteur + " contient '" + texteRequeteur + "'";	    		
    					
    					innerTechnique += 'INNER JOIN fsn_categorie '+aliasTable+' on '+aliasTable+'.id = '+aliasTableJoin+'.extension_value ';
    					whereTechnique += aliasTable+'.categorie_type like "' + categorieTechnique +'" AND ' + aliasTable+'.categorie_value like "%'+texteRequeteur+'%" ';
    				}
    				break;
    			}
    			elementRequeteurPrec = elementRequeteurValue;
    			categorieRequeteurPrec = categorieRequeteurFirst; //categorieRequeteurValue
    			criteresArrayO.push(critereObjPush); // Ajout du critère au tableau
    		}
    	});
    	if ((elementRequeteurPrec != "") && parentheseOuverteTech) {
    		if (completerWhereRequeteur && parentheseOuverteNat) {
    			whereRequeteur += ")";
    		}
    		whereTechnique += ")";
    	}

    	// gestion fermeture parenthese apres suppression des lignes filles requete
    	for (var i=0; i<whereRequeteur.length; i++) {
    		if(whereRequeteur.charAt(i) == "("){
		    	if(whereRequeteur.substr(-1) != ")")
		    		whereRequeteur += ")";
    		}
	    }

    	$('#select_Requeteur').attr('value',selectRequeteur);
    	$('#from_Requeteur').attr('value',fromRequeteur);
    	$('#where_Requeteur').attr('value',whereRequeteur);
    	$('#select_Technique').attr('value',selectTechnique);
    	$('#from_Technique').attr('value',fromTechnique);
    	$('#inner_Technique').attr('value',innerTechnique);
    	$('#where_Technique').attr('value',whereTechnique);
    	
		requeteObjO.requeteNat.selectRequeteur = selectRequeteur;
		requeteObjO.requeteNat.fromRequeteur = fromRequeteur;
		requeteObjO.requeteNat.whereRequeteur = whereRequeteur;
		requeteObjO.requeteTec.selectTechnique = selectTechnique;
		requeteObjO.requeteTec.fromTechnique = fromTechnique;
		requeteObjO.requeteTec.innerTechnique = innerTechnique;
		requeteObjO.requeteTec.whereTechnique = whereTechnique;
		
}