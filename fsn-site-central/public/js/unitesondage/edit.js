/***********************************************
 *  Author: Joseph Kalule                      *
 *  Date: 04/01/2017                           *
 *  Used for: /unitesondage/edit/              *
 ***********************************************/

$(function() {
	getLogsList();
	getTronconsList();
	transformInputTypeForDimensions();
	adjustInputClasses();
});

function addListenersToLogForm() {
	$("#ajouterLogAUS").click(function() {
		getNewLogForm();
	});
}

function addListenersToTronconForm() {
	$("#ajouterTronconAUS").click(function() {
		getNewTronconForm();
	});
}

function addListenersToNewLogForm() {
	$("#annulerAjoutLog").click(function() {
		$("#Logs").html(logsHTML);
		$("#myModal").modal('hide');
		addListenersToLogForm();
	});
	$("#confirmerAjoutLog").click(function(e) {
		//alert("form data = "+$("form[name='form_log']").serialize()+" baseURL = "+$("#baseURLfield").val());
		$.ajax({
			type: "POST",
			url: $("#baseURLfield").val()+"/Log/add",
			data: $("form[name='form_log']").serialize()
		})
		.done(function(d) {
			//alert("success!: "+d);
			$("#myModal").modal('hide');
			getLogsList();
		})
		.error(function() { /*alert("error!");*/ });
		e.preventDefault();
	});
}

function addListenersToNewTronconForm() {
	$("#annulerAjoutTroncon").click(function() {
		$("#Troncons").html(tronconsHTML);
		$("#myModal").modal('hide');
		addListenersToTronconForm();
	});
	$("#confirmerAjoutTroncon").click(function(e) {
		//alert("form data = "+$("form[name='form_log']").serialize()+" baseURL = "+$("#baseURLfield").val());
		var datas = $("form[name='form_troncon']").serialize();
		$.ajax({
			type: "POST",
			url: $("#baseURLfield").val()+"/Troncon/add",
			data: datas
		})
		.done(function(d) {
			var errors = $(d).find("#errors_messages").val();
			if(errors == ""){
				$("#myModal").modal('hide');
				getTronconsList();
			}
			else{
				alert(errors);			
			}
		})
		.error(function() { /*alert("error!");*/ });
		e.preventDefault();
	});
}

function getLogsList() {
	$.ajax({
		url: $("#baseURLfield").val()+"/Log/index",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#Logs").html(response);
        document.getElementById("us_id_logs").disabled = true;
        $("#Logs .btn").remove()
        $("#Logs .table-clickable-row").each(function() {
            var redirect = $(this).attr('href')
            $(".table-row-data", $(this)).click(function() {
                window.location.href = redirect;
            })
        })
        addListenersToLogForm();
	});
}

function getTronconsList() {
	$.ajax({
		url: $("#baseURLfield").val()+"/Troncon/index",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#Troncons").html(response);
        document.getElementById("us_id_troncon").disabled = true;
        $("#Troncons .btn").remove()
        $("#Troncons .table-clickable-row").each(function() {
            var redirect = $(this).attr('href')
            $(".table-row-data", $(this)).click(function() {
                window.location.href = redirect;
            })
        })
		addListenersToTronconForm();
	});
}

var logsHTML;

function getNewLogForm() {
	logsHTML = $("#Logs").html();
	$.ajax({
		url: $("#baseURLfield").val()+"/Log/add",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#modalBody").html(response);
		$("#modalTitle").text("Ajout Log");
		$("#myModal").modal();
		addListenersToNewLogForm();
	});
}

var tronconsHTML;

function getNewTronconForm() {
	tronconsHTML = $("#Troncons").html();
	$.ajax({
		url: $("#baseURLfield").val()+"/Troncon/add",
		type: "POST",
		data: "unitesondage_id="+$("#id_usondage").val()
	}).done(function(response) {
		$("#modalBody").html(response);
		$("#modalTitle").text("Ajout Tronçon");
		$("#myModal").modal();

		addListenersToNewTronconForm();
	});
}


/*
 * cette methode fait la suppression
 * */
function confirmer(url) {  // jfb 2016-04-13
	//alert('hi');
	if (confirm('Veuillez Cliquer sur "OK" pour confirmer la suppression.')) {
		//document.location.href = url;
		$.ajax({
			type: "POST", url: url
		})
		.done(function() {
			getLogsList();
		});
	}
}

/*
 * cette methode fait la suppression de troncons
 * */
function confirmerT(url) {  // jfb 2016-04-13
	//alert('hi');
	if (confirm('Veuillez Cliquer sur "OK" pour confirmer la suppression.')) {
		// document.location.href = document.location;
		$.ajax({
			type: "POST", url: url
		})
		.done(function() {
			getTronconsList();
		});
	}
}

/* *
 * Cette methode transforme le type des inputs des dimensions en nombre
 * (largeur, longueur, profondeur)
 * */
function transformInputTypeForDimensions() {
	$("input[step]").attr("type", "number");
}


function adjustInputClasses() {
	$(".form-control").parent().attr("class", "col-xs-7");
}