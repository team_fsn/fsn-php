<?php
$root = dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR;

require $root.'library/PHPExcel.php' ;
require $root.'library/Spyc/Spyc.php';
require $root.'library/Yab/Loader.php';

try {

	$loader = Yab_Loader::getInstance();

	$loader->addPath($root.'application');
	
  try {
	  			if (file_exists($root.'config/application.yaml')) {

					$loader->configure($root . 'config/application.yaml');

				}
	  			else{
					header("Location: ./install.php");
					die;
				}
               
	} catch(Yab_Exception $e) {

	  var_dump($e);
	  die;

	}
        
	$loader->startMvc();


} catch(Yab_Exception $e) {

	echo $e;

}
