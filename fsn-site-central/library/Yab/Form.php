<?php
/**
 * Yab Framework
 *
 * @category   Yab
 * @package    Yab_Form
 * @author     Yann BELLUZZI
 * @copyright  (c) 2010 YBellu
 * @license    http://www.ybellu.com/yab-framework/license.html
 * @link       http://www.ybellu.com/yab-framework 
 */

class Yab_Form extends Yab_Object {
	
	private $_csrf = null;

	private $_elements = array();

	final public function getElements() {

		return $this->_elements;

	}

	final public function hasElement($name) {

		return array_key_exists($name, $this->_elements);

	}

	final public function remElement($name) {

		if(!$this->hasElement($name))
			throw new Yab_Exception('"'.$name.'" is not defined is this form');

		unset($this->_elements[$name]);

		return $this;

	}

	final public function setElement($name, array $attributes = array()) {

		$element = new Yab_Form_Element($this, array_merge($attributes, array('name' => $name)));

		if($this->hasElement($name))
			throw new Yab_Exception('"'.$name.'" has already been defined is this form');

		$this->_elements[$name] = $element;

		return $this;

	}

	final public function getElement($name) {

		if(!$this->hasElement($name))
			throw new Yab_Exception('"'.$name.'" is not defined is this form');

		return $this->_elements[$name];

	}

	final public function getErrors($filters = null) {

		$errors = array();

		if($this->_csrf && !$this->_csrf->isSubmitted()) 
			return $errors;
					
		if($this->_csrf && !$this->_csrf->isValid()) 
			return $errors;

		if($this->isSubmitted()) {
		
			foreach($this->_elements as $element) {
				
				foreach($element->getErrors($filters) as $error_code => $message) {
				
					if(!array_key_exists($element->get('name'), $errors)) 
						$errors[$element->get('name')] = array();
						
					$errors[$element->get('name')][$error_code] = $message;
					
				}
			
			}
			
		}

		return $errors;

	}

	final public function csrf($token = null) {
	
		if($token === null)
			$token = get_class($this);
			
		$token = strtolower($token);
	
		$request = Yab_Loader::getInstance()->getRequest();	
		$session = Yab_Loader::getInstance()->getSession();	
		
		$csrf_name = 'yab_'.$token.'_csrf_token';
		$csrf_value = md5($csrf_name.$request->getServer()->get('REMOTE_ADDR').$request->getServer()->get('HTTP_USER_AGENT'));

		if(!$session->has($csrf_name))
			$session->set($csrf_name, $csrf_value);

		$this->_csrf = new Yab_Form_Element($this, array(
			'name' => $csrf_name,
			'type' => 'hidden',
			'id' => $csrf_name,
			'value' => $csrf_value,
			'validators' => array(
				'Equal' => array(
					'to' => $session->get($csrf_name)
				),
			),
		));
			
		return $this;
		
	}

	final public function isSubmitted() {
		
		if($this->_csrf && !$this->_csrf->isSubmitted()) 
			return false;

		foreach($this->_elements as $element) { 

			if($element->has('disabled') && strtolower($element->get('disabled')) == 'disabled')
				continue;

			if(!$element->isSubmitted()) 
				return false;

		}

		return true;

	}

	final public function isValid() {
		
		if($this->_csrf && !$this->_csrf->isValid()) 
			return false;

		foreach($this->_elements as $element) {

			if(!$element->isValid()) 
				return false;

		}
		
		return true;

	}

	final public function getValues($filters = true) {

		$values = array();

		foreach($this->_elements as $element) 
			$values[$element->get('name')] = $element->getValue($filters);

		return $values;

	}

	final public function getTypedValues ($values = null) {

		if ($values === null) {
			
			foreach ($this->_elements as $name => $element) {
				
				$values = $this->setTypedValue($element, $values, $name);
				
			}
		} else if (count($values) >= 1 ) {
			
			foreach ($values as $key => $value ) {
				
				if ($this->hasElement($key)) {
					
					$values = $this->setTypedValue($this->getElement($key), $values, $key);
					
				}
				
			}
			
		}
		$values = $this->setNullValues($values);
		return $values;
	}

	public function setNullValues($attrs = array() ){
		foreach ($attrs as $key => $value) {
			if (empty($value) and $value !== 0 and $value !== false and $value !== "0")
				$attrs[$key] = null;
		}
		return $attrs;
	}
	private function setTypedValue($element, $values, $name) {
		$type = $element->get('type');
		//$name = $element->get('name');
		$value = trim($element->get('value'));
		
		$values[$name] = null;
		
		switch ($type) {
			
			case 'select' :
				if (is_numeric($value)) {
					$number = (int) $value;
					if ($number != null)
						$values[$name] = $number;
					else 
						$values[$name] = null;
				} else if (empty($value)) { 
				
					$values[$name] = null;
					
				} else {
					
					$values[$name] = $value;
				}
					
				break;
			
			case 'text' :
				$validators =  $element->has('validators')?$element->get('validators'):array();
				if (!empty($validators)) {
					foreach ($validators as $key => $item) {
						
							if ($item == 'Int') {
								$number = (int)$value;
								if ($value !== null and $value !== '')
									$value = $number;
							} else if ($item == 'Float') {
								$number = (float)$value;
								if ($value !== null and $value !== '')
									$value = $number;
							} 
								
						
					}
					$values[$name] = $value;
				} /*else if (preg_match("/\d{4}\-\d{2}-\d{2}/", $value)) {
					
					$transformDateFrUs = new Plugin_DateFrUs();
					$insert_values['datedecouverte'] = $transformDateFrUs->convertitDateFRUs($insert_values['datedecouverte']);
					
					$values[$name] = $value;
				}*/ else {
					$values[$name] = $value;
				}
				
				break;
			
			case 'hidden' :
				if (is_numeric($value)) {
					$number = (float) $value;
					if ($number != null)
						$values[$name] = $number;
					else
						$values[$name] = null;
				
				}else if ($value == null ) {
					$values[$name] = null;
				} else {
					$values[$name] = $value;
					
				} 
				break;
			
			case 'checkbox' :
				if ($value === 0 or $value === 1 or $value === '0' or $value === '1' ){
					$bool = (bool) $value;
					$values[$name] = $bool;
				}
				break;
				
			case 'textarea':
				
				if (!empty($value)) {
					$values[$name] = $value;
				}
				
				break;
				
			default :
				$values[$name] = $value;
				break;
		}
		
		return $values;
	}
	
	final public function getHeadHtml() {

		$html = '<div class="well"><form';

		foreach($this->getAttributes() as $key => $value)
			$html .= ' '.strtolower($key).'="'.$this->get($key, 'Html').'"';

		return $html.'>';

	}

	final public function getTailHtml() {

		$html = '</form></div>';
		
		if($this->_csrf) 
			$html = $this->_csrf->getHtml().$html;

		return $html;
	}

	public function getRessourceType(){
	    return str_replace("Form_","",get_class($this));
    }

}

// Do not clause PHP tags unless it is really necessary
